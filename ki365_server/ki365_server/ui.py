from flask import Blueprint, render_template
from pathlib import Path
from werkzeug.exceptions import NotFound

dist_path = Path(__file__).parent.parent / "frontend" / "dist"

bp = Blueprint(
    "ui",
    __name__,
    url_prefix="/",
    static_folder=dist_path / "assets",
    static_url_path="/assets",
    template_folder=dist_path,
)


# This will simply serve the frontend dist
@bp.route("/", defaults={"path": ""})
@bp.route("/<path:path>")
def index(path: str):
    # API/AUTH blueprints doesn't catch 404 links and sends them here instead
    if path.startswith('api/') or path.startswith('auth/'):
        raise NotFound()
    return render_template("index.html")
