#!/usr/bin/env python
from argparse import ArgumentParser, Namespace
import logging
import os
from redis import Redis # type: ignore
from rq import Worker, Queue # type: ignore
import time

from . import export_symbol, export_footprint

REDIS_HOST = os.environ.get("REDIS_HOST", default="localhost")
REDIS_PORT = int(os.environ.get("REDIS_PORT", default=6379))

def main_symbol(args: Namespace):
    # Tell RQ what Redis connection to use
    redis_conn = Redis()
    q = Queue('kicad_default', connection=redis_conn)  # no args implies the default queue

    # Delay execution of count_words_at_url('http://nvie.com')
    job = q.enqueue(export_symbol, args.library, args.symbol)
    print(job.return_value())   # => None  # Changed to job.return_value() in RQ >= 1.12.0

    # Now, wait a while, until the worker is finished
    time.sleep(5)
    print(job.return_value()) 

def main_footprint(args: Namespace):
    # Tell RQ what Redis connection to use
    redis_conn = Redis()
    q = Queue('kicad_default', connection=redis_conn)  # no args implies the default queue

    # Delay execution of count_words_at_url('http://nvie.com')
    job = q.enqueue(export_footprint, args.library, args.footprint)
    print(job.return_value())   # => None  # Changed to job.return_value() in RQ >= 1.12.0

    # Now, wait a while, until the worker is finished
    time.sleep(5)
    print(job.return_value()) 


def main():
    parser = ArgumentParser()
    parser.set_defaults(func=None)
    sp = parser.add_subparsers()

    symbol_parser = sp.add_parser('symbol')
    symbol_parser.set_defaults(func=main_symbol)
    symbol_parser.add_argument('library')
    symbol_parser.add_argument('symbol')

    footprint_parser = sp.add_parser('footprint')
    footprint_parser.set_defaults(func=main_footprint)
    footprint_parser.add_argument('library')
    footprint_parser.add_argument('footprint')

    args = parser.parse_args()

    if args.func is not None:
        args.func(args)
    else:
        # Provide the worker with the list of queues (str) to listen to.
        logging.basicConfig()
        logging.getLogger().setLevel(logging.DEBUG)
        redis = Redis(host=REDIS_HOST, port=REDIS_PORT)
        w = Worker(['kicad_high', 'kicad_default', 'kicad_low'], connection=redis)
        w.work()


if __name__ == "__main__":
    main()