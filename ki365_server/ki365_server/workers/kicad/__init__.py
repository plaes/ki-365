import logging
import subprocess
import os
import tempfile
from time import sleep

import dotenv
from slugify import slugify

dotenv.load_dotenv()

logger = logging.getLogger("kicad-exporter")

KICAD_CLI_EXECUTABLE = os.environ.get("KICAD_CLI_EXECUTABLE", "kicad-cli-nightly")
KICAD7_FOOTPRINT_DIR = os.environ.get("KICAD7_FOOTPRINT_DIR", "kicad-footprints")
KICAD7_SYMBOL_DIR = os.environ.get("KICAD7_SYMBOL_DIR", "kicad-symbols")

KICAD_CACHE_DIR = os.environ.get(
    "KICAD_CACHE_DIR", os.path.join(tempfile.gettempdir(), "kicad-cache")
)


def export_footprint(nick: str, footprint: str):
    with tempfile.TemporaryDirectory(prefix="ki-365-") as output_dir:
        command = [
            KICAD_CLI_EXECUTABLE,
            "fp",
            "export",
            "svg",
            "--footprint",
            footprint,
            "--output",
            output_dir,
            os.path.join(KICAD7_FOOTPRINT_DIR, f"{nick}.pretty"),
        ]
        logger.debug('Executing "' + " ".join(command) + '"')
        p = subprocess.run(
            command,
            # capture_output=True,
        )
        if p.returncode == 0:
            with open(os.path.join(output_dir, footprint + ".svg")) as footprint_svg:
                return [footprint_svg.read()]
        else:
            raise RuntimeError("Failed to execute kicad-cli")


def export_symbol(nick: str, symbol: str):
    """Just an example function that's called async."""
    with tempfile.TemporaryDirectory(prefix="ki-365-") as output_path:
        command = [
            KICAD_CLI_EXECUTABLE,
            "sym",
            "export",
            "svg",
            "--symbol",
            symbol,
            "--output",
            output_path,
            os.path.join(KICAD7_SYMBOL_DIR, f"{nick}.kicad_sym"),
        ]
        logger.debug('Executing "' + " ".join(command) + '"')
        p = subprocess.run(
            command,
            # capture_output=True
        )
        if p.returncode == 0:
            res = []
            symbols = os.listdir(output_path)
            for symbol_file in symbols:
                with open(os.path.join(output_path, symbol_file)) as symbol_svg:
                    res.append(symbol_svg.read())
            return res
        else:
            raise RuntimeError("Failed to execute kicad-cli")
