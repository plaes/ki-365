from marshmallow import Schema, fields


class ErrorSchema(Schema):
    # Error message
    error = fields.Str()
    
    # Error information
    info = fields.Raw()