from __future__ import annotations

from enum import Enum
from typing import List, Optional
import argon2
from sqlalchemy import ForeignKey

from sqlalchemy.orm import Mapped, DeclarativeBase, Session
from sqlalchemy.orm import mapped_column, relationship


ph = argon2.PasswordHasher()


class UserPasswordNotSet(Exception):
    pass


class UserBase(DeclarativeBase):
    pass


class User(UserBase):
    __tablename__ = "users"
    __bind_key__ = "users"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)

    name: Mapped[str] = mapped_column(unique=True)
    hash: Mapped[Optional[str]] = mapped_column()

    is_engineer: Mapped[bool] = mapped_column("is_engineer", nullable=False)
    is_librarian: Mapped[bool] = mapped_column("is_librarian", nullable=False)
    is_admin: Mapped[bool] = mapped_column("is_admin", nullable=False)

    #tokens: Mapped[List["ApiToken"]] = relationship(
    #    cascade="all, delete-orphan", back_populates="user"
    #)

    @property
    def permission_admin(self) -> bool:
        return self.is_admin

    @property
    def permission_engineer(self) -> bool:
        return self.is_engineer or self.permission_admin

    @property
    def permission_librarian(self) -> bool:
        return self.is_librarian or self.permission_admin

    @staticmethod
    def get_by_id(session: Session, id: int) -> User | None:
        return session.query(User).where(User.id == id).one_or_none()

    @staticmethod
    def get_by_username(session: Session, username: str) -> User:
        user = session.query(User).where(User.name == username).one_or_none()
        if user is None:
            raise argon2.exceptions.VerificationError("User does not exist")
        return user

    @staticmethod
    def authenticate(session: Session, username: str, password: str) -> User:
        user = User.get_by_username(session, username)

        # User haven't set a password, redirect to the change password prompt
        if user.hash is None:
            raise UserPasswordNotSet()

        # Validate password (update password hash if necesssary)
        ph.verify(user.hash, password)
        if ph.check_needs_rehash(user.hash):
            user.hash = ph.hash(password)
            session.add(user)
            session.commit()

        # Return user
        return user

    @staticmethod
    def change_password(
        session: Session, username: str, old_password: str | None, new_password: str
    ):
        if old_password is not None:
            # Old password provided, authenticate then change
            user = User.authenticate(session, username, old_password)
            user.hash = ph.hash(new_password)
        else:
            # No old password provided, check if user has no password set then change
            user = User.get_by_username(session, username)
            if user.hash is not None:
                raise argon2.exceptions.VerificationError("Provide password")
            user.hash = ph.hash(new_password)
        session.add(user)
        session.commit()

        return user


# class ApiToken(UserBase):
#     __tablename__ = "api_tokens"
#     __bind_key__ = "users"

#     id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
#     secret: Mapped[str] = mapped_column()

#     # User who created this key
#     user_id: Mapped[int] = mapped_column(ForeignKey("users.id"))
#     user: Mapped["User"] = relationship(back_populates="tokens")

#     _permission_engineer: Mapped[bool] = mapped_column("permission_engineer")
#     _permission_librarian: Mapped[bool] = mapped_column("permission_librarian")
#     _permission_admin: Mapped[bool] = mapped_column("permission_admin")

#     @property
#     def permission_engineer(self) -> bool:
#         return self._permission_engineer and self.user.permission_engineer

#     @property
#     def permission_librarian(self) -> bool:
#         return self._permission_librarian and self.user.permission_librarian

#     @property
#     def permission_admin(self) -> bool:
#         return self._permission_admin and self.user.permission_admin
