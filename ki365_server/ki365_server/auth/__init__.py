from datetime import datetime, timedelta, timezone
from http import HTTPStatus
from typing import Any
import argon2
from ki365_server.api.api_errors import IdNotFound
from marshmallow import Schema, fields
from flask import Blueprint, request, jsonify, redirect, Flask
from flask_jwt_extended import (
    JWTManager,
    create_access_token,
    create_refresh_token,
    current_user,
    get_csrf_token,
    get_jwt,
    get_jwt_identity,
    jwt_required,
    set_access_cookies,
    set_refresh_cookies,
    unset_jwt_cookies,
)

from ki365_server.auth.model import User, UserPasswordNotSet

from werkzeug.exceptions import HTTPException, Unauthorized, Forbidden

from marshmallow import ValidationError

from sqlalchemy.exc import SQLAlchemyError
from ..schemas import ErrorSchema


bp = Blueprint("auth", __name__, url_prefix="/auth")


from ..db import db

jwt = JWTManager()


def init_app(app: Flask):
    jwt.init_app(app)
    app.register_blueprint(bp)

    @app.after_request
    def refresh_expiring_jwts(response):
        session = db.session
        try:
            exp_timestamp = get_jwt()["exp"]
            now = datetime.now(timezone.utc)
            target_timestamp = datetime.timestamp(now + timedelta(minutes=30))
            if target_timestamp > exp_timestamp:
                identity: int = get_jwt_identity()
                user = session.query(User).where(User.id == identity).one_or_none()

                access_token = create_access_token(identity=user)
                set_access_cookies(response, access_token)
            return response
        except (RuntimeError, KeyError):
            # Case where there is not a valid JWT. Just return the original response
            return response


class UserSchema(Schema):
    id = fields.Int(dump_only=True)
    name = fields.Str(required=True)
    is_engineer = fields.Bool(load_default=False)
    is_librarian = fields.Bool(load_default=False)
    is_admin = fields.Bool(load_default=False)


class LoginResponse(Schema):
    user = fields.Nested(UserSchema, required=False)
    success = fields.Bool()
    change_required = fields.Bool()
    csrf_access_token = fields.Str()


class LoginCredentials(Schema):
    username = fields.Str(required=True)
    password = fields.Str(required=True, load_only=True)


class ChangeCredentials(Schema):
    username = fields.Str(required=True)
    old_password = fields.Str(required=False, load_only=True)
    new_password = fields.Str(required=True, load_only=True)

class ChangeUsername(Schema):
    new_username = fields.Str(required=True)


# Register a callback function that takes whatever object is passed in as the
# identity when creating JWTs and converts it to a JSON serializable format.
@jwt.user_identity_loader
def user_identity_lookup(user: User):
    return user.id


# Register a callback function that loads a user from your database whenever
# a protected route is accessed. This should return any python object on a
# successful lookup, or None if the lookup failed for any reason (for example
# if the user has been deleted from the database).
@jwt.user_lookup_loader
def user_lookup_callback(_jwt_header, jwt_data):
    session = db.session
    identity = jwt_data["sub"]
    return session.query(User).where(User.id == identity).one_or_none()


@bp.route("/login", methods=("POST",))
def login():
    session = db.session

    data = request.get_json()
    creds: dict[str, Any] = LoginCredentials().load(data) # type: ignore
    username = creds["username"]
    password = creds["password"]

    try:
        user = User.authenticate(session, username, password)
    except argon2.exceptions.VerificationError:
        raise Unauthorized("Invalid credentials")
    except UserPasswordNotSet:
        # Do not generate tokens but request user to change the password
        user = User.get_by_username(session, username)
        response = jsonify(
            LoginResponse().dump(
                {"user": user, "success": True, "change_required": True}
            )
        )
        return response

    # Create token
    access_token = create_access_token(identity=user)
    csrf_access_token = get_csrf_token(access_token)
    print(csrf_access_token)

    # Send token back
    response = jsonify(
        LoginResponse().dump(
            {
                "user": user,
                "success": True,
                # Include CSRF Token
                "csrf_access_token": csrf_access_token,
            }
        )
    )
    set_access_cookies(response, access_token)
    return response


@bp.route("/change-password", methods=("POST",))
@jwt_required(optional=True)
def change_password():
    session = db.session

    data = request.get_json()
    creds: dict[str, Any] = ChangeCredentials().load(data) # type: ignore
    username = creds["username"]
    old_password = creds.get("old_password")
    new_password = creds["new_password"]

    try:
        user = User.change_password(session, username, old_password, new_password)

        # Create token
        access_token = create_access_token(identity=user)
        csrf_access_token = get_csrf_token(access_token)
        print(csrf_access_token)

        # Send token back
        response = jsonify(
            LoginResponse().dump(
                {
                    "user": user,
                    "success": True,
                    # Include CSRF Token
                    "csrf_access_token": csrf_access_token,
                }
            )
        )
        set_access_cookies(response, access_token)
        return response
    except argon2.exceptions.VerificationError:
        raise Unauthorized("Invalid credentials")

@bp.route("/username", methods=("PUT",))
@jwt_required(fresh=True)
def change_username():
    session = db.session
    user: User = current_user

    data = request.get_json()
    creds: dict[str, Any] = ChangeUsername().load(data) # type: ignore
    username = creds["new_username"]

    # Set username
    user.name = username
    session.add(user)
    session.commit()

    # Send token back
    response = jsonify(user.name)
    return response


@bp.route("/logout", methods=["POST"])
def logout():
    response = jsonify(LoginResponse().dump({"user": None, "success": True}))
    unset_jwt_cookies(response)
    return response


@bp.errorhandler(SQLAlchemyError)
def handle_database_error(err: SQLAlchemyError):
    print(err)
    res = ErrorSchema().dump({"error": "Database error", "info": err.code})
    print(err)
    return jsonify(res), HTTPStatus.INTERNAL_SERVER_ERROR


@bp.errorhandler(ValidationError)
def handle_validation_error(err: ValidationError):
    res = ErrorSchema().dump({"error": "Invalid request data", "info": err.messages})
    print(err)
    return jsonify(res), HTTPStatus.BAD_REQUEST
