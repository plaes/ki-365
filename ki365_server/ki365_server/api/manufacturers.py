from typing import Any
from flask import Blueprint, jsonify, request
import sqlalchemy
from ki365_server.api.api_errors import IdNotFound
from ki365_common.models.manufacturers import Manufacturer, ManufacturerPart
from ki365_common.models.suppliers import SupplierPart
from ki365_common.schemas.manufacturer import ManufacturerPartSchema, ManufacturerSchema
from ki365_common.schemas.suppliers import SupplierPartSchema

from ..db import db


from marshmallow import Schema, fields

bp = Blueprint("manufacturers", __name__, url_prefix="/manufacturers")

MAX_ITEMS_PER_PAGE=100

# Get part information
@bp.route("/<int:manufacturer_id>", methods=["GET", "POST", "DELETE"])
def manufacturer_info(manufacturer_id: int):
    session = db.session
    manufacturer: Manufacturer | None = (
        session.query(Manufacturer)
        .where(Manufacturer.id == manufacturer_id)
        .one_or_none()
    )
    if manufacturer:
        if request.method == "POST":
            update_manufacturer_info = request.get_json()

            # Validate data
            res: dict[str, Any] = ManufacturerSchema(partial=True).load(
                update_manufacturer_info
            )

            # Update part info
            manufacturer.name = res.get("name", manufacturer.name)
            manufacturer.description = res.get("description", manufacturer.description)
            manufacturer.link = res.get("link", manufacturer.link)
            manufacturer.status = res.get("status", manufacturer.status)

            # Commit changes
            session.add(manufacturer)
            session.commit()
        elif request.method == "DELETE":
            session.delete(manufacturer)
            session.commit()

        response = ManufacturerSchema().dump(manufacturer)
        return jsonify(response)

    else:
        raise IdNotFound(manufacturer_id)


# Liat all parts or insert a new part
@bp.route("", methods=["GET", "PUT"])
@bp.route("/", methods=["GET", "PUT"])
def list_manufacturers():
    session = db.session

    if request.method == "PUT":
        create_manufacturer = request.get_json()

        # Create new part
        res = ManufacturerSchema().load(create_manufacturer)
        manufacturer = Manufacturer(**res)
        session.add(manufacturer)
        session.commit()

        response = ManufacturerSchema().dump(manufacturer)
    else:
        page = request.args.get("page", default=1, type=int)
        items_per_page = request.args.get("items_per_page", default=100, type=int)
        search = request.args.get("search")
        sort_key = request.args.get("sort_key")
        sort_order = request.args.get("sort_order")

        # When 'all' is selected
        if items_per_page == -1:
            items_per_page = MAX_ITEMS_PER_PAGE

        # Search part info if a search parameter is provided
        select = session.query(Manufacturer)
        if search is not None:
            select = select.filter(
                sqlalchemy.or_(
                    Manufacturer.name.ilike(f"%{search}%"),
                    Manufacturer.description.ilike(f"%{search}%"),
                )
            )

        # Order result based on key and order
        sortable = {
            "id": Manufacturer.id,
            "name": Manufacturer.name,
            "description": Manufacturer.description,
            "status": Manufacturer.status,
        }
        sort = sortable.get(sort_key, Manufacturer.id)
        select = select.order_by(sort.desc() if sort_order == "desc" else sort.asc())

        # Paginate the response
        page = db.paginate(
            select,
            page=page,
            per_page=items_per_page,
            max_per_page=MAX_ITEMS_PER_PAGE,
        )

        response = {
            "manufacturers": ManufacturerSchema(many=True).dump(page.items),
            "total": page.total,
        }
    return jsonify(response)


# List all parts belonging to a manufacturer
@bp.route("/<int:manufacturer_id>/parts")
@bp.route("/<int:manufacturer_id>/parts/")
def list_manufacturer_parts(manufacturer_id: int):
    session = db.session
    supplier: Manufacturer | None = (
        session.query(Manufacturer)
        .where(Manufacturer.id == manufacturer_id)
        .one_or_none()
    )
    if supplier:
        response = ManufacturerPartSchema(many=True).dump(supplier)
        return jsonify(response)
    else:
        raise IdNotFound(manufacturer_id)


# Get, update and delete manufacturer parts by id
# Manufacturer id is not required (and ignored)
@bp.route("/parts/<int:id>", methods=["GET", "POST", "DELETE"])
def manufacturer_parts(id: int):
    session = db.session
    manufacturer_part: ManufacturerPart | None = (
        session.query(ManufacturerPart).where(ManufacturerPart.id == id).one_or_none()
    )
    if manufacturer_part:
        if request.method == "POST":
            update_manufacturer_info = request.get_json()

            # Validate data
            res: dict[str, Any] = ManufacturerPartSchema(partial=True).load(
                update_manufacturer_info
            )

            # Update part info
            manufacturer_part.partno = res.get("partno", manufacturer_part.partno)
            manufacturer_part.status = res.get("status", manufacturer_part.status)

            # Commit changes
            session.add(manufacturer_part)
            session.commit()
            response = ManufacturerPartSchema().dump(manufacturer_part)
        elif request.method == "DELETE":
            response = ManufacturerPartSchema().dump(manufacturer_part)
            session.delete(manufacturer_part)
            session.commit()

        return jsonify(response)

    else:
        raise IdNotFound(id)


@bp.route("/parts/<int:manufacture_part_id>/suppliers", methods=["GET", "PUT"])
def part_manufacturer_parts(manufacture_part_id: int):
    session = db.session
    part: ManufacturerPart | None = (
        session.query(ManufacturerPart)
        .where(ManufacturerPart.id == manufacture_part_id)
        .one_or_none()
    )
    if part:
        if request.method == "PUT":
            supplier_part_info = request.get_json()
            # Create new part
            res: dict[str, Any] = SupplierPartSchema().load(supplier_part_info)
            # Update part info
            supplier_part = SupplierPart(**res)
            # with session.begin():
            part.supplier_parts.append(supplier_part)
            session.add_all([part, supplier_part])
            session.commit()
            res = SupplierPartSchema().dump(supplier_part)
        else:
            # Respond with info
            res = SupplierPartSchema(many=True).dump(part.supplier_parts)
    else:
        raise IdNotFound(manufacture_part_id)
    return jsonify(res)
