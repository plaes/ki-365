from http import HTTPStatus
from logging import Logger
from flask import (
    Blueprint, abort, jsonify, request
)
from flask_jwt_extended import verify_jwt_in_request
from flask_jwt_extended.exceptions import NoAuthorizationError

from werkzeug.exceptions import HTTPException

from marshmallow import ValidationError

from sqlalchemy.exc import SQLAlchemyError
from ..schemas import ErrorSchema

bp = Blueprint('api', __name__, url_prefix='/api')

@bp.before_request
def check_login():
    if (request.endpoint):
        try:
            verify_jwt_in_request()
        except NoAuthorizationError as e:
            print(e)
            abort(401)

# /api/parts
from .parts import bp as part_bp
bp.register_blueprint(part_bp)

# /api/manufacturers
from .manufacturers import bp as mfr_bp
bp.register_blueprint(mfr_bp)

# /api/suppliers
from .suppliers import bp as supp_bp
bp.register_blueprint(supp_bp)

# /api/kicad
from .assemblies import bp as assemblies_pb
bp.register_blueprint(assemblies_pb)

# /api/kicad
from .kicad import bp as kicad_pb
bp.register_blueprint(kicad_pb)

# /api/users
from .users import bp as users_pb
bp.register_blueprint(users_pb)


@bp.errorhandler(SQLAlchemyError)
def handle_database_error(err: SQLAlchemyError):
    print(err)
    res = ErrorSchema().dump({"error": "Database error", "info": err.code})
    print(err)
    return jsonify(res), HTTPStatus.INTERNAL_SERVER_ERROR

@bp.errorhandler(ValidationError)
def handle_validation_error(err: ValidationError):
    res = ErrorSchema().dump({"error": "Invalid request data", "info": err.messages})
    print(err)
    return jsonify(res), HTTPStatus.BAD_REQUEST

@bp.app_errorhandler(HTTPException) # type: ignore
def handle_exception(e: HTTPException):
    """Return JSON instead of HTML for HTTP errors."""

    res = ErrorSchema().dump({"error": e.name, "info": {
        "code": e.code,
        "description": e.description,
    }})

    return jsonify(res), e.code
