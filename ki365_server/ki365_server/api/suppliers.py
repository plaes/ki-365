from typing import Any
from flask import Blueprint, jsonify, request
import sqlalchemy
from ki365_server.api.api_errors import IdNotFound
from ki365_common.models.suppliers import Supplier, SupplierPart
from ki365_common.schemas.suppliers import SupplierSchema, SupplierPartSchema

from ..db import db


from marshmallow import Schema, fields

bp = Blueprint("suppliers", __name__, url_prefix="/suppliers")

MAX_ITEMS_PER_PAGE=100


# Get part information
@bp.route("/<int:supplier_id>", methods=["GET", "POST", "DELETE"])
def supplier_info(supplier_id: int):
    session = db.session
    supplier: Supplier | None = (
        session.query(Supplier)
        .where(Supplier.id == supplier_id)
        .one_or_none()
    )
    if supplier:
        if request.method == "POST":
            update_supplier_info = request.get_json()

            # Validate data
            res: dict[str, Any] = SupplierSchema(partial=True).load(
                update_supplier_info
            )

            # Update part info
            supplier.name = res.get("name", supplier.name)
            supplier.description = res.get("description", supplier.description)
            supplier.link = res.get("link", supplier.link)
            supplier.status = res.get("status", supplier.status)

            # Commit changes
            session.add(supplier)
            session.commit()
        elif request.method == "DELETE":
            session.delete(supplier)
            session.commit()

        response = SupplierSchema().dump(supplier)
        return jsonify(response)

    else:
        raise IdNotFound(supplier_id)


# Liat all parts or insert a new part
@bp.route("", methods=["GET", "PUT"])
@bp.route("/", methods=["GET", "PUT"])
def list_suppliers():
    session = db.session

    if request.method == "PUT":
        create_supplier = request.get_json()

        # Create new part
        res = SupplierSchema().load(create_supplier)
        supplier = Supplier(**res)
        session.add(supplier)
        session.commit()

        response = SupplierSchema().dump(supplier)
    else:
        page = request.args.get("page", default=1, type=int)
        items_per_page = request.args.get("items_per_page", default=100, type=int)
        search = request.args.get("search")
        sort_key = request.args.get("sort_key")
        sort_order = request.args.get("sort_order")

        # When 'all' is selected
        if items_per_page == -1:
            items_per_page = MAX_ITEMS_PER_PAGE

        # Search part info if a search parameter is provided
        select = session.query(Supplier)
        if search is not None:
            select = select.filter(
                sqlalchemy.or_(
                    Supplier.name.ilike(f"%{search}%"),
                    Supplier.description.ilike(f"%{search}%"),
                )
            )

        # Order result based on key and order
        sortable = {
            "id": Supplier.id,
            "name": Supplier.name,
            "description": Supplier.description,
            "status": Supplier.status,
        }
        sort = sortable.get(sort_key, Supplier.id)
        select = select.order_by(sort.desc() if sort_order == "desc" else sort.asc())

        # Paginate the response
        page = db.paginate(
            select,
            page=page,
            per_page=items_per_page,
            max_per_page=MAX_ITEMS_PER_PAGE,
        )

        response = {
            "suppliers": SupplierSchema(many=True).dump(page.items),
            "total": page.total,
        }
    return jsonify(response)


# List all parts belonging to a supplier
@bp.route("/<int:supplier_id>/parts")
@bp.route("/<int:supplier_id>/parts/")
def list_supplier_parts(supplier_id: int):
    session = db.session
    supplier: Supplier | None = (
        session.query(Supplier)
        .where(Supplier.id == supplier_id)
        .one_or_none()
    )
    if supplier:
        response = SupplierPartSchema(many=True).dump(supplier)
        return jsonify(response)
    else:
        raise IdNotFound(supplier_id)


@bp.route("/parts/<int:id>", methods=["GET", "POST", "DELETE"])
def supplier_parts(id):
    session = db.session
    supplier_part = session.query(SupplierPart).where(SupplierPart.id == id).one_or_none()
    if supplier_part:
        if request.method == "POST":
            update_supplier_info = request.get_json()

            # Validate data
            res: dict[str, Any] = SupplierPartSchema(partial=True).load(
                update_supplier_info
            )

            # Update part info
            supplier_part.partno = res.get("partno", supplier_part.partno)
            supplier_part.status = res.get("status", supplier_part.status)

            # Commit changes
            session.add(supplier_part)
            session.commit()
            response = SupplierPartSchema().dump(supplier_part)
        elif request.method == "DELETE":
            response = SupplierPartSchema().dump(supplier_part)
            session.delete(supplier_part)
            session.commit()

        return jsonify(response)

    else:
        raise IdNotFound(id)
