from pprint import pprint
from typing import Any, Mapping
from flask import Blueprint, jsonify, request
from slugify import slugify  # type: ignore
from sqlalchemy import and_, or_, select

from werkzeug.exceptions import BadRequest
from ki365_server.api.api_errors import IdNotFound, IdRevNotFound

from ki365_common.models.assemblies import (
    Assembly,
    AssemblyRevision,
    AssemblyRevisionField,
    AssemblyRevisionPart,
    AssemblyRevisionPartField,
)
from ki365_common.models.part import Part

from ki365_common.schemas.assemblies import (
    AssemblyFieldSchema,
    AssemblyRevisionPartSchema,
    AssemblyRevisionSchema,
    AssemblySchema,
)
from ki365_common.schemas.part import PartSchema

from ..db import db


bp = Blueprint("assemblies", __name__, url_prefix="/assemblies")

MAX_ITEMS_PER_PAGE = 100


# Get part information
@bp.route("", methods=["GET", "POST"])
@bp.route("/", methods=["GET", "POST"])
def assembly_list():
    session = db.session
    if request.method == "POST":
        create_part = request.get_json()

        # Create new part
        res: dict[str, Any] = PartSchema().load(create_part, partial=False)  # type: ignore

        assembly = Assembly(revisions=[])
        part = Part(**res, manufacturer_parts=[], assembly=assembly)

        session.add_all([part, assembly])
        session.commit()

        response = AssemblySchema().dump(assembly)
    else:
        page = request.args.get("page", default=1, type=int)
        items_per_page = request.args.get("items_per_page", default=100, type=int)
        search = request.args.get("search")
        sort_key = request.args.get("sort_key", "")
        sort_order = request.args.get("sort_order")

        # When 'all' is selected
        if items_per_page == -1:
            items_per_page = MAX_ITEMS_PER_PAGE

        # Search part info if a search parameter is provided
        stmt = select(Assembly)
        if search is not None:
            stmt = stmt.join(Assembly.part).filter(
                or_(
                    Part.title.ilike(f"%{search}%"),
                    Part.description.ilike(f"%{search}%"),
                )
            )

        # Order result based on key and order
        sortable = {
            "id": Assembly.id,
            "name": Part.title,
            "description": Part.description,
            "status": Part.status,
        }
        sort = sortable.get(sort_key, Assembly.id)
        stmt = stmt.order_by(sort.desc() if sort_order == "desc" else sort.asc())

        # Paginate the response
        page = db.paginate(
            stmt,
            page=page,
            per_page=items_per_page,
            max_per_page=MAX_ITEMS_PER_PAGE,
        )

        response = {
            "assemblies": AssemblySchema(many=True).dump(page.items),
            "total": page.total,
        }
    return jsonify(response)


# Get part information
@bp.route("/<int:assembly_id>", methods=["GET", "PUT", "POST", "DELETE"])
@bp.route("/<int:assembly_id>/", methods=["GET", "PUT", "POST", "DELETE"])
def assembly_info(assembly_id: int):
    session = db.session
    assembly: Assembly | None = (
        session.query(Assembly).where(Assembly.id == assembly_id).one_or_none()
    )
    if assembly:
        if request.method == "POST":
            create_assembly = request.get_json()

            # Create new part
            res: dict[str, Any] = AssemblyRevisionSchema(exclude=["parts"]).load(create_assembly)  # type: ignore
            fields = []

            # Create a slug if not provided
            if "revision_slug" not in res:
                res["revision_slug"] = slugify(res["revision"])

            _fields = res.get("_fields")
            if _fields is not None:
                for field in _fields:
                    fields.append(AssemblyRevisionField(**field))
                del res["_fields"]
            pprint(res)

            # Add to assembly
            assembly_revision = AssemblyRevision(**res, parts=[], _fields=fields)

            assembly.revisions.append(assembly_revision)

            session.add_all([assembly, assembly_revision])
            session.commit()

            response = AssemblyRevisionSchema().dump(assembly_revision)
        elif request.method == "PUT" or request.method == "PATCH":
            update_assembly = request.get_json()
            validated: dict[str, Any] = PartSchema(partial=(request.method == "PATCH")).load(update_assembly)  # type: ignore

            # Update assembly part information
            part = assembly.part
            part.title = validated.get("title", part.title)
            part.description = validated.get(
                "description", part.description
            )
            part.status = validated.get("status", part.status)

            # Commit to database
            session.add(part)
            session.commit()

            response = AssemblySchema().dump(assembly)
        elif request.method == "DELETE":
            response = AssemblySchema().dump(assembly)
            # Delete the assembly parent part which in turn will delete the assembly and its revisions
            session.delete(assembly.part)
            session.commit()
        else:
            response = AssemblySchema().dump(assembly)

        return jsonify(response)

    else:
        raise IdNotFound(assembly_id)


@bp.route("/<int:assembly_id>/<revision>", methods=["GET", "DELETE"])
@bp.route("/<int:assembly_id>/<revision>/", methods=["GET", "DELETE"])
def assembly_rev_info(assembly_id: int, revision: str):
    session = db.session

    assembly_revision: AssemblyRevision | None = (
        session.query(AssemblyRevision)
        .where(
            and_(
                AssemblyRevision.assembly_id == assembly_id,
                AssemblyRevision.revision_slug == revision,
            )
        )
        .one_or_none()
    )
    if assembly_revision:
        if request.method == "PUT":
            #TODO
            response = AssemblyRevisionSchema().dump(assembly_revision)
        elif request.method == "DELETE":
            response = AssemblyRevisionSchema().dump(assembly_revision)
            session.delete(assembly_revision)
            session.commit()
        else:
            response = AssemblyRevisionSchema().dump(assembly_revision)

    else:
        raise IdRevNotFound(assembly_id, revision)

    return jsonify(response)


@bp.route("/<int:assembly_id>/<revision>/parts", methods=["GET", "POST"])
def assembly_rev_parts(assembly_id: int, revision: str):
    session = db.session
    assembly_revision: AssemblyRevision | None = (
        session.query(AssemblyRevision)
        .where(
            and_(
                AssemblyRevision.assembly_id == assembly_id,
                AssemblyRevision.revision_slug == revision,
            )
        )
        .one_or_none()
    )
    if assembly_revision:
        if request.method == "POST":
            add_part = request.get_json()
            res: dict[str, Any] = AssemblyRevisionPartSchema().load(add_part)  # type: ignore

            fields = []

            _fields = res.get("_fields")
            if _fields is not None and isinstance(_fields, Mapping):
                for field in _fields:
                    fields.append(AssemblyRevisionPartField(**field))
                del res["_fields"]

            pprint(res)

            q = session.query(Part.id).filter(Part.id == res["part_id"])
            if session.query(q.exists()).scalar():
                assembly_rev_part = AssemblyRevisionPart(**res, _fields=fields)
                assembly_revision.parts.append(assembly_rev_part)

                session.add_all([assembly_revision, assembly_rev_part])
                session.commit()

                result = AssemblyRevisionPartSchema().dump(assembly_rev_part)
            else:
                raise BadRequest("Invalid part-id")
        else:
            parts = (
                session.query(AssemblyRevisionPart)
                .where(AssemblyRevisionPart.assembly_rev_id == assembly_revision.id)
                .all()
            )
            result = AssemblyRevisionPartSchema(many=True).dump(parts)

        return jsonify(result)
    else:
        raise IdRevNotFound(assembly_id, revision)


@bp.route("/<int:assembly_id>/<revision>/parts/<int:id>", methods=["GET", "DELETE"])
def assembly_rev_part_info(assembly_id: int, revision: str, id: int):
    session = db.session
    assembly_revision: AssemblyRevision | None = (
        session.query(AssemblyRevision)
        .where(
            and_(
                AssemblyRevision.assembly_id == assembly_id,
                AssemblyRevision.revision_slug == revision,
            )
        )
        .one_or_none()
    )
    if assembly_revision:
        part = (
            session.query(AssemblyRevisionPart)
            .where(
                and_(
                    AssemblyRevisionPart.assembly_rev_id == assembly_revision.id,
                    AssemblyRevisionPart.id == id,
                )
            )
            .one_or_none()
        )
        if part is None:
            raise IdNotFound(id)

        result = AssemblyRevisionPartSchema().dump(part)

        if request.method == "DELETE":
            session.delete(part)
            session.commit()

        return jsonify(result)
    else:
        raise IdRevNotFound(assembly_id, revision)


@bp.route("/<int:assembly_id>/<revision>/fields", methods=["GET", "POST"])
def assembly_rev_fields(assembly_id: int, revision: str):
    session = db.session
    assembly_revision: AssemblyRevision | None = (
        session.query(AssemblyRevision)
        .where(
            and_(
                AssemblyRevision.assembly_id == assembly_id,
                AssemblyRevision.revision_slug == revision,
            )
        )
        .one_or_none()
    )
    if assembly_revision:
        if request.method == "POST":
            add_field = request.get_json()
            res: dict[str, Any] = AssemblyFieldSchema().load(add_field)  # type: ignore

            assembly_rev_field = AssemblyRevisionField(**res)
            assembly_revision._fields.append(assembly_rev_field)

            session.add_all([assembly_revision, assembly_rev_field])
            session.commit()

            result = AssemblyFieldSchema().dump(assembly_rev_field)

        else:
            parts = (
                session.query(AssemblyRevisionField)
                .where(AssemblyRevisionField.assembly_rev_id == assembly_revision.id)
                .all()
            )
            result = AssemblyFieldSchema(many=True).dump(parts)

        return jsonify(result)
    else:
        raise IdRevNotFound(assembly_id, revision)


@bp.route("/<int:assembly_id>/<revision>/fields/<int:id>", methods=["GET", "DELETE"])
def assembly_rev_field_info(assembly_id: int, revision: str, id: int):
    session = db.session
    assembly_revision: AssemblyRevision | None = (
        session.query(AssemblyRevision)
        .where(
            and_(
                AssemblyRevision.assembly_id == assembly_id,
                AssemblyRevision.revision_slug == revision,
            )
        )
        .one_or_none()
    )
    if assembly_revision:
        field = (
            session.query(AssemblyRevisionField)
            .where(
                and_(
                    AssemblyRevisionField.assembly_rev_id == assembly_revision.id,
                    AssemblyRevisionField.id == id,
                )
            )
            .one_or_none()
        )
        if field is None:
            raise IdNotFound(id)

        result = AssemblyFieldSchema().dump(field)

        if request.method == "DELETE":
            session.delete(field)
            session.commit()

        return jsonify(result)
    else:
        raise IdRevNotFound(assembly_id, revision)
