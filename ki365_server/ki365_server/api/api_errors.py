from werkzeug.exceptions import NotFound

class IdNotFound(NotFound):
    def __init__(self, id: int) -> None:
        super().__init__(description="Requested ID not found")
        self.id = id

class IdRevNotFound(NotFound):
    def __init__(self, id: int, rev: str) -> None:
        super().__init__(description="Requested ID/revision not found")
        self.id = id
        self.rev = rev

