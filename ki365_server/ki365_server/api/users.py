from typing import Any
from flask import Blueprint, jsonify, request
from flask_jwt_extended import current_user
from ki365_server.auth import UserSchema
from ki365_server.auth.model import User

from ki365_server.api.api_errors import IdNotFound
from werkzeug.exceptions import HTTPException, Unauthorized, Forbidden

from ..db import db

bp = Blueprint("users", __name__, url_prefix="/users")


@bp.route("", methods=["GET", "POST"])
@bp.route("/", methods=["GET", "POST"])
def list_users():
    session = db.session

    user: User = current_user
    if not user.permission_admin:
        raise Forbidden("Insufficient user permissions")

    if request.method == "POST":
        data = request.get_json()
        new_user_data: dict[str, Any] = UserSchema().load(data)  # type: ignore
        new_user = User(**new_user_data)
        session.add(new_user)
        session.commit()
        response = jsonify(UserSchema().dump(new_user))
    else:
        all_users = session.query(User).all()
        response = jsonify(UserSchema(many=True).dump(all_users))
    return response


@bp.route("/<int:id>", methods=["GET", "PUT", "DELETE"])
@bp.route("/<int:id>/", methods=["GET", "PUT", "DELETE"])
def get_user(id: int):
    session = db.session

    admin: User = current_user
    if not admin.permission_admin:
        raise Forbidden("Insufficient user permissions")

    user = User.get_by_id(session, id)  # type: ignore
    if user is None:
        raise IdNotFound(id)

    if request.method == "PUT" or request.method == "PATCH":
        data = request.get_json()
        new_user_data: dict[str, Any] = UserSchema(partial=(request.method == "PATCH")).load(data)  # type: ignore
        user.name = new_user_data.get('name', user.name)
        user.is_admin = new_user_data.get('is_admin', user.is_admin)
        user.is_engineer = new_user_data.get('is_engineer', user.is_engineer)
        user.is_librarian = new_user_data.get('is_librarian', user.is_librarian)

        response = UserSchema().dump(user)

    elif request.method == "DELETE":
        response = UserSchema().dump(user)
        session.delete(user)
        session.commit()
    else:
        response = UserSchema().dump(user)

    return jsonify(response)


@bp.route("/<int:id>/password", methods=["DELETE"])
@bp.route("/<int:id>/password/", methods=["DELETE"])
def reset_user_password(id: int):
    session = db.session

    admin: User = current_user
    if not admin.permission_admin:
        raise Forbidden("Insufficient user permissions")

    user = User.get_by_id(session, id)  # type: ignore

    if user is not None:
        user.hash = None
        session.add(user)
        session.commit()

        return jsonify(msg="successfull")
    else:
        raise IdNotFound(id)
