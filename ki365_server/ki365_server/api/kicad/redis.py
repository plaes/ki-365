import os
from redis import Redis
from rq.queue import Queue

KICAD_CONFIG_DIR = os.environ.get("KICAD_CONFIG_DIR", default="")
KICAD_CACHE_EXPIRE = int(os.environ.get("KICAD_CACHE_EXPIRE", default=900))

REDIS_HOST = os.environ.get("REDIS_HOST", default="localhost")
REDIS_PORT = int(os.environ.get("REDIS_PORT", default=6379))

redis = Redis(host=REDIS_HOST, port=REDIS_PORT)
kicad_queue = Queue("kicad_default", connection=redis)

