from fnmatch import fnmatch
from http.client import ACCEPTED
import os
from flask import Blueprint, jsonify, request, redirect
from ki365_server.workers.kicad import export_footprint
from werkzeug.exceptions import (
    NotFound,
    ServiceUnavailable,
    InternalServerError,
    BadRequest,
)
from ki365_common.integrations.kicad.footprints import (
    FootprintLibrary,
    FootprintLibrarySchema,
    FootprintSchema,
    load_footprint_libraries,
)
from ki365_common.integrations.kicad.library_table import (
    LibraryTable,
    LibraryTableItem,
    LibraryTableSchema,
    LibraryTableItemSchema,
)

bp = Blueprint("footprints", __name__, url_prefix="/footprints")

from ki365_server.api.kicad.redis import KICAD_CACHE_EXPIRE, KICAD_CONFIG_DIR, kicad_queue, redis

KICAD_FP_LIB_TABLE_PATH = os.environ.get(
    "KICAD_FP_LIB_TABLE_PATH", default=os.path.join(KICAD_CONFIG_DIR, "fp-lib-table")
)

footprints: tuple[
    LibraryTable, list[tuple[LibraryTableItem, FootprintLibrary]]
] | None = None


def load_fp_libraries():
    global footprints

    print(f"Loading {KICAD_FP_LIB_TABLE_PATH}, this might take a while...")
    footprints = load_footprint_libraries(KICAD_FP_LIB_TABLE_PATH)
    if footprints is None:
        print("No footprint libraries loaded!")
    else:
        print("Done")


@bp.route("", methods=["GET"])
@bp.route("/", methods=["GET"])
def list_libraries():
    if footprints is None:
        raise NotFound

    fp_lib_table, _symbol_libraries = footprints
    res = LibraryTableSchema().dump(fp_lib_table)
    return jsonify(res)


@bp.route("/all", methods=["GET"])
@bp.route("/all/", methods=["GET"])
def list_all_libraries():
    if footprints is None:
        raise NotFound

    search = request.args.get("search", default="")

    _fp_lib_table, footprints_libraries = footprints
    res = []
    for lib, symbol_lib in footprints_libraries:
        filtered_symbols = [
            fp
            for fp in symbol_lib.footprints
            if search.casefold() in fp.library_id.casefold()
            or search.casefold() in (fp.description or "").casefold()
        ]
        if len(filtered_symbols) > 0:
            res.append(
                {
                    **LibraryTableItemSchema().dump(lib),  # type: ignore
                    "footprints": FootprintSchema(many=True).dump(filtered_symbols),
                }
            )
    return jsonify(res)


@bp.route("/<library>", methods=["GET"])
def list_library_footprints(library: str):
    if footprints is None:
        raise NotFound

    _, footprint_libraries = footprints
    fp_lib = next(
        (fp_lib for nick, fp_lib in footprint_libraries if nick.slug == library),
        None,
    )
    if fp_lib is not None:
        res = FootprintLibrarySchema().dump(fp_lib)
        return jsonify(res)
    else:
        raise NotFound("Library not found")


@bp.route("/export", methods=["GET"])
def export_library_symbols():
    if footprints is None:
        raise NotFound

    library = request.args.get("library")
    footprint = request.args.get("footprint")

    url_hash = f"footprint:{library}:{footprint}"
    rq_job_id = redis.hget(url_hash, "job_id")
    if rq_job_id is not None:
        job_id = rq_job_id.decode()
        job_hash = f"rq:job:{job_id}"
        ttl = redis.ttl(job_hash)
        if ttl:
            # Add 30 more seconds of buffer room
            # to ensure job.result doesn't get deleted pre-maturely
            redis.expire(job_hash, KICAD_CACHE_EXPIRE)
        else:
            # Job result has already been deleted, clear lookup hash
            redis.delete(url_hash)
    else:
        # Start job
        job = kicad_queue.enqueue_call(
            func=export_footprint, args=(library, footprint), result_ttl=KICAD_CACHE_EXPIRE
        )
        # Create job.id lookup using hash as key (for cache)
        if redis.hsetnx(url_hash, "job_id", job.id):
            redis.expire(url_hash, KICAD_CACHE_EXPIRE)
        job_id = job.get_id()

    # Redirect to job page
    return redirect(f"./export/{job_id}")


@bp.route("/export/<job_id>", methods=["GET"])
def get_export_library_symbols(job_id: str):
    job = kicad_queue.fetch_job(job_id)
    if job is None:
        raise BadRequest("Invalid job id")
    if job.is_canceled:
        raise InternalServerError(description="Job was cancelled")
    elif job.is_failed:
        raise InternalServerError(description="Job failed")
    elif not job.is_finished:
        return jsonify(status=str(job.get_status())), ACCEPTED
    else:
        res = job.result
        if res is not None:
            return jsonify(res)
        else:
            raise BadRequest("Invalid job id or wrong endpoint.")


@bp.route("/find", methods=["GET"])
def search_library_footprints():
    if footprints is None:
        raise NotFound

    items_per_page = request.args.get("items_per_page", default=10, type=int)
    search = request.args.get("search", default="")

    results = []
    more_available = False
    _, footprint_libraries = footprints

    res = search.split(":")
    match res:
        case [lib, id]:
            for nick, fp_lib in footprint_libraries:
                # Search query contains a library name
                if nick.name.casefold() != lib.casefold():
                    continue

                for footprint in fp_lib.footprints:
                    # print(f"{id.casefold()} in {symbol.library_id.casefold()} = {id.casefold() in symbol.library_id.casefold()}")
                    if id.casefold() in footprint.library_id.casefold():
                        results.append(
                            {
                                **FootprintSchema().dump(footprint),  # type: ignore
                                "ref": f"{nick.name}:{footprint.library_id}",
                            }
                        )
                    # Number of symbols in library > items_per_page
                    if len(results) >= items_per_page:
                        more_available = True
                        break
        case [id]:
            for nick, fp_lib in footprint_libraries:
                for footprint in fp_lib.footprints:
                    if (
                        id.casefold() in footprint.library_id.casefold()
                        or id.casefold() in nick.name.casefold()
                    ):
                        results.append(
                            {
                                **FootprintSchema().dump(footprint),  # type: ignore
                                "ref": f"{nick.name}:{footprint.library_id}",
                            }
                        )
                    # Number of symbols in library > items_per_page
                    if len(results) >= items_per_page:
                        more_available = True
                        break
        case _:
            more_available = True
            results = []

    return jsonify({"results": results, "more_available": more_available})
