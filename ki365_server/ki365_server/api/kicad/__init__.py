import os
from pprint import pprint
from typing import Any, Type
import sqlalchemy
from werkzeug.exceptions import NotFound, BadRequest
from flask import Blueprint, jsonify, request
from sqlalchemy.orm import scoped_session
import json

from slugify import slugify

from ki365_common.integrations.kicad.kicad_dbl import (
    KicadDbl,
    KicadDblLibrary,
    KicadDblSchema,
)
from ki365_common.integrations.kicad.model import KicadPart, create_mapping_from_dbl
from ki365_common.schemas.kicad import DatabaseSchema, LibraryPartSchema, LibrarySchema

from ki365_common.models.part import Part
from .redis import KICAD_CONFIG_DIR

from ...db import db

bp = Blueprint("kicad", __name__, url_prefix="/kicad")

MAX_ITEMS_PER_PAGE = 100


# /kicad/symbols
from .symbols import bp as symbols_bp, load_sym_libraries

bp.register_blueprint(symbols_bp)

# /kicad/footprints
from .footprints import bp as footprints_bp, load_fp_libraries

bp.register_blueprint(footprints_bp)


mappings: dict[str, tuple[KicadDblLibrary, Type[KicadPart]]] = {}
kicad_dbl: KicadDbl | None = None

KICAD_DBL_PATH = os.environ.get(
    "KICAD_DBL_PATH", default=os.path.join(KICAD_CONFIG_DIR, "ki365.kicad_dbl")
)


def load_dbl_library():
    global kicad_dbl
    print(f"Loading {KICAD_DBL_PATH}...")
    with open(KICAD_DBL_PATH) as kicad_dbl_json:
        kicad_dbl_json = json.load(kicad_dbl_json)
        kicad_dbl = KicadDblSchema().load(kicad_dbl_json)  # type: ignore
        if kicad_dbl is not None:
            if kicad_dbl.slug is None:
                kicad_dbl.slug = slugify(kicad_dbl.name)

            for lib in kicad_dbl.libraries:
                if lib.slug is None:
                    lib.slug = slugify(lib.name)
                print(
                    f"Mapping library '{lib.name}' as 'kicad/libraries/{lib.slug}'..."
                )

                # Remove part_id as a field
                lib.fields = [f for f in lib.fields if f.column != "part_id"]
                for field in lib.fields:
                    if field.key is None:
                        field.key = slugify(field.name, separator="_")
                        print(f"\tField '{field.name}' as '{field.key}'...")

                # Create dynamic table
                Cls = create_mapping_from_dbl(lib)
                mappings[lib.slug] = (lib, Cls)


# Load on startup
def load_libraries():
    load_dbl_library()
    load_sym_libraries()
    load_fp_libraries()


@bp.route("/reload", methods=["POST"])
def reload_libraries():
    load_libraries()
    return jsonify({})


@bp.route("/libraries", methods=["GET"])
@bp.route("/libraries/", methods=["GET"])
def list_libraries():
    if kicad_dbl is None:
        raise NotFound

    res = DatabaseSchema().dump(kicad_dbl)
    return jsonify(res)


@bp.route("/libraries/<slug>", methods=["GET", "POST"])
@bp.route("/libraries/<slug>/", methods=["GET", "POST"])
def list_library_parts(slug: str):
    if slug not in mappings:
        raise NotFound("Library not found")
    else:
        session: scoped_session = db.session
        _name, LibPart = mappings[slug]

        if request.method == "POST":
            part_info_json = request.get_json()
            part_info: dict[str, Any] = LibraryPartSchema().load(part_info_json)  # type: ignore

            # Check if reference exists
            q = session.query(LibPart).where(
                LibPart.reference == part_info["reference"]
            )
            part_exists: bool = session.query(q.exists()).scalar()
            if part_exists:
                raise BadRequest("Reference already exists")

            # Save part
            part = LibPart(**part_info)  # type: ignore
            session.add(part)
            session.commit()

            res = LibraryPartSchema().dump(part)
        else:
            page = request.args.get("page", default=1, type=int)
            items_per_page = request.args.get("items_per_page", default=100, type=int)
            search = request.args.get("search")
            sort_key = request.args.get("sort_key")
            sort_order = request.args.get("sort_order")

            # When 'all' is selected
            if items_per_page == -1:
                items_per_page = MAX_ITEMS_PER_PAGE

            # Search part info if a search parameter is provided
            stmt = sqlalchemy.select(LibPart)
            if search is not None:
                stmt = stmt.filter(
                    sqlalchemy.or_(
                        LibPart.reference.ilike(f"%{search}%"),
                        LibPart.description.ilike(f"%{search}%"),
                        LibPart.keywords.ilike(f"%{search}%"),
                    )
                )

            # Order result based on key and order
            sortable = {
                "reference": LibPart.reference,
            }
            sort = sortable.get(sort_key or "", LibPart.reference)
            stmt = stmt.order_by(sort.desc() if sort_order == "desc" else sort.asc())

            # Paginate the response
            page_data = db.paginate(
                stmt,
                page=page,
                per_page=items_per_page,
                max_per_page=MAX_ITEMS_PER_PAGE,
            )

            res = {
                "parts": LibraryPartSchema(many=True).dump(page_data.items),
                "total": page_data.total,
            }

        return jsonify(res)


@bp.route("/libraries/<slug>/<reference>", methods=["GET", "PUT", "DELETE"])
@bp.route("/libraries/<slug>/<reference>/", methods=["GET", "PUT", "DELETE"])
def library_part_detail(slug: str, reference: str):
    if slug not in mappings:
        raise NotFound(f"Library '{slug}' not found")
    else:
        session: scoped_session = db.session
        _name, LibPart = mappings[slug]
        part: KicadPart | None = (
            session.query(LibPart).where(LibPart.reference == reference).one_or_none()
        )

        if part is None:
            raise NotFound(f"Part '{reference}' not found in '{slug}'")

        if request.method == "DELETE":
            res = LibraryPartSchema().dump(part)
            session.delete(part)
            session.commit()

        elif request.method == "PUT":
            part_info = request.get_json()
            x: dict[str, Any] = LibraryPartSchema(partial=True).load(part_info)  # type: ignore

            # Update symbol and footprint
            new_reference = x.get("reference")
            print(f"{new_reference}  {part.reference}")
            if new_reference is not None and new_reference != part.reference:
                q = session.query(LibPart).where(LibPart.reference == new_reference)
                part_exists: bool = session.query(q.exists()).scalar()
                if part_exists:
                    raise BadRequest("Reference already exists")

            part.symbols = x.get("symbols", part.symbols)
            part.footprints = x.get("footprints", part.footprints)

            # Update properties
            part.description = x.get("description", part.description)
            part.keywords = x.get("keywords", part.keywords)
            part.exclude_from_bom = x.get("exclude_from_bom", part.exclude_from_bom)
            part.exclude_from_board = x.get(
                "exclude_from_board", part.exclude_from_board
            )

            try:
                fields: dict[str, str] | None = x.get("fields")
                if fields is not None:
                    part_fields = part.fields
                    part_fields.update(fields)
                    part.fields = part_fields

            except KeyError as kerr:
                raise BadRequest(f'Invalid field(s) {", ".join(kerr.args)}')

            session.add(part)
            session.commit()

            res = LibraryPartSchema().dump(part)
        else:
            res = LibraryPartSchema().dump(part)

        return jsonify(res)


@bp.route("/by-part-id/<int:part_id>")
def list_library_parts_by_partid(part_id: int):
    res = []
    session: scoped_session = db.session

    q = session.query(Part).where(Part.id == part_id)
    part_exists: bool = session.query(q.exists()).scalar()

    if part_exists:
        for lib, LibPart in mappings.values():
            parts = session.query(LibPart).where(LibPart.part_id == part_id).all()
            if len(parts) > 0:
                res.append(
                    {
                        "library": LibrarySchema().dump(lib),
                        "parts": LibraryPartSchema(many=True).dump(parts),
                    }
                )
    return jsonify(res)
