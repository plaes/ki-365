from http.client import ACCEPTED
import os
from flask import Blueprint, jsonify, request, redirect
from ki365_server.workers.kicad import export_symbol
from redis import Redis
from rq.queue import Queue
from werkzeug.exceptions import (
    NotFound,
    ServiceUnavailable,
    InternalServerError,
    BadRequest,
)
from ki365_common.integrations.kicad.library_table import (
    LibraryTable,
    LibraryTableItem,
    LibraryTableItemSchema,
    LibraryTableSchema,
)

from ki365_common.integrations.kicad.symbols import (
    SymbolLibrary,
    SymbolLibrarySchema,
    SymbolSchema,
    load_symbol_libraries,
)

from marshmallow import Schema, fields

bp = Blueprint("symbols", __name__, url_prefix="/symbols")

from ki365_server.api.kicad.redis import (
    KICAD_CACHE_EXPIRE,
    KICAD_CONFIG_DIR,
    kicad_queue,
    redis,
)

KICAD_SYM_LIB_TABLE_PATH = os.environ.get(
    "KICAD_SYM_LIB_TABLE_PATH", default=os.path.join(KICAD_CONFIG_DIR, "sym-lib-table")
)


class SymbolExportSchema(Schema):
    library = fields.Str()
    symbol = fields.Str(required=True)


symbols: tuple[LibraryTable, list[tuple[LibraryTableItem, SymbolLibrary]]] | None = None


def load_sym_libraries():
    global symbols
    print(f"Loading {KICAD_SYM_LIB_TABLE_PATH}, this might take a while...")
    symbols = load_symbol_libraries(KICAD_SYM_LIB_TABLE_PATH)
    if symbols is None:
        print("No symbol libraries loaded!")
    else:
        print("Done")


@bp.route("", methods=["GET"])
@bp.route("/", methods=["GET"])
def list_libraries():
    if symbols is None:
        raise NotFound

    sym_lib_table, _symbol_libraries = symbols
    res = LibraryTableSchema().dump(sym_lib_table)
    return jsonify(res)


@bp.route("/all", methods=["GET"])
@bp.route("/all/", methods=["GET"])
def list_all_libraries():
    if symbols is None:
        raise NotFound

    search = request.args.get("search", default="")

    _sym_lib_table, symbol_libraries = symbols
    res = []
    for lib, symbol_lib in symbol_libraries:
        filtered_symbols = [
            s
            for s in symbol_lib.symbols
            if search.casefold() in s.library_id.casefold()
            or search.casefold() in (s.description or "").casefold()
        ]
        if len(filtered_symbols) > 0:
            res.append(
                {
                    **LibraryTableItemSchema().dump(lib),  # type: ignore
                    "symbols": SymbolSchema(many=True).dump(filtered_symbols),
                }
            )
    return jsonify(res)


@bp.route("/<library>", methods=["GET"])
def list_library_symbols(library: str):
    if symbols is None:
        raise NotFound

    _sym_lib_table, symbol_libraries = symbols
    sym_lib = next(
        ((sym_lib) for nick, sym_lib in symbol_libraries if nick.slug == library),
        None,
    )
    if sym_lib is not None:
        res = SymbolLibrarySchema().dump(sym_lib)
        return jsonify(res)
    else:
        raise NotFound("Library not found")


@bp.route("/export", methods=["GET"])
def export_library_symbols():
    if symbols is None:
        raise NotFound

    library = request.args.get("library")
    symbol = request.args.get("symbol")

    url_hash = f"symbol:{library}:{symbol}"
    rq_job_id = redis.hget(url_hash, "job_id")
    if rq_job_id is not None:
        job_id = rq_job_id.decode()
        job_hash = f"rq:job:{job_id}"
        ttl = redis.ttl(job_hash)
        if ttl:
            # Add 30 more seconds of buffer room
            # to ensure job.result doesn't get deleted pre-maturely
            redis.expire(job_hash, KICAD_CACHE_EXPIRE)
        else:
            # Job result has already been deleted, clear lookup hash
            redis.delete(url_hash)
    else:
        # Start job
        job = kicad_queue.enqueue_call(
            func=export_symbol, args=(library, symbol), result_ttl=KICAD_CACHE_EXPIRE
        )
        # Create job.id lookup using hash as key (for cache)
        if redis.hsetnx(url_hash, "job_id", job.id):
            redis.expire(url_hash, KICAD_CACHE_EXPIRE)
        job_id = job.get_id()

    # Redirect to job page
    return redirect(f"./export/{job_id}")


@bp.route("/export/<job_id>", methods=["GET"])
def get_export_library_symbols(job_id: str):
    job = kicad_queue.fetch_job(job_id)
    if job is None:
        raise BadRequest("Invalid job id")
    if job.is_canceled:
        raise InternalServerError(description="Job was cancelled")
    elif job.is_failed:
        raise InternalServerError(description="Job failed")
    elif not job.is_finished:
        return jsonify(status=str(job.get_status())), ACCEPTED
    else:
        res = job.result
        if res is not None:
            return jsonify(res)
        else:
            raise BadRequest("Invalid job id or wrong endpoint.")


@bp.route("/find", methods=["GET"])
def search_library_symbols():
    if symbols is None:
        raise NotFound

    items_per_page = request.args.get("items_per_page", default=10, type=int)
    search = request.args.get("search", default="")

    results = []
    more_available = False
    _, symbol_libraries = symbols

    res = search.split(":")
    match res:
        case [lib, id]:
            for nick, sym_lib in symbol_libraries:
                # Search query contains a library name
                if nick.name.casefold() != lib.casefold():
                    continue

                for symbol in sym_lib.symbols:
                    # print(f"{id.casefold()} in {symbol.library_id.casefold()} = {id.casefold() in symbol.library_id.casefold()}")
                    if id.casefold() in symbol.library_id.casefold():
                        results.append(
                            {
                                **SymbolSchema().dump(symbol),  # type: ignore
                                "ref": f"{nick.name}:{symbol.library_id}",
                            }
                        )
                    # Number of symbols in library > items_per_page
                    if len(results) >= items_per_page:
                        more_available = True
                        break
        case [id]:
            for nick, sym_lib in symbol_libraries:
                for symbol in sym_lib.symbols:
                    if (
                        id.casefold() in symbol.library_id.casefold()
                        or id.casefold() in nick.name.casefold()
                    ):
                        results.append(
                            {
                                **SymbolSchema().dump(symbol),  # type: ignore
                                "ref": f"{nick.name}:{symbol.library_id}",
                            }
                        )
                    # Number of symbols in library > items_per_page
                    if len(results) >= items_per_page:
                        more_available = True
                        break
        case _:
            more_available = True
            results = []

    return jsonify({"results": results, "more_available": more_available})
