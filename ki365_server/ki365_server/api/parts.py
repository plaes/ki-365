import pprint
from typing import Any
from flask import Blueprint, jsonify, request
import sqlalchemy

from werkzeug.exceptions import NotFound

from ki365_common.models.manufacturers import ManufacturerPart
from ki365_common.models.part import Part
from ki365_common.schemas.assemblies import AssemblySchema
from ki365_common.schemas.manufacturer import ManufacturerPartSchema
from ki365_common.schemas.part import PartSchema

from .api_errors import IdNotFound
from ..db import db

bp = Blueprint("parts", __name__, url_prefix="/parts")

MAX_ITEMS_PER_PAGE=100


# Get part information
@bp.route("/<int:part_id>", methods=["GET", "POST", "DELETE"])
def part(part_id: int):
    session = db.session
    part: Part | None = session.query(Part).where(Part.id == part_id).one_or_none()
    if part:
        if request.method == "POST":
            update_part_info = request.get_json()
            # Create new part
            res: dict[str, Any] = PartSchema(partial=True).load(update_part_info)
            # Update part info
            # with session.begin():
            part.title = res.get("title", part.title)
            part.description = res.get("description", part.description)
            part.status = res.get("status", part.status)
            session.add(part)
            session.commit()
        elif request.method == "DELETE":
            session.delete(part)
            session.commit()

        # Respond with info
        res = PartSchema().dump(part)
        return jsonify(res)
    else:
        raise IdNotFound(part_id)


# Liat all parts or insert a new part
@bp.route("", methods=["GET", "PUT"])
@bp.route("/", methods=["GET", "PUT"])
def list_parts():
    session = db.session

    if request.method == "PUT":
        create_part = request.get_json()
        # Create new part
        res = PartSchema().load(create_part, partial=False)
        part = Part(
            **res,
            manufacturer_parts=[],
        )
        session.add(part)
        session.commit()

        response = PartSchema().dump(part)
    else:
        page = request.args.get("page", default=1, type=int)
        items_per_page = request.args.get("items_per_page", default=100, type=int)
        search = request.args.get("search")
        sort_key = request.args.get("sort_key")
        sort_order = request.args.get("sort_order")

        # When 'all' is selected
        if items_per_page == -1:
            items_per_page = MAX_ITEMS_PER_PAGE

        # Search part info if a search parameter is provided
        select = session.query(Part)
        if search is not None:
            select = select.filter(
                sqlalchemy.or_(
                    Part.title.ilike(f"%{search}%"),
                    Part.description.ilike(f"%{search}%"),
                )
            )

        # Order result based on key and order
        sortable = {
            "id": Part.id,
            "title": Part.title,
            "description": Part.description,
            "status": Part.status,
        }
        sort = sortable.get(sort_key, Part.id)
        select = select.order_by(sort.desc() if sort_order == "desc" else sort.asc())

        # Paginate the response
        page = db.paginate(
            select,
            page=page,
            per_page=items_per_page,
            max_per_page=MAX_ITEMS_PER_PAGE,
        )

        response = {
            "parts": PartSchema(many=True).dump(page.items),
            "total": page.total,
        }
    return jsonify(response)


@bp.route("/<int:part_id>/manufacturers", methods=["GET", "PUT"])
def part_manufacturer_parts(part_id: int):
    session = db.session
    part: Part | None = session.query(Part).where(Part.id == part_id).one_or_none()
    if part:
        if request.method == "PUT":
            mfr_part_info = request.get_json()
            # Create new part
            res: dict[str, Any] = ManufacturerPartSchema().load(mfr_part_info)
            # Update part info
            mfr_part = ManufacturerPart(
                **res
            )
            # with session.begin():
            part.manufacturer_parts.append(mfr_part)
            session.add_all([part, mfr_part])
            session.commit()
            res = ManufacturerPartSchema().dump(mfr_part)
        else:
            # Respond with info
            res = ManufacturerPartSchema(many=True).dump(part.manufacturer_parts)
    else:
        raise IdNotFound(part_id)
    return jsonify(res)

@bp.route("/<int:part_id>/assembly")
def part_assembly(part_id: int):
    session = db.session
    part: Part | None = session.query(Part).where(Part.id == part_id).one_or_none()
    if part:
        if part.assembly is None:
            raise NotFound(description="Part has no assembly")
        res = AssemblySchema().dump(part.assembly)
    else:
        raise IdNotFound(part_id)
    return jsonify(res)