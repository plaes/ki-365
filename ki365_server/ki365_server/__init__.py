from datetime import timedelta
import os

from flask import Flask
from flask_cors import CORS # type: ignore

from dotenv import load_dotenv

print(f"Loading dotenv {os.getcwd()}")
load_dotenv()
print(f"{os.environ.get('KICAD_DBL_PATH')}")
from ki365_server.api.kicad import load_libraries

def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    database_uri = os.environ.get("DATABASE_URL")
    app.config.from_mapping(
        SECRET_KEY=os.environ.get("FLASK_SECRET"),
        # Database
        SQLALCHEMY_DATABASE_URI=database_uri,
        SQLALCHEMY_BINDS={
            "users": os.environ.get("USER_DATABASE_URL", database_uri),
            # "kicad": os.environ.get("KICAD_DATABASE_URL", database_uri),
            # "freecad": os.environ.get("KICAD_DATABASE_URL", database_uri),
        },
        # JWT
        JWT_SECRET_KEY=os.environ.get("JWT_SECRET_KEY"),
        JWT_TOKEN_LOCATION=["cookies"],
        JWT_ACCESS_TOKEN_EXPIRES=timedelta(hours=1),
        JWT_COOKIE_SECURE=False,  # TODO
        JWT_CSRF_IN_COOKIES = False,
    )

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile("config.py", silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    # Enable CORS
    cors = CORS(app, resources={r"/*": {"origins": "*"}}, supports_credentials=True)

    # Add auth blueprint
    from . import auth

    auth.init_app(app)

    # Initialize database
    from . import db

    db.init_app(app)

    # Add api blueprint

    from . import api

    app.register_blueprint(api.bp)

    # Add frontend blueprint

    from . import ui

    app.register_blueprint(ui.bp)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    # Load libraries
    load_libraries()

    return app


def main():
    app = create_app()
    app.run(debug=True)
