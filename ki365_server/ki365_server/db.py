from flask import Flask
from flask_sqlalchemy import SQLAlchemy

import click

import migrations 

db = SQLAlchemy()


def init_app(app: Flask):
    db.init_app(app)
    app.cli.add_command(db_current_cmd) # type: ignore
    app.cli.add_command(db_upgrade_cmd) # type: ignore
    app.cli.add_command(db_downgrade_cmd) # type: ignore


@click.command() # type: ignore
@click.option("-v", "--verbose", is_flag=True, default=False, help="Verbose mode")
def db_current_cmd(verbose):
    """Display current database revision"""
    migrations.current(verbose)


@click.command() # type: ignore
@click.option("-r", "--revision", default="head", help="Revision target")
def db_upgrade_cmd(revision):
    """Upgrade to a later database revision"""
    migrations.upgrade(revision)


@click.command() # type: ignore
@click.option("-r", "--revision", required=True, help="Revision target")
def db_downgrade_cmd(revision):
    """Revert to a previous database revision"""
    migrations.downgrade(revision)