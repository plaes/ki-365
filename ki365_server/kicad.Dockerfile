FROM kicad/kicad:nightly
WORKDIR /worker
USER root

RUN apt-get update && apt-get install -y \
    python3-pip \
    python3-poetry \
    python3-venv

## BACKEND
# Copy backend files
COPY . ./
COPY .env.production .env

# Do not install in a virtual env
RUN poetry config virtualenvs.in-project true
# RUN poetry config virtualenvs.create false 

# Install dependencies
RUN python3 -m venv --system-site-packages .venv
RUN poetry install
RUN cd ki365_common && poetry install
RUN cd ki365_server && poetry install

RUN ["chmod", "+x", "./ki365_server/ki365_server/workers/kicad/entrypoint.sh"]

USER kicad
ENV KICAD_CLI_EXECUTABLE=kicad-cli
ENTRYPOINT ["./ki365_server/ki365_server/workers/kicad/entrypoint.sh"]
CMD [ "ki365_server.workers.kicad" ]
