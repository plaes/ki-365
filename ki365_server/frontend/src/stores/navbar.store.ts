import { defineStore } from 'pinia'

function get_stored_value(): boolean | null {
    let stored = localStorage.getItem('navbar_rail')
    if (stored) {
        return !!JSON.parse(stored)
    }
    return null
}

function set_stored_value(rail: boolean | null) {
    localStorage.setItem('navbar_rail', JSON.stringify(rail))
}

// User authentication store
export const useNavbarStore = defineStore('navbar', {
    state: () => ({
        rail: get_stored_value()
    }),
    getters: {
        is_rail: (state) => state.rail
    },
    actions: {
        set_hidden(val: boolean) {
            this.rail = val
            set_stored_value(val)
        },
        toggle() {
            this.set_hidden(!this.rail)
        },
        hide() {
            this.set_hidden(true)
        },
        show() {
            this.set_hidden(false)
        }
    },
})