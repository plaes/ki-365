import axios, { AxiosInstance } from 'axios';
import { defineStore } from 'pinia'

export const auth_client: AxiosInstance = axios.create({
    baseURL: import.meta.env.VITE_AUTH_URL,
    withCredentials: true
});

export interface User {
    id?: number
    name: string
    is_engineer?: boolean
    is_librarian?: boolean
    is_admin?: boolean
}

interface LoginResponse {
    user?: User
    success: boolean
    change_required: boolean
    csrf_access_token: string
}

function get_stored_user(): User | null {
    let stored = localStorage.getItem('user')
    if (stored) {
        return JSON.parse(stored)
    }
    return null
}

function get_stored_csrf(): string | null {
    let stored = localStorage.getItem('csrf_access_token')
    if (stored) {
        return JSON.parse(stored)
    }
    return null
}

function set_stored_user(user: User | null) {
    localStorage.setItem('user', JSON.stringify(user))
}

function set_stored_csrf(csrf: string | null) {
    localStorage.setItem('csrf_access_token', JSON.stringify(csrf))
}


// User authentication store
export const useAuthStore = defineStore('auth', {
    state: () => ({
        authenticate: false,
        csrf_access_token: get_stored_csrf(),
        user: get_stored_user()
    }),
    getters: {
        username: (state) => state.user?.name,
        user_id: (state) => state.user?.id,
        should_authenticate: (state) => state.authenticate
    },
    actions: {
        request_authentication() {
            this.authenticate = true
        },
        /**
         * 
         * @param username 
         * @param password 
         * @param return_url 
         * @returns true if a password change is necessary, false otherwise
         */
        async login(username: string, password: string): Promise<boolean> {
            let response = await auth_client.post<LoginResponse>('/login', { username, password })

            let data = response.data
            if (data.user && data.change_required) {
                this.user = data.user
                return true
            } else if (data.user && data.success) {
                this.user = data.user
                this.csrf_access_token = data.csrf_access_token
                set_stored_user(data.user)
                set_stored_csrf(data.csrf_access_token)
                this.authenticate = false
            } else {
                throw Error("Failed to log in")
            }
            return false
        },
        async logout() {
            this.user = null
            set_stored_user(null)
            let _response = await auth_client.post('/logout')
        },
        async change_username(new_username: string) {
            // Update user
            if (this.user) {
                let response = await auth_client.put<string>('/username', { new_username })

                this.user.name = response.data
                set_stored_user(this.user)
            }
        },
        async change_password(username: string, new_password: string, old_password?: string) {
            let response = await auth_client.post<LoginResponse>('/change-password', { username, old_password, new_password })

            // Update user
            let data = response.data as LoginResponse
            if (data.user && data.success) {
                // Clear stored user
                this.user = data.user
                this.csrf_access_token = data.csrf_access_token
                set_stored_user(data.user)
                set_stored_csrf(data.csrf_access_token)
                this.authenticate = false
            } else {
                throw Error("Failed to change password")
            }
        },

    },
})