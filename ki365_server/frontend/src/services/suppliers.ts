import http from '@/http_common';
import { SupplierPartStatus, SupplierStatus } from 'frontend_common/src/status';
import { } from 'axios';

export interface Supplier {
  id?: number;
  name: string;
  link?: string,
  description: string;
  status: SupplierStatus;
}

export interface RequestInterface {
  page: number,
  per_page: number,
  total: number,
  total_pages: number,
  suppliers: Supplier[]
}

export interface SupplierPart {
  id?: number;
  supplier_id?: number,
  supplier?: Supplier,
  partno: string,
  status: SupplierPartStatus;
}

export abstract class SuppliersService {
  static async get_all(params: {
    page?: number,
    items_per_page?: number,
    sort_key?: string,
    sort_order?: string,
    search?: string
  }): Promise<RequestInterface> {
    let response = await http.get<RequestInterface>('/suppliers', { params });
    return response.data
  }

  static async create(data: Supplier): Promise<Supplier> {
    delete data.id;
    let response = await http.put("/suppliers", data);
    return response.data
  }

  static async update(id: number, data: Supplier): Promise<Supplier> {
    delete data.id;
    let response = await http.post(`/suppliers/${id}`, data);
    return response.data
  }

  static async delete(id: number): Promise<Supplier> {
    let response = await http.delete(`/suppliers/${id}`);
    return response.data
  }
}

export abstract class SupplierPartService {
  static async update(id: number, data: SupplierPart): Promise<SupplierPart> {
    delete data.id;
    let response = await http.post(`/suppliers/parts/${id}`, data);
    return response.data
  }

  static async delete(id: number): Promise<Supplier> {
    let response = await http.delete(`/suppliers/parts/${id}`);
    return response.data
  }
}
