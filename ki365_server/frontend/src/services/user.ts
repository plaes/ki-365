import apiClient from "@/http_common";
import { User } from "@/stores/auth.store";

export abstract class UserService {
    static async create(data: User): Promise<User> {
        let response = await apiClient.post<User>(`/users/`, data)
        return response.data
    }

    static async update(id: number, data: User): Promise<User> {
        delete data.id
        let response = await apiClient.put<User>(`/users/${id}`, data)
        return response.data
    }

    static async delete(id: number): Promise<User> {
        let response = await apiClient.delete<User>(`/users/${id}`)
        return response.data
    }

    static async reset_password(id: number): Promise<string> {
        let response = await apiClient.delete<{msg: string}>(`/users/${id}/password`)
        return response.data.msg
    }

    static async get_all_users(): Promise<User[]> {
        let response = await apiClient.get<User[]>('/users')
        return response.data
    }
}