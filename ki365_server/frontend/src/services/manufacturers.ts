import http from '@/http_common';
import { } from 'axios';
import { SupplierPart } from './suppliers';
import { ManufacturerPartStatus, ManufacturerStatus } from 'frontend_common/src/status';




export interface Manufacturer {
  id?: number;
  name: string;
  link?: string,
  description: string;
  status: ManufacturerStatus
}

export interface RequestInterface {
  page: number,
  per_page: number,
  total: number,
  total_pages: number,
  manufacturers: Manufacturer[]
}

export interface ManufacturerPart {
  id?: number,
  manufacturer_id?: number,
  manufacturer?: Manufacturer,
  partno: string,
  status?: ManufacturerPartStatus,
  supplier_parts?: SupplierPart[]
}


export abstract class ManufacturersService {
  static async get_all(params: {
    page?: number,
    items_per_page?: number,
    sort_key?: string,
    sort_order?: string,
    search?: string
  }): Promise<RequestInterface> {
    let response = await http.get<RequestInterface>('/manufacturers', { params });
    return response.data
  }

  static async create(data: Manufacturer): Promise<Manufacturer> {
    delete data.id;
    let response = await http.put("/manufacturers", data);
    return response.data
  }

  static async update(id: number, data: Manufacturer): Promise<Manufacturer> {
    delete data.id;
    let response = await http.post(`/manufacturers/${id}`, data);
    return response.data
  }

  static async delete(id: number): Promise<Manufacturer> {
    let response = await http.delete(`/manufacturers/${id}`);
    return response.data
  }

  static async create_supplier_part(id: number, data: SupplierPart): Promise<SupplierPart> {
    delete data.id;
    let response = await http.put(`/manufacturers/parts/${id}/manufacturers`, data);
    return response.data
  }
}


export abstract class ManufacturerPartService {
  static async update(id: number, data: ManufacturerPart): Promise<ManufacturerPart> {
    delete data.id;
    let response = await http.post(`/manufacturers/parts/${id}`, data);
    return response.data
  }

  static async delete(id: number): Promise<ManufacturerPart> {
    let response = await http.delete(`/manufacturers/parts/${id}`);
    return response.data
  }

  static async create_supplier_part(id: number, data: SupplierPart): Promise<SupplierPart> {
    delete data.id;
    let response = await http.put(`/manufacturers/parts/${id}/suppliers`, data);
    return response.data
  }
}
