import http from '@/http_common';
import { } from 'axios';


export interface LibraryField {
  name: string,
  key: string,
  required?: boolean
  hint?: string,
  unit?: string,

}

export interface Library {
  name: string,
  slug: string,
  fields: LibraryField[]
}

export interface Database {
  name: string,
  slug: string,
  description: string,
  libraries: Library[]
}

export interface KicadPart {
  id?: number,
  part_id: number,
  //
  reference: string,
  symbols?: string,
  footprints?: string,
  //
  description?: string,
  keywords?: string,
  exclude_from_bom?: boolean,
  exclude_from_board?: boolean,
  fields: {
    [column: string]: string
  }
}

export interface KicadPartRequest {
  total: number,
  parts: KicadPart[]
}

export interface KicadSymbolLibrary {
  name: string,
  slug: string,
  description?: string,
  symbols?: KicadSymbol[]
}

export interface KicadSymbol {
  ref?: string
  library_id: string,
  footprint?: string,
  description?: string,
  fp_filters?: string,
}

export interface KicadSymbolFindResults {
  more_available: boolean
  results: KicadSymbol[]
}

export interface KicadFootprintLibrary {
  name: string,
  slug: string,
  description?: string,
  footprints?: KicadFootprint[]
}

export interface KicadFootprint {
  ref?: string
  library_id: string,
  description?: string,
}

export interface KicadFootprintFindResults {
  more_available: boolean
  results: KicadFootprint[]
}

export abstract class KicadLibrariesService {
  static async get_all(): Promise<Database> {
    let response = await http.get<Database>('/kicad/libraries');
    return response.data
  }
}

export abstract class KicadPartService {
  static async get_all(library: string, params: {
    page?: number,
    items_per_page?: number,
    sort_key?: string,
    sort_order?: string,
    search?: string
  }): Promise<KicadPartRequest> {
    let response = await http.get<KicadPartRequest>(`/kicad/libraries/${library}`, { params });
    return response.data
  }

  static async create(library: string, data: KicadPart): Promise<KicadPart> {
    delete data.id;
    let response = await http.post(`/kicad/libraries/${library}`, data);
    return response.data
  }

  static async update(library: string, part: string, data: KicadPart): Promise<KicadPart> {
    delete data.id;
    let response = await http.put(`/kicad/libraries/${library}/${part}`, data);
    return response.data
  }

  static async delete(library: string, part: string): Promise<Library> {
    let response = await http.delete(`/kicad/libraries/${library}/${part}`);
    return response.data
  }
}

export abstract class KicadSymbolService {
  static async all(params: { search?: string, items_per_page?: number }): Promise<KicadSymbolLibrary[]> {
    let response = await http.get<KicadSymbolLibrary[]>('/kicad/symbols/all', { params });
    return response.data
  }

  static async find(params: { search?: string, items_per_page?: number }): Promise<KicadSymbolFindResults> {
    let response = await http.get<KicadSymbolFindResults>('/kicad/symbols/find', { params });
    return response.data
  }

  static async export_svg(params: { library: string, symbol: string }): Promise<string[]> {
    let response = await http.get<string[]>('/kicad/symbols/export', { params });
    // Retry once every 1000ms until when API returns '202 Accepted'
    while (response.status == 202) {
      await new Promise(f => setTimeout(f, 1000));
      response = await http.get<string[]>('/kicad/symbols/export', { params });
    }
    return response.data
  }

}

export abstract class KicadFootprintService {
  static async all(params: { search?: string, items_per_page?: number }): Promise<KicadFootprintLibrary[]> {
    let response = await http.get<KicadFootprintLibrary[]>('/kicad/footprints/all', { params });
    return response.data
  }

  static async find(params: { search?: string, items_per_page?: number }): Promise<KicadFootprintFindResults> {
    let response = await http.get<KicadFootprintFindResults>('/kicad/footprints/find', { params });
    return response.data
  }

  static async export_svg(params: { library: string, footprint: string }): Promise<string[]> {
    let response = await http.get<string[]>('/kicad/footprints/export', { params });
    // Retry once every 1000ms until when API returns '202 Accepted'
    while (response.status == 202) {
      await new Promise(f => setTimeout(f, 1000));
      response = await http.get<string[]>('/kicad/footprints/export', { params });
    }
    return response.data
  }
}
