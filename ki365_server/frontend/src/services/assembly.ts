


import http from '@/http_common';
import { } from 'axios';
import { Part } from './parts';
import { AssemblyRevisionStatus } from 'frontend_common/src/status';

export interface AssemblyField {
    id: number,
    name: string,
    value: string
}

export interface Assembly {
    id: number,
    part: Part,
    revisions: AssemblyRevision[]
}

export interface AssemblyRevision {
    id?: number,
    revision: string,
    revision_slug?: string,
    status: AssemblyRevisionStatus,
    parts?: AssemblyRevisionPart[]
    fields?: AssemblyField[]
}

export interface AssemblyRevisionPart {
    id?: number,
    reference: string,
    part?: Part,
    part_id?: number,
    fields?: AssemblyField[]
}

export interface AssemblyRevisionPartGroup {
    id: number,
    part?: Part,
    items: AssemblyRevisionPart[],
}

export interface AssemblySearchResult {
    total: number,
    assemblies: Assembly[]
}

export abstract class AssemblyService {
    static async get_all(params: {
        page: number,
        items_per_page?: number,
        sort_key?: string,
        sort_order?: string,
        search?: string
    }): Promise<AssemblySearchResult> {
        let response = await http.get<AssemblySearchResult>('/assemblies', { params });
        return response.data
    }

    static async get(id: number): Promise<Assembly> {
        let response = await http.get(`/assemblies/${id}`);
        return response.data
    }

    static async create(data: Part): Promise<Assembly> {
        delete data.id;
        let response = await http.post("/assemblies", data);
        return response.data
    }

    static async update(id: number, data: Part): Promise<Assembly> {
        delete data.id;
        let response = await http.put(`/assemblies/${id}`, data);
        return response.data
    }

    static async delete(id: number): Promise<Assembly> {
        let response = await http.delete(`/assemblies/${id}`);
        return response.data
    }

    static async create_revision(id: number, data: AssemblyRevision): Promise<AssemblyRevision> {
        delete data.id;
        delete data.parts;
        let response = await http.post(`/assemblies/${id}`, data);
        return response.data
    }
}

export abstract class AssemblyRevisionService {
    static async update(id: number, rev: string, data: AssemblyRevision): Promise<AssemblyRevision> {
        let response = await http.put(`/assemblies/${id}/${rev}`, data);
        return response.data
    }

    static async create_part(id: number, rev: string, data: AssemblyRevisionPart): Promise<AssemblyRevisionPart> {
        let response = await http.post(`/assemblies/${id}/${rev}/parts`, data);
        return response.data
    }

    static async delete_part(id: number, rev: string, data: AssemblyRevisionPart): Promise<AssemblyRevisionPart> {
        let response = await http.delete(`/assemblies/${id}/${rev}/parts/${data.id}`);
        return response.data
    }

    static async get(id: number, rev: string): Promise<AssemblyRevision> {
        let response = await http.get(`/assemblies/${id}/${rev}`);
        return response.data
    }

}
