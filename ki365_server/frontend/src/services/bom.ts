import { Part } from "./parts";

export type { Bom } from 'frontend_common'


export const currencies = ['EUR', 'USD', 'SEK']
export type BomCurrency = typeof currencies[number]

/**
 * Get the default currency. Defaults to EURO but can be overriden with BOM_DEFAULT_CURRENCY env variable
 * @returns default currency to use
 */
export function get_default_currency(): BomCurrency {
    return import.meta.env.BOM_DEFAULT_CURRENCY || 'EUR'
}

export interface BomPart extends Part {
    reference: string,
    part: Part
}