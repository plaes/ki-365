import http from '@/http_common';
import { } from 'axios';
import { ManufacturerPart } from './manufacturers';
import { KicadPart, Library } from './kicad';
import { PartStatus } from 'frontend_common/src/status';
import { Assembly } from './assembly';

export interface Part {
  id?: number;
  title: string;
  description: string;
  status: PartStatus;
}

export interface RequestInterface {
  page: number,
  per_page: number,
  total: number,
  total_pages: number,
  parts: Part[]
}


export abstract class PartService {
  static async get_all(params: {
    page: number,
    items_per_page?: number,
    sort_key?: string,
    sort_order?: string,
    search?: string
  }): Promise<RequestInterface> {
    let response = await http.get<RequestInterface>('/parts', { params });
    return response.data
  }

  static get(id: number): Promise<Part> {
    return http.get(`/parts/${id}`);
  }

  static async create(data: Part): Promise<Part> {
    delete data.id;
    let response = await http.put("/parts", data);
    return response.data
  }

  static async update(id: number, data: Part): Promise<Part> {
    delete data.id;
    let response = await http.post(`/parts/${id}`, data);
    return response.data
  }

  static async delete(id: number): Promise<Part> {
    let response = await http.delete(`/parts/${id}`);
    return response.data
  }

  static async get_all_manufacturer_parts(id: number): Promise<ManufacturerPart[]> {
    let response = await http.get(`/parts/${id}/manufacturers`);
    return response.data
  }

  static async create_manufacturer_part(id: number, data: ManufacturerPart): Promise<Part> {
    delete data.id;
    let response = await http.put(`/parts/${id}/manufacturers`, data);
    return response.data
  }
  static async get_assembly(id: number): Promise<Assembly> {
    let response = await http.get(`/parts/${id}/assembly`);
    return response.data
  }

  static async get_all_kicad_parts(id: number): Promise<{ library: Library, parts: KicadPart[] }[]> {
    let response = await http.get(`/kicad/by-part-id/${id}`);
    return response.data
  }
}
