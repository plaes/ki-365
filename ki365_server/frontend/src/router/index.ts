// Composables
import { RouteLocationNormalizedLoaded, createRouter, createWebHistory } from 'vue-router'

import PageNotFound from '@/views/PageNotFound.vue'
import { useAuthStore } from '@/stores/auth.store'
import Settings from '@/views/settings/Index.vue'

const routes = [
  {
    path: '/',
    name: 'root',
    component: () => import('@/layouts/default/Default.vue'),
    meta: {
      bread_crumbs: [
        {
          title: 'Home'
        }
      ]
    },
    children: [
      {
        path: '',
        name: 'home',
        component: () => import('@/views/Home.vue'),
      },
      {
        path: 'parts',
        name: 'parts',
        component: () => import('@/views/Parts.vue'),
      },
      {
        path: 'manufacturers',
        name: 'manufacturers',
        component: () => import('@/views/Manufacturers.vue'),
      },
      {
        path: 'suppliers',
        name: 'suppliers',
        component: () => import('@/views/Suppliers.vue'),
      },
      {
        path: 'assemblies',
        name: 'assemblies',
        component: () => import('@/views/Assemblies.vue'),
      },
      {
        path: 'assemblies/:assembly_id/:slug',
        name: 'assembly_revision',
        component: () => import('@/views/AssemblyRevision.vue'),
        props: ({ params: { assembly_id, slug } }: { params: { assembly_id: string, slug: string } }) =>
          ({ assembly_id: Number.parseInt(assembly_id, 10) || 0, slug: slug }),
      },
      {
        path: 'kicad',
        name: 'kicad',
        component: () => import('@/views/kicad/Index.vue'),
        children: [
          {
            path: 'libraries/:library',
            name: 'kicad_libraries',
            component: () => import('@/views/kicad/Libraries.vue'),
          },
          {
            path: 'symbols',
            name: 'kicad_symbols',
            component: () => import('@/views/kicad/Symbols.vue'),
          },
          {
            path: 'footprints',
            name: 'kicad_footprints',
            component: () => import('@/views/kicad/Footprints.vue'),
          },
        ]
      },
      {
        path: '/settings',
        name: 'settings',
        component: () => import('@/views/settings/Index.vue'),
      },
      { path: '/:pathMatch(.*)*', name: 'page_not_found', component: PageNotFound }
    ],
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
})


router.beforeEach(async (to) => {
  // redirect to login page if not logged in and trying to access a restricted page
  const publicPages = ['/'];
  const authRequired = !publicPages.includes(to.path);
  const auth = useAuthStore();

  // Set the should_authenticate flag to force user to log in
  if (authRequired && !auth.user) {
    auth.request_authentication()
  }
});

export default router
