import axios, { AxiosError, AxiosInstance } from "axios";
import router from "./router";
import { useAuthStore } from "./stores/auth.store";

const auth_store = useAuthStore()

const apiClient: AxiosInstance = axios.create({
  baseURL: import.meta.env.VITE_API_URL,
  withCredentials: true
});

// Add CSRF token to POST/PUT/DELETE headers
apiClient.interceptors.request.use((request) => {
  if(auth_store.csrf_access_token && request.method != "GET" ) {
    request.headers['X-CSRF-TOKEN'] = auth_store.csrf_access_token
  }
  return request
})

// Trigger login prompt on 401 errors
apiClient.interceptors.response.use(function (response) {
  return response
}, function (error: AxiosError) {
  console.log(error.response?.data)
  if (error.response?.status === 401) {
    // Logout
    auth_store.logout()
    auth_store.request_authentication()
  }
  return Promise.reject(error)
})


export default apiClient;
