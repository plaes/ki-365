"""users

Revision ID: 799c14bf94f2
Revises: 32a71aa126df
Create Date: 2023-07-14 18:36:09.876236

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "799c14bf94f2"
down_revision = "32a71aa126df"
branch_labels = None
depends_on = None


def upgrade() -> None:
    users = op.create_table(
        "users",
        sa.Column("id", sa.Integer(), autoincrement=True, nullable=False),
        sa.Column("name", sa.String(), nullable=False),
        sa.Column("hash", sa.String(), nullable=True),
        sa.Column("is_engineer", sa.Boolean, nullable=False),
        sa.Column("is_librarian", sa.Boolean, nullable=False),
        sa.Column("is_admin", sa.Boolean, nullable=False),
        sa.PrimaryKeyConstraint("id"),
    )
    # Add a default account for Admin without a password
    op.bulk_insert(
        users,
        [
            {
                "name": "Admin",
                "hash": None,
                "is_engineer": False,
                "is_librarian": False,
                "is_admin": True,
            }
        ],
    )


def downgrade() -> None:
    op.drop_table("users")
