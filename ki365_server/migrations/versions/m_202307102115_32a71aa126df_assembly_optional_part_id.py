"""assembly-optional-part-id

Revision ID: 32a71aa126df
Revises: 8187bb0d3678
Create Date: 2023-07-10 21:15:43.636595

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '32a71aa126df'
down_revision = '8187bb0d3678'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.alter_column('assembly_revision_parts', 'part_id', nullable=True)


def downgrade() -> None:
    op.alter_column('assembly_revision_parts', 'part_id', nullable=False)

