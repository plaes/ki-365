from logging.config import fileConfig
import os
from dotenv import load_dotenv

from sqlalchemy import create_engine

from alembic import context

# this is the Alembic Config object, which provides
# access to the values within the .ini file in use.
config = context.config

# Interpret the config file for Python logging.
# This line sets up loggers basically.
if config.config_file_name is not None:
    fileConfig(config.config_file_name)

# add your model's MetaData object here
# for 'autogenerate' support
#from ki365_common.models.base import Base
#from ki365_common.models.part import *
#from ki365_common.models.manufacturers import *
#from ki365_common.models.suppliers import *
#from ki365_common.models.assemblies import *

#from ki365_common.integrations.kicad.model import *


target_metadata = None #Base.metadata

# other values from the config, defined by the needs of env.py,
# can be acquired:
# my_important_option = config.get_main_option("my_important_option")
# ... etc.

#
load_dotenv()
url = os.environ.get("DATABASE_URL")

def include_object(object, name, type_, reflected, compare_to):
    """
    Ignore kicad library tables and other alembic schemas
    """

    return not (
        type_ == "table"
        and (
            name.startswith("lib_")
            or "alembic_version" in name
            or object.info.get("skip_autogenerate", False)
        )
    )


def run_migrations_offline() -> None:
    """Run migrations in 'offline' mode.

    This configures the context with just a URL
    and not an Engine, though an Engine is acceptable
    here as well.  By skipping the Engine creation
    we don't even need a DBAPI to be available.

    Calls to context.execute() here emit the given string to the
    script output.

    """
    context.configure(
        url=url,
        target_metadata=target_metadata,
        literal_binds=True,
        include_object=include_object,
        dialect_opts={"paramstyle": "named"},
        version_table="parts_alembic_version",
        # version_table_schema="parts",
    )

    with context.begin_transaction():
        context.run_migrations()


def run_migrations_online() -> None:
    """Run migrations in 'online' mode.

    In this scenario we need to create an Engine
    and associate a connection with the context.

    """
    connectable = create_engine(url)
    with connectable.connect() as connection:
        context.configure(
            connection=connection,
            target_metadata=target_metadata,
            include_object=include_object,
            version_table="parts_alembic_version",
            # version_table_schema="parts",

        )

        with context.begin_transaction():
            context.run_migrations()


if context.is_offline_mode():
    run_migrations_offline()
else:
    run_migrations_online()
