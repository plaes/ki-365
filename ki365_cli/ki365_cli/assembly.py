"""Assembly"""
from argparse import ArgumentParser, Namespace
from pprint import pprint


from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


from ki365_common.bill_of_material.bom_db import (
    bom_explode_subassemblies,
    bom_from_db,
    bom_populate_groups_from_db,
    create_revision_from_bom,
)
from . import bom


def main_export_bom(args: Namespace):
    engine = create_engine(args.database_url)
    session_maker = sessionmaker(engine)
    with session_maker.begin() as session:
        db_bom = bom_from_db(session, revision_id=args.revision_id)
        bom_explode_subassemblies(session, db_bom)
        db_grouped_bom = db_bom.group_by_part_id()
        bom_populate_groups_from_db(session, db_grouped_bom.groups)

    bom.export_bom(args, db_grouped_bom)


def main_import_bom(args: Namespace):
    ext_bom = bom.import_bom(args)

    engine = create_engine(args.database_url)
    session_maker = sessionmaker(engine, expire_on_commit=False)
    with session_maker.begin() as session:
        rev = create_revision_from_bom(
            session, ext_bom, args.assembly_id, args.revision
        )
        session.commit()

        print(f"Created revision ID={rev.id}")

def init_parser(parser: ArgumentParser):
    group = parser.add_argument_group("Assembly")
    ids = group.add_mutually_exclusive_group(required=True)
    ids.add_argument(
        "--assembly-id",
        type=int,
        metavar="ID",
        help="Assembly ID (requires -r|--revision)",
    )
    ids.add_argument(
        "--revision-id", type=int, metavar="ID", help="Assembly revision ID"
    )
    group.add_argument("-r", "--revision", help="Assembly revision")
    bom.init_database_parser(group)

    sp = parser.add_subparsers(dest="asm-cmd", required=True)

    export_parser = sp.add_parser("export", help="Export assembly to local BOM")
    export_parser.set_defaults(asm_func=main_export_bom)
    bom.init_export_parser(export_parser)

    import_parser = sp.add_parser("import", help="Import assembly from local BOM")
    import_parser.set_defaults(asm_func=main_import_bom)
    bom.init_import_parser(import_parser)
    import_parser.add_argument(
        "-c", "--create", help="Create assembly revision if it does not exist"
    )
    import_parser.add_argument(
        "-f",
        "--force",
        help="Forcefully replace assembly revision if it already exists",
    )

    delete_parser = sp.add_parser(
        "remove", aliases=["rm"], help="Import assembly from local BOM"
    )
    delete_parser.set_defaults(asm_func=main_import_bom)


def subcommand(args: Namespace):
    args.asm_func(args)
