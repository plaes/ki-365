import argparse
from types import ModuleType
from dotenv import load_dotenv

import shtab

from ki365_common.bill_of_material import *

from .import bom, assembly


HOMEPAGE = "https://gitlab.com/ki365/ki-365"
WIKIPAGE = "https://gitlab.com/ki365/ki-365/-/wikis/Cli"


def get_main_parser() -> argparse.ArgumentParser:

    parser = argparse.ArgumentParser(
        prog=__package__,
        description=f"Ki365 command line tools",
        epilog=f'See "{WIKIPAGE}" for detailed help and guides',
    )
    shtab.add_argument_to(parser, ["-s", "--print-completion"])
    s = parser.add_subparsers(dest="cmd", required=True)

    modules: list[ModuleType] = [
        bom,
        assembly
    ]


    # Find and add all submodules as commands
    for submodule in modules:
        sp = s.add_parser(submodule.__name__.split('.')[-1], help=submodule.__doc__, description=submodule.__doc__)
        submodule.init_parser(sp)
        sp.set_defaults(func=submodule.subcommand)
    
    return parser

def main():
    load_dotenv()
    parser = get_main_parser()
    
    # Run submodule
    args = parser.parse_args()
    args.func(args)