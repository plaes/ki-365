"""Generate BOM with database information"""
from argparse import _ArgumentGroup, ArgumentParser, Namespace
import json
import logging
import os
from pprint import pprint
from typing_extensions import override

from dotenv import load_dotenv

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from ki365_common.bill_of_material.bom import (
    Bom,
    BomGrouped,
    BomGroupedSchema,
    BomSchema,
)
from ki365_common.bill_of_material.bom_csv import bom_from_csv, bom_to_csv
from ki365_common.bill_of_material.bom_db import bom_populate_groups_from_db
from ki365_common.bill_of_material.bom_kicad import bom_from_kicad
from ki365_common.bill_of_material.suppliers import get_supplier_part_pricing


class Importer:
    @staticmethod
    def extensions() -> list[str]:
        raise NotImplementedError

    @staticmethod
    def import_bom(file, **kwargs) -> Bom:
        raise NotImplementedError


class KicadImporter(Importer):
    @override
    @staticmethod
    def extensions() -> list[str]:
        return [".xml"]

    @staticmethod
    def import_bom(file, **kwargs) -> Bom:
        with open(file, "rb") as xml:
            return bom_from_kicad(xml, **kwargs)


class CsvImporter(Importer):
    @override
    @staticmethod
    def extensions() -> list[str]:
        return [".csv", ".tsv"]

    @staticmethod
    def import_bom(file, **kwargs) -> Bom:
        _filename, file_extension = os.path.splitext(file)

        if file_extension == ".tsv":
            delimiter = "\t"
        else:
            delimiter = ","

        return bom_from_csv(file, delimiter=delimiter)


IMPORTERS: dict[str, type[Importer]] = {"kicad": KicadImporter, "csv": CsvImporter}


class Exporter:
    @staticmethod
    def extension() -> str:
        raise NotImplementedError

    @staticmethod
    def export_bom(file: str, bom: Bom | BomGrouped, **kwargs):
        raise NotImplementedError


class JsonExporter(Exporter):
    @override
    @staticmethod
    def extension() -> str:
        return ".json"

    @override
    @staticmethod
    def export_bom(file: str, bom: Bom | BomGrouped, **kwargs):
        with open(file, "w") as output:
            json.dump(
                BomSchema().dump(bom)
                if isinstance(bom, Bom)
                else BomGroupedSchema().dump(bom),
                output,
                indent=4,
            )


class CsvExporter(Exporter):
    @override
    @staticmethod
    def extension() -> str:
        return ".csv"

    @override
    @staticmethod
    def export_bom(file: str, bom: Bom | BomGrouped, **kwargs):
        # pprint(bom)
        bom_to_csv(bom, file)


EXPORTERS: dict[str, type[Exporter]] = {"json": JsonExporter, "csv": CsvExporter}


def init_parser(parser: ArgumentParser):
    parser.add_argument('--no-group', action='store_true', help="Do not group BOM items")
    init_import_parser(parser)
    init_export_parser(parser)
    init_database_parser(parser)
    init_supplier_parser(parser)


def init_database_parser(parser: ArgumentParser | _ArgumentGroup):
    default_url = os.environ.get("DATABASE_URL")
    parser.add_argument(
        "--database-url",
        default=default_url,
        help="URL to Ki365 database (default: %(default)s)",
    )


def init_import_parser(parser: ArgumentParser):
    parser.add_argument(
        "input",
        default="bom.xml",
        help="Path to BOM to import (default: %(default)s)",
    )
    parser.add_argument(
        "--input-format",
        choices=[f for f in IMPORTERS.keys()],
        help="Input file format. Default behaviour is to use input file extension to derive format",
    )


def init_export_parser(parser: ArgumentParser):
    parser.add_argument("output", nargs="?", default=None, help="Output path")
    parser.add_argument(
        "--output-format",
        choices=[f for f in EXPORTERS.keys()],
        default="json",
        help="Output file format",
    )

def init_supplier_parser(parser: ArgumentParser):
    parser.add_argument("--check-suppliers", action='store_true', help="Fetch info from supported suppliers")

def import_bom(args: Namespace) -> Bom:
    filename, file_extension = os.path.splitext(args.input)

    if args.input_format is not None:
        importer = IMPORTERS[args.input_format]
    else:
        try:
            importer = next(
                v for v in IMPORTERS.values() if file_extension in v.extensions()
            )
        except StopIteration:
            print(f"Unknown file extension '{file_extension}'.\nKnown extensions:")
            for k, v in IMPORTERS.items():
                print(f" * {k}: " + ", ".join([f"'{ext}'" for ext in v.extensions()]))
            exit(1)

    return importer.import_bom(args.input)


def add_from_db(args: Namespace, bom: BomGrouped):
    # Connect to database
    engine = create_engine(args.database_url)
    session = sessionmaker(engine)
    with session.begin() as sess:
        bom_populate_groups_from_db(sess, bom.groups)


def export_bom(args: Namespace, bom: Bom | BomGrouped):
    if hasattr(args, "input"):
        filename, file_extension = os.path.splitext(args.input)
    else:
        filename = "bom"

    args.exporter = EXPORTERS[args.output_format]

    if args.output is None:
        args.output = filename + args.exporter.extension()
        print("Output file: " + args.output)

    return args.exporter.export_bom(args.output, bom)


def subcommand(args: Namespace):
    #logging.basicConfig(level=logging.DEBUG)

    bom = import_bom(args)
    if not args.no_group:
        grouped_bom = bom.group_by_part_id()
        # pprint(bom)
        add_from_db(args, grouped_bom)
        if args.check_suppliers:
            get_supplier_part_pricing(grouped_bom)
        
        export_bom(args, grouped_bom)
    else:
        export_bom(args, bom)
