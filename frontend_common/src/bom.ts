// BOM currencies
export const currencies = ['EUR', 'USD', 'SEK']
export type BomCurrency = typeof currencies[number]

/**
 * Get the default currency. Defaults to EURO but can be overriden with BOM_DEFAULT_CURRENCY env variable
 * @returns default currency to use
 */
export function get_default_currency(): BomCurrency {
    return import.meta.env.BOM_DEFAULT_CURRENCY || 'EUR'
}

// A ts mirror of the schema defined in ki365_common/ki365_common/bill_of_material/bom.py
// KEEP IN SYNC!
export interface BomPriceBreak {
    quantity: number
    cost: number
}

export interface BomProjectPrice {
    currency: BomCurrency
    pricing: BomPriceBreak[]
}

export interface BomProject {
    source: string
    title: string
    revision?: string
    properties?: Record<string, string>
    total_costs: BomProjectPrice
    part_id?: number
}

export interface BomSupplierStock {
    status: string //TODO
    in_stock: number
    currency: BomCurrency
    pricing: BomPriceBreak[]
}

export interface BomSupplier {
    id: number
    name: string
    link: string
    status: string //TODO
}


export interface BomSupplierPart {
    id: number
    partno: string
    status: string //TODO
    supplier: BomSupplier
    stock?: BomSupplierStock
}

export interface BomManufacturer {
    id: number
    name: string
    link: string
    status: string //TODO
}

export interface BomManufacturerPart {
    id: number
    partno: string
    status: string //TODO
    manufacturer: BomManufacturer
    suppliers: BomSupplierPart[]
}


export interface BomPartGroup {
    sort: number
    references: string[]
    title?: string
    part_id?: number
    quantity: number
    datasheet: string
    manufacturers: BomManufacturerPart[]

}

export interface BomPart {
    sort: number
    reference: string
    title?: string
    part_id: number
    datasheet: string
    fields?: Record<string, string>
}

export interface Bom {
    project: BomProject
    groups?: BomPartGroup[]
    parts?: BomPart[]
}


