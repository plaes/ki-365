// Status codes
//

export interface StatusInfo<T> {
    value: T,
    name: string,
    color: string
    icon?: string
}

export interface Status {
    name: string,
    color: string
    icon?: string
}

export type PartStatus = 'preliminary' | 'active' | 'not-recommended' | 'obsolete'
export const part_stats: Record<PartStatus, Status> = {
    'preliminary': { name: 'Preliminary', color: 'warning', icon: 'mdi-alert' },
    'active': { name: 'Active', color: 'success', icon: 'mdi-check-circle' },
    'not-recommended': { name: 'Not recommended', color: 'warning', icon: 'mdi-alert' },
    'obsolete': { name: 'Obsolete', color: 'error', icon: 'mdi-alert-circle' },
};
export const part_status_choices = Object.entries(part_stats).map(([k, v]) =>({ value: k, name: v.name }))


export type ManufacturerStatus = 'preliminary' | 'active' | 'preferred' | 'not-recommended' | 'do-not-use' | 'defunct'
export const manufacturer_stats: Record<ManufacturerStatus, Status> = {
    'preliminary': { name: "Preliminary", color: "warning", icon: "mdi-alert" },
    'active': { name: "Active", color: "success", icon: "mdi-check-circle" },
    'preferred': { name: "Preferred", color: "success", icon: "mdi-check-underline-circle" },
    'not-recommended': { name: "Not recommended", color: "warning", icon: "mdi-alert" },
    'do-not-use': { name: "Do not use", color: "error", icon: "mdi-alert-circle" },
    'defunct': { name: "Defunct", color: "error", icon: "mdi-alert-circle" },
}
export const manufacturer_status_choices = Object.entries(manufacturer_stats).map(([k, v]) => ({ value: k, name: v.name }))


export type ManufacturerPartStatus = 'preliminary' | 'active' | 'preferred' | 'not-recommended' | 'obsolete'
export const manufacturer_part_stats: Record<ManufacturerPartStatus, Status> = {
    'preliminary': { name: 'Preliminary', color: 'warning', icon: 'mdi-alert' },
    'active': { name: 'Active', color: 'success', icon: 'mdi-check-circle' },
    'preferred': { name: 'Preferred', color: 'success', icon: 'mdi-check-underline-circle' },
    'not-recommended': { name: 'Not recommended', color: 'warning', icon: 'mdi-alert' },
    'obsolete': { name: 'Obsolete', color: 'error', icon: 'mdi-alert-circle' },
};
export const manufacturer_part_status_choices = Object.entries(manufacturer_part_stats).map(([k, v]) => ({ value: k, name: v.name }))


export type SupplierStatus = 'preliminary' | 'active' | 'preferred' | 'not-recommended' | 'do-not-use' | 'defunct'
export const supplier_stats: Record<SupplierStatus, Status> = {
    'preliminary': { name: 'Preliminary', color: 'warning', icon: 'mdi-alert' },
    'active': { name: 'Active', color: 'success', icon: 'mdi-check-circle' },
    'preferred': { name: 'Preferred', color: 'success', icon: 'mdi-check-underline-circle' },
    'not-recommended': { name: 'Not recommended', color: 'warning', icon: 'mdi - alert' },
    'do-not-use': { name: 'Do not use', color: 'error', icon: 'mdi-alert-circle' },
    'defunct': { name: 'Defunct', color: 'error', icon: 'mdi-alert-circle' },
};
export const supplier_status_choices = Object.entries(supplier_stats).map(([k, v]) => ({ value: k, name: v.name }))


export type SupplierPartStatus = 'preliminary' | 'active' | 'preferred' | 'not-recommended' | 'obsolete'
export const supplier_part_stats: Record<SupplierPartStatus, Status> = {
    'preliminary': { name: 'Preliminary', color: 'warning', icon: 'mdi-alert' },
    'active': { name: 'Active', color: 'success', icon: 'mdi-check-circle' },
    'preferred': { name: 'Preferred', color: 'success', icon: 'mdi-check-underline-circle' },
    'not-recommended': { name: 'Not recommended', color: 'warning', icon: 'mdi-alert' },
    'obsolete': { name: 'Obsolete', color: 'error', icon: 'mdi-alert-circle' },
};
export const supplier_part_status_choices = Object.entries(supplier_part_stats).map(([k, v]) => ({ value: k, name: v.name }))


export type AssemblyRevisionStatus = 'preliminary' | 'active' | 'obsolete'
export const assembly_revision_stats: Record<AssemblyRevisionStatus, Status> = {
    'preliminary': { name: 'Preliminary', color: 'warning', icon: 'mdi-alert' },
    'active': { name: 'Active', color: 'success', icon: 'mdi-check-circle' },
    'obsolete': { name: 'Obsolete', color: 'error', icon: 'mdi-alert-circle' },
};
export const assembly_revision_status_choices = Object.entries(assembly_revision_stats).map(([k, v]) => ({ value: k, name: v.name }))

