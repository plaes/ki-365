import StatusIcon from "./components/StatusIcon.vue"

import ManufacturerPartInfo from "./components/ManufacturerPartInfo.vue"
import SupplierPartInfo from "./components/SupplierPartInfo.vue"

// Idk how this works
//import Ki365Logo from "./assets/ki-365-logo.svg"

export * from "./status"
export * from "./bom"

export { StatusIcon, ManufacturerPartInfo, SupplierPartInfo }