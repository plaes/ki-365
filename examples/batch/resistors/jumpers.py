# Batch script to add E12 series thinfilm/thickfilm resistors

import os
import sys

from dotenv import load_dotenv

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import Session

from slugify import slugify


# Add common to path
dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.join(dir, os.pardir, os.pardir, os.pardir))

# Add batch to path
dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.join(dir, os.pardir))

from resistors.common import KicadResistorMisc, create_part, create_resistor
from batch_utils import batch_commment

TYPE = ''
SIZES = [
    # (Imperial, metric, footprint)
    ("1206", "3216", "Resistor_SMD:R_1206_3216Metric"),
    ("0805", "2012", "Resistor_SMD:R_0805_2012Metric"),
    ("0603", "1608", "Resistor_SMD:R_0603_1608Metric"),
    ("0402", "1005", "Resistor_SMD:R_0402_1005Metric"),
]

COMMENT = batch_commment(__file__)

TAGS = []


def create_jumper(
    sess: Session,
    size: tuple[str, str, str],
    comment: str,
    tags: list[str] | None = None,
):
    size_imperial, size_metric, footprint = size
    if tags is None:
        tags = []

    #  Create part
    title = f"Jumper 0R {size_imperial} (metric {size_metric}) {' '.join(tags)}"
    title = title.strip()
    new_part = create_part(sess, title, title + "\n" + comment)
    sess.add(new_part)

    # Create kicad part
    part_id = new_part.id
    print(part_id)
    new_kicad_part = (
        sess.query(KicadResistorMisc)
        .where(KicadResistorMisc.part_id == part_id)
        .one_or_none()
    )
    if new_kicad_part is None:
        print(f"Creating new kicad mapping '{title}'")
        new_kicad_part = KicadResistorMisc()
        new_kicad_part.part_id = new_part.id
    else:
        print(f"Updating kicad mapping '{title}'")

    new_kicad_part.reference = slugify(
        f"jumper 0r {size_imperial} {size_metric} {' '.join(tags)}"
    )
    new_kicad_part.description = title + " " + comment
    new_kicad_part.keywords = " ".join(
        ["resistor", "jumper", *[tag.lower() for tag in tags]]
    )
    new_kicad_part.symbols = "Device:R"
    new_kicad_part.footprints = footprint
    new_kicad_part.value = '0R'
    new_kicad_part.power = 'N/A'
    new_kicad_part.tolerance = 'N/A'
    new_kicad_part.type = 'Jumper'
    sess.add(new_kicad_part)


if __name__ == "__main__":
    load_dotenv()

    url = os.environ.get("DATABASE_URL")

    # Create all required tables
    engine = create_engine(url, echo=True)
    session = sessionmaker(engine)
    with session.begin() as sess:
        for size in SIZES:
            create_jumper(sess, size, COMMENT, TAGS)

