import os
import sys
from typing import Any
from sqlalchemy.orm import Mapped
from sqlalchemy.orm import mapped_column
from slugify import slugify
from sqlalchemy.orm import Session

# Add common to path
dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.join(dir, os.pardir, os.pardir, os.pardir))

from ki365_common.models.part import Part
from ki365_common.models.base import Base, CommonMixin
from ki365_common.integrations.kicad.model import KicadPart


def format_value(val: float, dec: int, suffix: str, num_decimals: int):
    s = f"{round(val*dec, num_decimals):g}"
    if "." in s:
        return s.replace(".", suffix)
    else:
        return s + suffix



class KicadResistor(KicadPart):
    value: Mapped[str] = mapped_column()
    power: Mapped[str] = mapped_column()
    tolerance: Mapped[str] = mapped_column()

    def set_value(
        self,
        value: float,
        decade: int,
        suffix: str,
        num_decimals: int = 1
    ):
        self.value = format_value(value, decade, suffix, num_decimals)


class KicadResistorMisc(KicadResistor, Base, CommonMixin):
    __tablename__ = "lib_resistors_misc"
    type: Mapped[str] = mapped_column()


class KicadResistorThickfilm(KicadResistor, Base, CommonMixin):
    __tablename__ = "lib_resistors_thickfilm"


class KicadResistorThinfilm(KicadResistor, Base, CommonMixin):
    __tablename__ = "lib_resistors_thinfilm"


def create_part(
    sess: Session, title: str, description: str | None = None, status: str = "active"
):
    part = sess.query(Part).where(Part.title == title).one_or_none()
    if part is None:
        print(f"Creating new part '{title}'")
        part = Part()
    else:
        print(f"Updating part '{title}'")

    part.title = title
    if description is not None:
        part.description = description
    else:
        part.description = title
    part.status = status

    return part

