# Batch script to add E96 series thinfilm/thickfilm resistors

import os
import sys
from typing import TypeVar

from dotenv import load_dotenv
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import Session

from slugify import slugify

# Add common to path
dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.join(dir, os.pardir, os.pardir, os.pardir))

# Add batch to path
dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.join(dir, os.pardir))

from resistors.common import KicadResistor, KicadResistorThickfilm, KicadResistorThinfilm, create_part, format_value
from batch_utils import batch_commment

SIZES = [
    # (Imperial, metric, footprint)
    ("1206", "3216", "Resistor_SMD:R_1206_3216Metric", "0.25W"),
    ("0805", "2012", "Resistor_SMD:R_0805_2012Metric", "0.125W"),
    ("0603", "1608", "Resistor_SMD:R_0603_1608Metric", "0.1W"),
    ("0402", "1005", "Resistor_SMD:R_0402_1005Metric", "0.062W"),
]

E96_VALUES = [
    1.00,
    1.02,
    1.05,
    1.07,
    1.10,
    1.13,
    1.15,
    1.18,
    1.21,
    1.24,
    1.27,
    1.30,
    1.33,
    1.37,
    1.40,
    1.43,
    1.47,
    1.50,
    1.54,
    1.58,
    1.62,
    1.65,
    1.69,
    1.74,
    1.78,
    1.82,
    1.87,
    1.91,
    1.96,
    2.00,
    2.05,
    2.10,
    2.15,
    2.21,
    2.26,
    2.32,
    2.37,
    2.43,
    2.49,
    2.55,
    2.61,
    2.67,
    2.74,
    2.80,
    2.87,
    2.94,
    3.01,
    3.09,
    3.16,
    3.24,
    3.32,
    3.40,
    3.48,
    3.57,
    3.65,
    3.74,
    3.83,
    3.92,
    4.02,
    4.12,
    4.22,
    4.32,
    4.42,
    4.53,
    4.64,
    4.75,
    4.87,
    4.99,
    5.11,
    5.23,
    5.36,
    5.49,
    5.62,
    5.76,
    5.90,
    6.04,
    6.19,
    6.34,
    6.49,
    6.65,
    6.81,
    6.98,
    7.15,
    7.32,
    7.50,
    7.68,
    7.87,
    8.06,
    8.25,
    8.45,
    8.66,
    8.87,
    9.09,
    9.31,
    9.53,
    9.76,
]

DECADES = [
#    (1, "R"),  # 1-9.1ohm
#    (10, "R"),  # 10-92ohm
    (100, "R"),  # 100-920ohm
    (1, "K"),  # 1k-9k2 ohm
    (10, "K"),  # 10k-92k ohm
    (100, "K"),  # 100k-920k ohm
    (1, "M"),  # 1M-9M2 ohm
]

TOLERANCE = 1.0 # %

COMMENT = batch_commment(__file__)

TAGS = ['E96']

R = TypeVar('R', bound=KicadResistor)

def create_resistor(
    sess: Session,
    cls: type[R],
    typ: str,
    size: tuple[str, str, str, str],
    decade: tuple[int, str],
    val: float,
    tolerance: float,
    comment: str,
    num_decimals: int,
    tags: list[str] | None = None,
) -> R:
    size_imperial, size_metric, footprint, power = size
    dec, suffix = decade

    if tags is None:
        tags = []

    #  Create part
    title = f"{typ.capitalize()} Resistor {round(val*dec, num_decimals):g}{suffix} {tolerance}% {size_imperial} (metric {size_metric}) {' '.join(tags)}"
    title = title.strip()
    new_part = create_part(sess, title, title + "\n" + comment)
    sess.add(new_part)

    # Create kicad part
    part_id = new_part.id
    print(part_id)
    kicad_part = (
        sess.query(cls)
        .where(cls.part_id == part_id)
        .one_or_none()
    )
    if kicad_part is None:
        print(f"Creating new kicad mapping '{title}'")
        kicad_part = cls()
        kicad_part.part_id = new_part.id
    else:
        print(f"Updating kicad mapping '{title}'")

    kicad_part.reference = slugify(
        f"{format_value(val, dec, suffix, num_decimals)} {format_value(tolerance, 1, 'pc', 3)} {size_imperial} {size_metric} {' '.join(tags)}"
    )
    kicad_part.description = title + " " + comment
    kicad_part.keywords = " ".join(
        [typ.lower(), "resistor", *[tag.lower() for tag in tags]]
    )
    kicad_part.symbols = "Device:R"
    kicad_part.footprints = footprint
    kicad_part.set_value(val, dec, suffix, num_decimals)
    kicad_part.power = power
    kicad_part.tolerance = f"{tolerance}%"
    sess.add(kicad_part)

    return kicad_part


if __name__ == "__main__":
    load_dotenv()

    url = os.environ.get("DATABASE_URL")

    # Create all required tables
    engine = create_engine(url, echo=True)
    session = sessionmaker(engine)
    with session.begin() as sess:
        for size in SIZES:
            for decade in DECADES:
                for val in E96_VALUES:
                    create_resistor(
                        sess, KicadResistorThickfilm, 'thickfilm', size, decade, val, TOLERANCE, COMMENT, 1, tags=TAGS
                    )
                    create_resistor(
                        sess, KicadResistorThinfilm, 'thinfilm', size, decade, val, TOLERANCE, COMMENT, 1, tags=TAGS
                    )
                    
            # 10M
            create_resistor(
                sess, KicadResistorThickfilm, 'thickfilm', size, DECADES[-1], 10.0, TOLERANCE, COMMENT, 1, tags=TAGS
            )
            create_resistor(
                sess, KicadResistorThinfilm, 'thinfilm', size, DECADES[-1], 10.0, TOLERANCE, COMMENT, 1, tags=TAGS
            )

        # Commit changes unless DRY_RUN is set
        if os.environ.get("DRY_RUN") is None:
            sess.commit()
