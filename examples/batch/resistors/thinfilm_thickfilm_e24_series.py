# Batch script to add E12 series thinfilm/thickfilm resistors

import os
import sys
from typing import TypeVar

from dotenv import load_dotenv
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import Session

from slugify import slugify

# Add common to path
dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.join(dir, os.pardir, os.pardir, os.pardir))

# Add batch to path
dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.join(dir, os.pardir))

from resistors.common import KicadResistor, KicadResistorThickfilm, KicadResistorThinfilm, create_part, format_value
from batch_utils import batch_commment

SIZES = [
    # (Imperial, metric, footprint)
    ("1206", "3216", "Resistor_SMD:R_1206_3216Metric", "0.25W"),
    ("0805", "2012", "Resistor_SMD:R_0805_2012Metric", "0.125W"),
    ("0603", "1608", "Resistor_SMD:R_0603_1608Metric", "0.1W"),
    ("0402", "1005", "Resistor_SMD:R_0402_1005Metric", "0.062W"),
]

E24_VALUES = [
    1.0,
    1.1,
    1.2,
    1.3,
    1.5,
    1.6,
    1.8,
    2.0,
    2.2,
    2.4,
    2.7,
    3.0,
    3.3,
    3.6,
    3.9,
    4.3,
    4.7,
    5.1,
    5.6,
    6.2,
    6.8,
    7.5,
    8.2,
    9.1,
]

DECADES = [
    (1, "R"),  # 1-9.1ohm
    (10, "R"),  # 10-92ohm
    (100, "R"),  # 100-920ohm
    (1, "K"),  # 1k-9k2 ohm
    (10, "K"),  # 10k-92k ohm
    (100, "K"),  # 100k-920k ohm
    (1, "M"),  # 1M-9M2 ohm
]

TOLERANCE = 5.0 # %

COMMENT = batch_commment(__file__)

TAGS = ['E24']

R = TypeVar('R', bound=KicadResistor)

def create_resistor(
    sess: Session,
    cls: type[R],
    typ: str,
    size: tuple[str, str, str, str],
    decade: tuple[int, str],
    val: float,
    tolerance: float,
    comment: str,
    num_decimals: int,
    tags: list[str] | None = None,
) -> R:
    size_imperial, size_metric, footprint, power = size
    dec, suffix = decade

    if tags is None:
        tags = []

    #  Create part
    title = f"{typ.capitalize()} Resistor {round(val*dec, num_decimals):g}{suffix} {tolerance}% {size_imperial} (metric {size_metric}) {' '.join(tags)}"
    title = title.strip()
    new_part = create_part(sess, title, title + "\n" + comment)
    sess.add(new_part)

    # Create kicad part
    part_id = new_part.id
    print(part_id)
    kicad_part = (
        sess.query(cls)
        .where(cls.part_id == part_id)
        .one_or_none()
    )
    if kicad_part is None:
        print(f"Creating new kicad mapping '{title}'")
        kicad_part = cls()
        kicad_part.part_id = new_part.id
    else:
        print(f"Updating kicad mapping '{title}'")

    kicad_part.reference = slugify(
        f"{format_value(val, dec, suffix, num_decimals)} {format_value(tolerance, 1, 'pc', 3)} {size_imperial} {size_metric} {' '.join(tags)}"
    )
    kicad_part.description = title + " " + comment
    kicad_part.keywords = " ".join(
        [typ.lower(), "resistor", *[tag.lower() for tag in tags]]
    )
    kicad_part.symbols = "Device:R"
    kicad_part.footprints = footprint
    kicad_part.set_value(val, dec, suffix, num_decimals)
    kicad_part.power = power
    kicad_part.tolerance = f"{tolerance}%"
    sess.add(kicad_part)

    return kicad_part


if __name__ == "__main__":
    load_dotenv()

    url = os.environ.get("DATABASE_URL")

    # Create all required tables
    engine = create_engine(url, echo=True)
    session = sessionmaker(engine)
    with session.begin() as sess:
        for size in SIZES:
            for decade in DECADES:
                for val in E24_VALUES:
                    create_resistor(
                        sess, KicadResistorThickfilm, 'thickfilm', size, decade, val, TOLERANCE, COMMENT, 1, tags=TAGS
                    )
                    create_resistor(
                        sess, KicadResistorThinfilm, 'thinfilm', size, decade, val, TOLERANCE, COMMENT, 1, tags=TAGS
                    )
                    
            # 10M
            create_resistor(
                sess, KicadResistorThickfilm, 'thickfilm', size, DECADES[-1], 10.0, TOLERANCE, COMMENT, 1, tags=TAGS
            )
            create_resistor(
                sess, KicadResistorThinfilm, 'thinfilm', size, DECADES[-1], 10.0, TOLERANCE, COMMENT, 1, tags=TAGS
            )

        # Commit changes unless DRY_RUN is set
        if os.environ.get("DRY_RUN") is None:
            sess.commit()
