"""${message}

Revision ID: ${up_revision}
Revises: ${down_revision | comma,n}
Create Date: ${create_date}

"""
from alembic import op
import sqlalchemy as sa
${imports if imports else ""}

# revision identifiers, used by Alembic.
revision = ${repr(up_revision)}
down_revision = ${repr(down_revision)}
branch_labels = ${repr(branch_labels)}
depends_on = ${repr(depends_on)}


def upgrade() -> None:
    ${upgrades if upgrades else '''
    # My library table
    op.create_table(
        "my_custom_table",
        # Id and parent part
        sa.Column("id", sa.INTEGER, primary_key=True),
        sa.Column("part_id", sa.INTEGER, nullable=False),
        # Required columns
        ## Part key/reference, should be unique and index might make it faster idk
        sa.Column("reference", sa.TEXT, unique=True, index=True),
        sa.Column("symbols", sa.TEXT),
        sa.Column("footprints", sa.TEXT),
        # Properties
        sa.Column("description", sa.TEXT),
        sa.Column("keywords", sa.TEXT),
        sa.Column("exclude_from_bom", sa.INTEGER),
        sa.Column("exclude_from_board", sa.INTEGER),
        # Fields, add anything you want here
        sa.Column("value", sa.TEXT),
        # sa.Column("power", sa.TEXT),
        # sa.Column("whatever", sa.TEXT),

        # Put a foreignkey constraint on part_id
        sa.ForeignKeyConstraint(
            ["part_id"],
            ["parts.id"],
        ),
    )
    '''}


def downgrade() -> None:
    ${downgrades if downgrades else 'op.drop_table("resistors")'}
