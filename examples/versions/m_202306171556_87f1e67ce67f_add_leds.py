"""add-leds

Revision ID: 87f1e67ce67f
Revises: fcd20931f63d
Create Date: 2023-06-17 15:56:39.271972

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '87f1e67ce67f'
down_revision = 'fcd20931f63d'
branch_labels = None
depends_on = None


def upgrade() -> None:
    
    # My library table
    op.create_table(
        "lib_led_diodes",
        # Id and parent part
        sa.Column("id", sa.INTEGER, primary_key=True),
        sa.Column("part_id", sa.INTEGER, nullable=False),
        # Required columns
        ## Part key/reference, should be unique and index might make it faster idk
        sa.Column("reference", sa.TEXT, unique=True, index=True),
        sa.Column("symbols", sa.TEXT),
        sa.Column("footprints", sa.TEXT),
        # Properties
        sa.Column("description", sa.TEXT),
        sa.Column("keywords", sa.TEXT),
        sa.Column("exclude_from_bom", sa.INTEGER),
        sa.Column("exclude_from_board", sa.INTEGER),
        # Fields, add anything you want here
        sa.Column("value", sa.TEXT),
        sa.Column("color", sa.TEXT),
        sa.Column("current", sa.TEXT),

        # Put a foreignkey constraint on part_id
        sa.ForeignKeyConstraint(
            ["part_id"],
            ["parts.id"],
        ),
    )
    


def downgrade() -> None:
    op.drop_table("lib_led_diodes")
