"""add-tolerance-to-resistors

Revision ID: a744672a4be2
Revises: 6be5cef35c09
Create Date: 2023-06-17 16:54:41.116137

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'a744672a4be2'
down_revision = '6be5cef35c09'
branch_labels = None
depends_on = None


def upgrade() -> None:
    # Add tolerance to resistors table
    op.add_column("lib_resistors", sa.Column("tolerance", sa.TEXT))

def downgrade() -> None:
    op.drop_column("lib_resistors", "tolerance")
