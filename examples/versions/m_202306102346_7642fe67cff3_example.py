"""example

Revision ID: 7642fe67cff3
Revises: 
Create Date: 2023-06-10 23:46:00.983473

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "7642fe67cff3"
down_revision = None
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table(
        "resistors",
        # Id and parent part
        sa.Column("id", sa.INTEGER, primary_key=True),
        sa.Column("part_id", sa.INTEGER, nullable=False),
        # Required columns
        ## Part key/reference, should be unique and index might make it faster idk
        sa.Column("reference", sa.TEXT, unique=True, index=True),
        sa.Column("symbols", sa.TEXT),
        sa.Column("footprints", sa.TEXT),
        # Properties
        sa.Column("description", sa.TEXT),
        sa.Column("keywords", sa.TEXT),
        sa.Column("exclude_from_bom", sa.INTEGER),
        sa.Column("exclude_from_board", sa.INTEGER),
        # Fields
        sa.Column("value", sa.TEXT),
        sa.Column("power", sa.TEXT),
        # Put a foreignkey constraint on part_id
        sa.ForeignKeyConstraint(
            ["part_id"],
            ["parts.id"],
        ),
    )
    op.create_table(
        "capacitors",
        sa.Column("id", sa.INTEGER, primary_key=True),
        sa.Column("part_id", sa.INTEGER, nullable=False),
        # Required columns
        ## Part key/reference, should be unique and index might make it faster idk
        sa.Column("reference", sa.TEXT, nullable=False, unique=True, index=True),
        sa.Column("symbols", sa.TEXT, nullable=False),
        sa.Column("footprints", sa.TEXT),
        # Properties
        sa.Column("description", sa.TEXT),
        sa.Column("keywords", sa.TEXT),
        sa.Column("exclude_from_bom", sa.INTEGER),
        sa.Column("exclude_from_board", sa.INTEGER),
        # Fields
        sa.Column("value", sa.TEXT),
        sa.Column("type", sa.TEXT),
        sa.Column("voltage", sa.TEXT),
        sa.ForeignKeyConstraint(
            ["part_id"],
            ["parts.id"],
        ),
    )


def downgrade() -> None:
    op.drop_table("resistors")
    op.drop_table("capacitors")
