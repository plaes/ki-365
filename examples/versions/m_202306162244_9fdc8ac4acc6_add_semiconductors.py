"""add-semiconductors

Revision ID: 9fdc8ac4acc6
Revises: 7642fe67cff3
Create Date: 2023-06-16 22:44:13.898379

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '9fdc8ac4acc6'
down_revision = '7642fe67cff3'
branch_labels = None
depends_on = None


def upgrade() -> None:
    
    # My library table
    op.create_table(
        "lib_microcontrollers",
        # Id and parent part
        sa.Column("id", sa.INTEGER, primary_key=True),
        sa.Column("part_id", sa.INTEGER, nullable=False),
        # Required columns
        ## Part key/reference, should be unique and index might make it faster idk
        sa.Column("reference", sa.TEXT, unique=True, index=True),
        sa.Column("symbols", sa.TEXT),
        sa.Column("footprints", sa.TEXT),
        # Properties
        sa.Column("description", sa.TEXT),
        sa.Column("keywords", sa.TEXT),
        sa.Column("exclude_from_bom", sa.INTEGER),
        sa.Column("exclude_from_board", sa.INTEGER),
        # Fields, add anything you want here
        sa.Column("value", sa.TEXT),
        sa.Column("architecture", sa.TEXT),
        sa.Column("flash_memory", sa.TEXT),
        sa.Column("sram_memory", sa.TEXT),

        # Put a foreignkey constraint on part_id
        sa.ForeignKeyConstraint(
            ["part_id"],
            ["parts.id"],
        ),
    )
    


def downgrade() -> None:
    op.drop_table("lib_microcontrollers")
