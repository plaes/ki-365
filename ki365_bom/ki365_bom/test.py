import argparse
import os
from pprint import pprint
from queue import Queue
import subprocess
import sys
from tempfile import TemporaryDirectory
import time
import logging
from typing_extensions import override
from dotenv import load_dotenv
import shtab

from watchdog.observers import Observer
from watchdog.events import LoggingEventHandler, FileSystemEventHandler, FileModifiedEvent, DirModifiedEvent

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from ki365_common.bill_of_material.bom_db import bom_populate_groups_from_db
from ki365_common.bill_of_material.bom_kicad import bom_from_kicad

KICAD_CLI_EXECUTABLE = os.environ.get("KICAD_CLI_EXECUTABLE", "kicad-cli")

class KicadProjectEventHandler(FileSystemEventHandler):
    def __init__(self, queue: Queue) -> None:
        super().__init__()
        self.queue = queue
    
    @override
    def on_any_event(self, event):
        print(event)
   
    @override
    def on_modified(self, event: FileModifiedEvent):
        if event.is_directory:
            return
        self.queue.put(event)

def main():
    load_dotenv()
    
    parser = argparse.ArgumentParser(
        prog=__package__,
        description="Ki365 BOM preview",
        #epilog=f'See "{WIKIPAGE}" for detailed help and guides',
    )
    shtab.add_argument_to(parser, ["-s", "--print-completion"])
    default_url = os.environ.get("DATABASE_URL")
    parser.add_argument(
        "--database-url",
        default=default_url,
        help="URL to Ki365 database (default: %(default)s)",
    )
    parser.add_argument(
        "-p", "--port",
        default=5555,
        type=int,
        help="Server port (default: %(default)s)",
    )
    parser.add_argument(
        "--open",
        action='store_true',
        help="Open in browser",
    )
    parser.add_argument(
        "kicad_sch",
        help="Kicad schematic",
    )

    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s - %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S",
    )
    args = parser.parse_args()
    kicad_sch_path = args.kicad_sch

    queue = Queue() 

    event_handler = KicadProjectEventHandler(queue)
    observer = Observer()
    observer.schedule(event_handler, kicad_sch_path)
    observer.start()

    with TemporaryDirectory(prefix='ki365-bom-tool') as tmp:
        filename, _extension =  os.path.splitext(kicad_sch_path)
        xml_bom_path = os.path.join(tmp, os.path.basename(filename) + '.xml')
        try:
            while True:
                logging.debug(f'Exporting BOM to {xml_bom_path}...')
                command = [
                    KICAD_CLI_EXECUTABLE,
                    "sch",
                    "export",
                    "python-bom",
                    kicad_sch_path,
                    "--output",
                    xml_bom_path,
                ]
                logging.debug('Executing "' + " ".join(command) + '"')
                p = subprocess.run(
                    command,
                    #capture_output=True,
                )
                if p.returncode != 0:
                    logging.error("BOM export failed! Waiting 10s and trying again.")
                    time.sleep(10)
                    continue

                # Run BOM script
                with open(xml_bom_path, 'rb') as xml:
                    bom = bom_from_kicad(xml, encoding='utf8')
                
                bom.group_by_part_id()
                
                engine = create_engine(args.database_url)
                session = sessionmaker(engine)
                with session.begin() as sess:
                    bom_populate_groups_from_db(sess, bom.groups)

                # pprint(bom)
                
                        
                # Wait for a new event
                evt = queue.get() 
        except KeyboardInterrupt:
            observer.stop()
        observer.join()
