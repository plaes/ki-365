

from eventlet.queue import Empty, Queue
from eventlet.event import Event
import eventlet
#eventlet.monkey_patch() # doesn't work

import argparse
import json
import logging
import os
from pathlib import Path

from threading import  Timer
from typing_extensions import override
import webbrowser
from dotenv import load_dotenv

from ki365_common.bill_of_material.bom import BomSchema
from ki365_common.bill_of_material.bom_db import bom_populate_groups_from_db
from ki365_common.bill_of_material.bom_kicad import bom_from_kicad_sch

from watchdog.observers import Observer
from watchdog.events import (
    FileSystemEventHandler,
    FileCreatedEvent,
    FileSystemEvent,
)

from flask import Flask, render_template, request
from flask_cors import CORS
from flask_socketio import SocketIO, emit

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from kicost import (
    log as kicost_log,
    init_all_loggers,
    set_distributors_progress,
    ProgressConsole,
    get_logger,
    configure_kicost_apis,
    KiCostError,
)

from kicost.currency_converter import list_currencies

from ki365_common.bill_of_material.kicost import (
    calculate_cost,
    calculate_total_cost,
    command_line_api_options,
    kicost_it,
)

dist_path = Path(__file__).parent.parent / "frontend" / "dist"
print(dist_path)

app = Flask(
    __name__,
    static_folder=dist_path / "assets",
    static_url_path="/assets",
    template_folder=dist_path,
)
cors = CORS(app)
socketio = SocketIO(app, cors_allowed_origins="*")

queue = Queue()
observer_stop = Event()

namespace = None

cached_bom = None



# Serve frontend
@app.route("/", defaults={"path": ""})
@app.route("/<path:path>")
def index(path):
    return render_template("index.html")

@app.route("/shutdown")
def shutdown():
    #exit(signal.SIGTERM)
    socketio.stop()
    return 'OK'


@socketio.on("connect", namespace=namespace)
def connect():
    print("Client connected", request.sid)
    # emit('connect')
    if cached_bom is not None:
        # Send BOM
        bom_json = BomSchema().dump(cached_bom)
        emit("bill_of_material", bom_json, namespace=namespace)
    else:
        emit("reloading", namespace=namespace)


# Notification that a client has disconnected
@socketio.on("disconnect", namespace=namespace)
def disconnect():
    print("Client disconnected", request.sid)
    # socketio.stop()
    # emit('disconnect')


@socketio.on("refresh", namespace=namespace)
def refresh():
    queue.put(None)
    # emit('connect')


# replace with polling eventlet...
class KicadProjectEventHandler(FileSystemEventHandler):
    def __init__(self, socket_io: SocketIO, project_name: str) -> None:
        super().__init__()
        self.socket_io = socket_io
        self.project_name = project_name

    def p_filter(self, event: FileSystemEvent):
        if event.is_directory:
            return False

        filename, extension = os.path.splitext(event.src_path)
        return (
            os.path.basename(filename) == self.project_name
            and extension == ".kicad_sch"
        )

    def send_event(self, event: FileSystemEvent):
        queue.put(event, block=False)

    @override
    def on_created(self, event: FileCreatedEvent):
        if not self.p_filter(event):
            return
        self.send_event(event)

    @override
    def on_modified(self, event: FileCreatedEvent):
        if not self.p_filter(event):
            return
        self.send_event(event)


def background_task(
    socket_io: SocketIO,
    kicad_sch_path: str,
    database_url: str,
    quantity: list[int],
    currency: str,
):
    global cached_bom
    while True:
        t1 = os.path.getmtime(kicad_sch_path)
        socket_io.emit("reloading", namespace=namespace)
        eventlet.sleep(0)

        # Generate BOM
        bom = bom_from_kicad_sch(kicad_sch_path)
        bom.group_by_part_id()

        engine = create_engine(database_url)
        session = sessionmaker(engine)
        with session.begin() as sess:
            bom_populate_groups_from_db(sess, bom.groups)
        
        eventlet.sleep(0)

        # Kicost
        try:
            kicost_it(bom.groups)
            calculate_total_cost(bom, quantity, currency)
        except KiCostError as kcerr:
            # socket_io.emit("error", {
            #    'desc': 'KiCost error',
            #    'msg': kcerr.msg
            # }, namespace=namespace)
            print(kcerr)

        # pprint(bom.groups)

        # Send BOM
        bom_json = BomSchema().dump(bom)
        socket_io.emit("bill_of_material", bom_json, namespace=namespace)
        eventlet.sleep(0)

        cached_bom = bom

        while True:
            eventlet.sleep(1)
            t2 = os.path.getmtime(kicad_sch_path)
            if t1 != t2:
                print("File updated!")
                break
        
    print("Stopping observer...")


class ProgressSocketio(ProgressConsole):
    def __init__(self, total, logger):
        super().__init__(total, logger)
        self.total = total
        self.logger = logger
        self.value = 0
        print(logger)

    def update(self, val):
        self.value += val
        socketio.emit(
            "progress",
            {
                "value": 100.0 * float(self.value) / float(self.total),
                "total": self.total,
            },
            namespace=namespace,
        )
        # Stupid but it works...
        eventlet.sleep(0)
        super().update(val)


def open_browser(addr: str, port: int):
    webbrowser.open_new_tab(f"{addr}:{port}")


def run(
    kicost_config: str,
    debug: bool,
    quiet: bool,
    kicad_dir: str,
    project_name: str | None,
    database_url: str,
    quantity: list[int],
    currency: str,
    web_open: bool,
    port: int,
):
    # Init kicost
    kicost_log.set_domain("kicost")
    init_all_loggers(
        kicost_log.init(),
        kicost_log.get_logger("kicost.distributors"),
        kicost_log.get_logger("kicost.edas"),
    )
    kicost_log.set_verbosity(get_logger(), debug, quiet)
    set_distributors_progress(ProgressSocketio)

    configure_kicost_apis(
        kicost_config, kicost_config is None, command_line_api_options, None
    )

    # Init kicad schematic monitor
    kicad_dir_path: str = kicad_dir
    kicad_dir_path = kicad_dir_path.removesuffix("/")
    project_name = (
        project_name if project_name is not None else os.path.basename(kicad_dir_path)
    )
    kicad_sch_path = os.path.join(kicad_dir_path, project_name + ".kicad_sch")
    if not os.path.exists(kicad_sch_path):
        print(
            f"No schematic file @ {kicad_sch_path}.\nTry --project-name if project directory do not have the same name as the schematic"
        )
        exit(-1)

    # start server
    socketio.start_background_task(
        background_task,
        socketio,
        kicad_sch_path,
        database_url,
        quantity,
        currency
    )
    print(f"Monitoring {kicad_dir_path}, {project_name}...")

    if web_open:
        Timer(1, open_browser, ("localhost", port)).start()
    socketio.run(app, port=port)

    print("Stopping...")


def main():
    load_dotenv()
    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s - %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S",
    )

    parser = argparse.ArgumentParser(
        prog=__package__,
        description=f"Ki365 BOM preview",
        # epilog=f'See "{WIKIPAGE}" for detailed help and guides',
    )
    default_url = os.environ.get("DATABASE_URL")
    parser.add_argument(
        "--database-url",
        default=default_url,
        help="URL to Ki365 database (default: %(default)s)",
    )
    parser.add_argument(
        "--project-name",
        default=None,
        help="KiCad project name",
    )
    parser.add_argument(
        "kicad_dir",
        help="Kicad project directory",
    )
    parser.add_argument(
        "-q", "--quiet", action="store_true", help="Enable quiet mode with no warnings."
    )
    parser.add_argument(
        "-c",
        "--config",
        nargs="?",
        type=str,
        metavar="CONFIG.YAML",
        help="KiCost configuration file",
    )
    parser.add_argument(
        "--currency",
        type=str,
        choices=list_currencies(),
        default="EUR",
        metavar="CUR",
        help="Currency for total calculation",
    )
    parser.add_argument(
        "--quantity",
        nargs="+",
        default=[1, 10, 25, 50, 100],
        type=int,
        metavar="QTY",
        help="Quantities to consider for total calculation",
    )
    parser.add_argument(
        "-p",
        "--port",
        default=5555,
        type=int,
        help="Server port (default: %(default)s)",
    )
    parser.add_argument(
        "--open",
        action="store_true",
        help="Open in browser",
    )
    parser.add_argument("--debug", action="store_true")
    args = parser.parse_args()

    run(args.config, args.debug, args.quiet, args.kicad_dir, args.project_name, args.database_url, args.quantity, args.currency, args.open, args.port)
