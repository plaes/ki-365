import { reactive } from "vue";
import { Socket, io } from "socket.io-client";
import { Bom } from "frontend_common";

export interface Progress {
  value: number
  total: number
}

interface ServerToClientEvents {
  hello: () => void;
  bill_of_material: (bom: Bom) => void;
  reloading: () => void;
  progress: (progress: Progress) => void;
  //withAck: (d: string, callback: (e: number) => void) => void;
}

interface ClientToServerEvents {
  refresh: () => void;
}

export const state = reactive({
  connected: false,
  loading: false,
  bom: null as Bom | null,
  progress: null as Progress | null
});

// "undefined" means the URL will be computed from the `window.location` object
const URL = process.env.NODE_ENV === "production" ? undefined : "http://localhost:5000";
//const URL = "http://localhost:5555"
export const socket: Socket<ServerToClientEvents, ClientToServerEvents> = io();

socket.on("connect", () => {
  state.connected = true;
});

socket.on("disconnect", () => {
  state.connected = false;
});

socket.on("bill_of_material", (bom) => {
  state.loading = false;
  state.bom = bom;
});
socket.on("reloading", () => {
  state.loading = true;
});
socket.on("progress", (progress) => {
  state.loading = true;
  state.progress = progress;
});