import sys
from threading import Thread
import pcbnew  # type: ignore
import os

KICAD7_3RD_PARTY = os.environ.get("KICAD7_3RD_PARTY")

KI365_DIR = os.environ.get("KI365_DIR", KICAD7_3RD_PARTY + os.pathsep + "ki365")
KI365_DATABASE_URL = os.environ.get("KI365_DATABASE_URL")
KICOST_CONFIG = os.environ.get(
    "KICOST_CONFIG", KICAD7_3RD_PARTY + os.pathsep + "kicost.yml"
)


if KI365_DIR is not None:
    sys.path.append(KI365_DIR + "ki365_common")
    sys.path.append(KI365_DIR + "ki365_bom")

    print(f"Added $KI365_DIR/ki365_[bom/common] to python path")

from ki365_bom import server


class Ki365BomPluginAction(pcbnew.ActionPlugin):
    def defaults(self):
        self.name = "Ki365 Bom Preview"
        self.category = "bom tool"
        self.description = "A description of the plugin"
        self.show_toolbar_button = True  # Optional, defaults to False
        self.icon_file_name = os.path.join(
            os.path.dirname(__file__), "logo.png"
        )  # Optional

    def Run(self):
        # The entry function of the plugin that is executed on user action
        pcb_file = pcbnew.GetBoard().GetFileName()
        project_dir = os.path.dirname(pcb_file)
        filename, ext = os.path.splitext(pcb_file)

        print("Hello World " + " " + project_dir + ", " + os.path.basename(filename))
        Thread(
            target=server.run,
            args=(
                KICOST_CONFIG,
                False,
                True,
                project_dir,
                os.path.basename(filename),
                KI365_DATABASE_URL,
                [1, 10, 100],
                "SEK",
                True,
                5000,
            ),
            daemon=True
        ).start()
