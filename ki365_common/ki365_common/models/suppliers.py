from typing import List, Optional
from sqlalchemy import Enum, ForeignKey
from sqlalchemy.orm import Mapped
from sqlalchemy.orm import mapped_column
from sqlalchemy.orm import relationship

from ki365_common.status import SupplierPartStatus, SupplierStatus
from .base import Base, CommonMixin


class Supplier(CommonMixin, Base):
    __tablename__ = "suppliers"

    # Name of manufacturer
    name: Mapped[str] = mapped_column()
    # Description of manufacturer
    description: Mapped[str] = mapped_column()
    # Link/url to manufacturer
    link: Mapped[str] = mapped_column(nullable=True, default=None)
    # Status of this manufacturer
    status: Mapped[SupplierStatus] = mapped_column(
        Enum(SupplierStatus, values_callable=lambda x: [e.value for e in x])
    )

    parts: Mapped[List["SupplierPart"]] = relationship(
        cascade="all, delete-orphan", back_populates="supplier"
    )

    def __repr__(self) -> str:
        return f"Supplier(id={self.id!r})"


class SupplierPart(CommonMixin, Base):
    __tablename__ = "supplier_parts"

    # Part this maps to
    manufacturer_part_id: Mapped[int] = mapped_column(
        ForeignKey("manufacturer_parts.id")
    )

    # Supplier which provides this part
    supplier_id: Mapped[int] = mapped_column(ForeignKey("suppliers.id"))
    supplier: Mapped["Supplier"] = relationship()

    # Supplier part number
    partno: Mapped[str] = mapped_column()

    # Status of this part
    status: Mapped[SupplierPartStatus] = mapped_column(
        Enum(SupplierPartStatus, values_callable=lambda x: [e.value for e in x])
    )

    def __repr__(self) -> str:
        return f"SupplierPart(id={self.id!r}, partno={self.partno!r}, status={self.status!r})"

class SupplierPartPricing(CommonMixin, Base):
    __tablename__ = "supplier_part_pricings"

    # Part this maps to
    supplier_part_id: Mapped[int] = mapped_column(
        ForeignKey("supplier_parts.id")
    )
    supplier_part: Mapped["SupplierPart"] = relationship()

    currency: Mapped[str] = mapped_column()

    stock: Mapped[Optional[int]] = mapped_column()

    # Supplier part number
    price_breaks: Mapped[List["SupplierPartPriceBreak"]] = relationship(
        cascade="all, delete-orphan"
    )

    def __repr__(self) -> str:
        return f"SupplierPartPricing(id={self.id!r})"

class SupplierPartPriceBreak(CommonMixin, Base):
    __tablename__ = "supplier_part_price_breaks"

    # Part this maps to
    supplier_part_pricing_id: Mapped[int] = mapped_column(
        ForeignKey("supplier_part_pricings.id")
    )

    quantity: Mapped[int] = mapped_column()
    cost: Mapped[float] = mapped_column()

    def __repr__(self) -> str:
        return f"SupplierPartPricing(id={self.id!r})"
