from typing import List, TYPE_CHECKING, Optional
from sqlalchemy.types import Enum
from sqlalchemy.orm import Mapped
from sqlalchemy.orm import mapped_column
from sqlalchemy.orm import relationship

from ki365_common.status import PartStatus

from .base import Base, CommonMixin
from .manufacturers import ManufacturerPart



if TYPE_CHECKING:
    from ki365_common.models.assemblies import Assembly
    

class Part(CommonMixin, Base):
    __tablename__ = "parts"

    # Part title
    title: Mapped[str] = mapped_column()
    # Part description
    description: Mapped[str] = mapped_column()
    # Part status
    status: Mapped[PartStatus] = mapped_column(
        Enum(PartStatus, values_callable=lambda x: [e.value for e in x])
    )

    # List of manufacturer partnumbers
    manufacturer_parts: Mapped[List["ManufacturerPart"]] = relationship(
        cascade="all, delete-orphan"
    )

    assembly: Mapped[Optional["Assembly"]] = relationship(
        cascade="all, delete-orphan"
    )

    def __repr__(self) -> str:
        return f"Part(id={self.id!r}, title={self.title!r})"
