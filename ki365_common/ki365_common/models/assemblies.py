from __future__ import annotations

from typing import List, Optional
from sqlalchemy import Enum, ForeignKey, UniqueConstraint
from sqlalchemy.orm import Mapped
from sqlalchemy.orm import mapped_column
from sqlalchemy.orm import relationship

from ki365_common.models.part import Part
from ki365_common.status import AssemblyRevisionStatus

from .base import Base, CommonMixin


class AssemblyRevisionPartField(CommonMixin, Base):
    __tablename__ = "assembly_revision_part_fields"
    __table_args__ = (
        UniqueConstraint(
            "assembly_rev_part_id", "name", name="_assembly_rev_part_field_name_uc"
        ),
    )

    # Parent assembly part
    assembly_rev_part_id: Mapped[int] = mapped_column(
        ForeignKey("assembly_revision_parts.id")
    )

    # Name and value of the field
    name: Mapped[str] = mapped_column()
    value: Mapped[str] = mapped_column()

    def __repr__(self) -> str:
        return f"AssemblyRevisionPartField(id={self.id!r}, '{self.name!r}'='{self.name!r}')"


class AssemblyRevisionPart(CommonMixin, Base):
    __tablename__ = "assembly_revision_parts"
    __table_args__ = (
        UniqueConstraint(
            "assembly_rev_id", "reference", name="_assembly_rev_part_reference_uc"
        ),
    )

    # Parent assembly
    assembly_rev_id: Mapped[int] = mapped_column(ForeignKey("assembly_revisions.id"))

    # Part
    part_id: Mapped[Optional[int]] = mapped_column(ForeignKey("parts.id"), nullable=True)
    part: Mapped[Optional["Part"]] = relationship()

    # Name of manufacturer
    reference: Mapped[str] = mapped_column()

    # Assosciated fields
    _fields: Mapped[List["AssemblyRevisionPartField"]] = relationship(
        cascade="all, delete-orphan"
    )

    def __repr__(self) -> str:
        return f"AssemblyRevisionPart(id={self.id!r}, reference={self.reference!r}, part={self.part})"

    def get_field(
        self, fname: str, case_sensitive: bool = True, default: str | None = None
    ) -> str | None:
        # Then search in fields
        for f in self._fields:
            if f.name == fname:
                return f.value
            elif not case_sensitive and f.name.lower() == fname.lower():
                return f.value

        # No field found
        return default


class AssemblyRevisionField(CommonMixin, Base):
    __tablename__ = "assembly_revision_fields"
    __table_args__ = (
        UniqueConstraint("assembly_rev_id", "name", name="_assembly_rev_field_name_uc"),
    )

    # Parent assembly
    assembly_rev_id: Mapped[int] = mapped_column(ForeignKey("assembly_revisions.id"))

    # Name of manufacturer
    name: Mapped[str] = mapped_column()
    value: Mapped[str] = mapped_column()

    def __repr__(self) -> str:
        return f"AssemblyRevisionField(id={self.id!r}, '{self.name!r}'='{self.value}')"


class AssemblyRevision(CommonMixin, Base):
    __tablename__ = "assembly_revisions"
    __table_args__ = (
        UniqueConstraint("assembly_id", "revision_slug", name="_assembly_rev_slug_uc"),
    )

    # Supplier which provides this part
    assembly_id: Mapped[int] = mapped_column(ForeignKey("assemblies.id"))
    assembly: Mapped["Assembly"] = relationship(
        viewonly=True, back_populates="revisions"
    )

    # Assembly revision name
    revision: Mapped[str] = mapped_column()

    # Assembly revision slug
    revision_slug: Mapped[str] = mapped_column()

    # Status of this part
    status: Mapped[str] = mapped_column(
        Enum(AssemblyRevisionStatus, values_callable=lambda x: [e.value for e in x])
    )

    # List of parts in this assembly
    parts: Mapped[List["AssemblyRevisionPart"]] = relationship(
        cascade="all, delete-orphan"
    )

    # List of fields/variables/user data of this assembly
    _fields: Mapped[List["AssemblyRevisionField"]] = relationship(
        cascade="all, delete-orphan"
    )

    def __repr__(self) -> str:
        return f"AssemblyRevision(id={self.id!r}, assembly_id={self.assembly_id!r}, revision={self.revision!r}, revision_slug={self.revision_slug!r}, status={self.status!r})"

    def get_field(self, fname: str, default: str | None = None) -> str | None:
        return next((f.value for f in self._fields if f.name == fname), default)


class Assembly(CommonMixin, Base):
    __tablename__ = "assemblies"

    # Part this maps to
    part_id: Mapped[int] = mapped_column(ForeignKey("parts.id"))
    part: Mapped[Part] = relationship(viewonly=True, back_populates="assembly")

    # All revisions of this assembly
    revisions: Mapped[List["AssemblyRevision"]] = relationship(
        cascade="all, delete-orphan", back_populates="assembly"
    )

    def __repr__(self) -> str:
        return f"Assembly(id={self.id!r}, part={self.part!r})"
