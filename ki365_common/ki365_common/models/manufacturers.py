from typing import List
from sqlalchemy import Enum, ForeignKey
from sqlalchemy.orm import Mapped
from sqlalchemy.orm import mapped_column
from sqlalchemy.orm import relationship

from ki365_common.status import ManufacturerPartStatus, ManufacturerStatus

from .base import Base, CommonMixin
from .suppliers import SupplierPart


class Manufacturer(CommonMixin, Base):
    __tablename__ = "manufacturers"

    # Name of manufacturer
    name: Mapped[str] = mapped_column()
    # Description of manufacturer
    description: Mapped[str] = mapped_column()
    # Link/url to manufacturer
    link: Mapped[str] = mapped_column(nullable=True, default=None)
    # Status of this manufacturer
    status: Mapped[ManufacturerStatus] = mapped_column(
        Enum(ManufacturerStatus, values_callable=lambda x: [e.value for e in x])
    )

    parts: Mapped[List["ManufacturerPart"]] = relationship(
        cascade="all, delete-orphan", back_populates="manufacturer"
    )

    def __repr__(self) -> str:
        return f"Manufacturer(id={self.id!r}, name={self.name!r}, desc={self.description!r}, status={self.status!r})"


class ManufacturerPart(CommonMixin, Base):
    __tablename__ = "manufacturer_parts"

    # Part this maps to
    part_id: Mapped[int] = mapped_column(ForeignKey("parts.id"))

    # Supplier which provides this part
    manufacturer_id: Mapped[int] = mapped_column(ForeignKey("manufacturers.id"))
    manufacturer: Mapped["Manufacturer"] = relationship()

    # Manufacturer part number
    partno: Mapped[str] = mapped_column()

    # Status of this part
    status: Mapped[ManufacturerPartStatus] = mapped_column(
        Enum(ManufacturerPartStatus, values_callable=lambda x: [e.value for e in x])
    )

    # List of supplier partnumbers
    supplier_parts: Mapped[List["SupplierPart"]] = relationship(
        cascade="all, delete-orphan"
    )

    def __repr__(self) -> str:
        return f"ManufacturerPart(id={self.id!r}, mfr_id={self.manufacturer_id!r}, partno={self.partno!r}, status={self.status!r})"
