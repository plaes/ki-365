from enum import Enum
from sqlalchemy.types import TypeDecorator, String
from sqlalchemy.orm import DeclarativeBase
from sqlalchemy.orm import Mapped
from sqlalchemy.orm import mapped_column


class Base(DeclarativeBase):
    pass


class CommonMixin:
    """define a series of common elements that may be applied to mapped
    classes using this class as a mixin class."""

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)


# Copied from https://stackoverflow.com/questions/32287299/sqlalchemy-database-int-to-python-enum/38786737#38786737
class EnumAsString(TypeDecorator):
    """Column type for storing Python enums in a database STRING column.

    This will behave erratically if a database value does not correspond to
    a known enum value.
    """
    impl = String # underlying database type

    def __init__(self, enum_type: Enum):
        super(EnumAsString, self).__init__()
        self.enum_type = enum_type

    def process_bind_param(self, value, dialect):
        if isinstance(value, self.enum_type):
            return value.value
        raise ValueError('expected %s value, got %s'
            % (self.enum_type.__name__, value.__class__.__name__))

    def process_result_value(self, value, dialect):
        return self.enum_type(value)

    def copy(self, **kwargs):
        return EnumAsString(self.enum_type)
