from marshmallow import Schema, fields

from ki365_common.schemas.manufacturer import ManufacturerPartSchema
from ki365_common.status import PartStatus


class PartSchema(Schema):
    id = fields.Int(dump_only=True)
    title = fields.Str(required=True)
    description = fields.Str(required=True)
    status = fields.Enum(PartStatus, required=True, by_value=True)
