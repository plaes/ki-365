from marshmallow import Schema, fields

from ki365_common.schemas.suppliers import SupplierPartSchema
from ki365_common.status import ManufacturerPartStatus, ManufacturerStatus


class ManufacturerSchema(Schema):
    #
    id = fields.Int(dump_only=True)
    #
    name = fields.Str(required=True)
    description = fields.Str(required=True)
    link = fields.Str()
    status = fields.Enum(ManufacturerStatus, required=True, by_value=True)


class ManufacturerPartSchema(Schema):
    #
    id = fields.Int(dump_only=True)

    # Manufacturer of this part
    manufacturer_id = fields.Int(required=True, load_only=True)
    manufacturer = fields.Nested(ManufacturerSchema, dump_only=True)

    # Suppliers of this part
    supplier_parts = fields.Nested(SupplierPartSchema, many=True, dump_only=True)

    partno = fields.Str(required=True)
    status = fields.Enum(ManufacturerPartStatus, required=True, by_value=True)
