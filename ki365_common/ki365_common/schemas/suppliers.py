from marshmallow import Schema, fields

from ki365_common.status import SupplierPartStatus, SupplierStatus


class SupplierSchema(Schema):
    #
    id = fields.Int(dump_only=True)
    #
    name = fields.Str(required=True)
    description = fields.Str(required=True)
    link = fields.Str()
    status = fields.Enum(SupplierStatus, required=True, by_value=True)


class SupplierPartSchema(Schema):
    #
    id = fields.Int(dump_only=True)
    #
    supplier_id = fields.Int(required=True, load_only=True)
    supplier = fields.Nested(SupplierSchema, dump_only=True)

    partno = fields.Str(required=True)
    status = fields.Enum(SupplierPartStatus, required=True, by_value=True)
