from marshmallow import Schema, fields, validate

from ki365_common.schemas.manufacturer import ManufacturerPartSchema
from ki365_common.schemas.part import PartSchema
from ki365_common.status import AssemblyRevisionStatus, PartStatus

KI365_PART_REFERENCE_REGEX = "^[A-Z]+[0-9]*$"


class AssemblyFieldSchema(Schema):
    id = fields.Int(dump_only=True)
    name = fields.Str(required=True)
    value = fields.Str(required=True)


class AssemblyRevisionPartSchema(Schema):
    id = fields.Int(dump_only=True)
    reference = fields.Str(
        required=True,
        validate=validate.Regexp(
            KI365_PART_REFERENCE_REGEX,
            error="Invalid reference. Allowed characters are a-z, 0-9 seperated by - and starting with a letter",
        ),
    )
    part = fields.Nested(PartSchema, dump_only=True)
    part_id = fields.Int(load_only=True, required=True)

    _fields = fields.Nested(AssemblyFieldSchema, many=True, data_key="fields")


class AssemblyRevisionSchema(Schema):
    id = fields.Int(dump_only=True)
    revision = fields.Str(validate=lambda r: len(r) >= 1, required=True)
    revision_slug = fields.Str(required=False)
    status = fields.Enum(AssemblyRevisionStatus, by_value=True, required=True)
    parts = fields.Nested(AssemblyRevisionPartSchema, many=True, dump_only=True)

    _fields = fields.Nested(
        AssemblyFieldSchema, many=True, data_key="fields", required=False
    )


class AssemblySchema(Schema):
    id = fields.Int(dump_only=True)
    revisions = fields.Nested(AssemblyRevisionSchema, many=True, exclude=["parts"])
    part = fields.Nested(PartSchema, dump_only=True)
