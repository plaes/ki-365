from marshmallow import Schema, fields as Fields, validate

# regexr.com/7fhe2
KICAD_LIBRARY_REGEX = "^[a-zA-Z0-9_\\-.,+]+:[a-zA-Z0-9_\\-.,+]+$"

# regexr.com/7fhe5
KI365_REFERENCE_REGEX = "^[a-z0-9]+(?:-[a-z0-9]+)*$"


class LibraryFieldSchema(Schema):
    # Human name of the field
    name = Fields.Str(required=True)
    # Key used when encoding the field as json
    key = Fields.Str(dump_only=True)


class LibrarySchema(Schema):
    # Library identifier
    slug = Fields.Str(dump_only=True)

    # Human name
    name = Fields.Str(required=True)

    # Library fields  (except part_id if specified)
    fields = Fields.Nested(LibraryFieldSchema, data_key='fields', many=True) # type: ignore


class DatabaseSchema(Schema):
    # Library identifier
    slug = Fields.Str(dump_only=True)

    # Human name
    name = Fields.Str(required=True)
    description = Fields.Str(required=True)


    # Library fields  (except part_id if specified)
    libraries = Fields.Nested(LibrarySchema, many=True)


class LibraryPartSchema(Schema):
    id = Fields.Int(dump_only=True)
    part_id = Fields.Int(required=True)

    # ^[a-zA-Z0-9_\-.,+]+:[a-zA-Z0-9_\-.,+]+$
    # regexr.com/7fhe2
    reference = Fields.Str(
        required=True,
        validate=validate.Regexp(
            KI365_REFERENCE_REGEX,
            error="Invalid reference. Allowed characters are a-z, 0-9 seperated by - and starting with a letter",
        ),
    )
    symbols = Fields.Str(
        required=True,
        validate=validate.Regexp(
            KICAD_LIBRARY_REGEX,
            error="Invalid symbol, allowed symbols are a-z,0-9 and '-_.,+'. Library and symbol name must be seperated by ':'.",
        ),
    )
    footprints = Fields.Str(
        required=True,
        validate=validate.Regexp(
            KICAD_LIBRARY_REGEX,
            error="Invalid footprint, allowed symbols are a-z,0-9 and '-_.,+'. Library and symbol name must be seperated by ':'.",
        ),
    )

    description = Fields.Str()
    keywords = Fields.Str(
        validate=validate.ContainsNoneOf(
            ",", error="Comma not allowed, use space to seperate keywords"
        )
    )

    exclude_from_bom = Fields.Bool(required=False)
    exclude_from_board = Fields.Bool(required=False)

    #
    fields = Fields.Mapping(Fields.Str, Fields.Str, data_key='fields', required=False) # type: ignore
