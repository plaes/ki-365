from typing import Any


TRUE_VALUES = ["true", "t", "1", "yes", "y"]
FALSE_VALUES = ["false", "f", "0", "no", "n"]


def to_bool(value: str | Any):
    if not isinstance(value, str):
        return bool(value)
    elif value.lower() in TRUE_VALUES:
        return True
    elif value.lower() in FALSE_VALUES:
        return False

    raise ValueError(
        "Expected one of " + ", ".join(TRUE_VALUES) + " or " + ", ".join(FALSE_VALUES)
    )
