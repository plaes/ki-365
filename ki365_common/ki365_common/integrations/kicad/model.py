from typing import Dict, Type
from sqlalchemy import (
    Column,
    ForeignKey,
    String,
)
from sqlalchemy.orm import Mapped
from sqlalchemy.orm import mapped_column


from ki365_common.integrations.kicad.kicad_dbl import KicadDblLibrary


from ...models.base import Base, CommonMixin


class KicadPart:
    # Libraries must have a part_id
    part_id: Mapped[int] = mapped_column(ForeignKey("parts.id"))

    # Required columns
    reference: Mapped[str] = mapped_column(unique=True)
    symbols: Mapped[str] = mapped_column()
    footprints: Mapped[str] = mapped_column()

    description: Mapped[str] = mapped_column()
    keywords: Mapped[str] = mapped_column()
    _exclude_from_bom: Mapped[int] = mapped_column('exclude_from_bom', default=0)
    _exclude_from_board: Mapped[int] = mapped_column('exclude_from_board', default=0)

    @property
    def fields(self) -> Dict[str, str]:
        return {
            name.removeprefix("field_"): val
            for name, val in self.__dict__.items()
            if name.startswith("field_")
        }

    @fields.setter
    def fields(self, values: Dict[str, str]):
        for name, val in values.items():
            key = f"field_{name}"
            if not hasattr(self, key):
                raise KeyError(name)
            setattr(self, key, val)
    
    @property
    def exclude_from_bom(self) -> bool:
        return bool(self._exclude_from_bom) 
    
    @exclude_from_bom.setter
    def exclude_from_bom(self, value: bool):
        self._exclude_from_bom = int(value) 
    
    @property
    def exclude_from_board(self) -> bool:
        return bool(self._exclude_from_board) 
    
    @exclude_from_board.setter
    def exclude_from_board(self, value: bool):
        self._exclude_from_board = int(value) 


def create_mapping_from_dbl(lib: KicadDblLibrary) -> Type[KicadPart]:
    """Create a Sqlalchemy table class from the stored schema

    Returns:
        Cls: A new database mapping
    """
    custom_fields = {
        # Use table name from dbl
        "__tablename__": lib.table,
        # meta
        "dbl": lib,
        # Fields
        # Do not re-add part_id if listed as a field
        **{
            f"field_{field.key}": Column(field.column, String)
            for field in lib.fields
            if field.column != "part_id"
        },
    }

    # Create a declarative class for this library
    Cls = type(
        # TODO: Properly create name
        lib.table.replace("_", " ").replace("-", " ").title().replace(" ", ""),
        (Base, CommonMixin, KicadPart),
        custom_fields,
    )

    return Cls
