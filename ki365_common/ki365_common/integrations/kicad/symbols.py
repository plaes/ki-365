from __future__ import annotations

from io import TextIOWrapper
from multiprocessing import Pool
import os
from pprint import pprint
from typing import ItemsView
from dotenv import load_dotenv
from marshmallow import Schema, fields
from sexpdata import load, loads, Symbol as SeSymbol, car, cdr # type: ignore
import slugify

from ki365_common.integrations.kicad.library_table import LibraryTable, LibraryTableItem


def yes_no_bool(val: str):
    if val.lower() == "yes":
        return True
    elif val.lower() == "no":
        return False
    else:
        raise ValueError("Expected 'yes' or 'no'")


class SymbolProperty:
    key: str
    value: str

    def __init__(self, sexpr: list[SeSymbol]) -> None:
        super().__init__()
        self.parse_symbol_property(sexpr)

    def parse_symbol_property(self, sexpr: list[SeSymbol]):
        if car(sexpr) != SeSymbol("property"):
            raise ValueError("Not a 'property'")

        self.key, self.value, *_tokens = cdr(sexpr)

    def __repr__(self) -> str:
        return f"Property({self.key!r}, {self.value!r})"


class Symbol:
    library_id: str
    extends: str | None
    in_bom: bool
    on_board: bool
    properties: dict[str, str]

    def __init__(self, sexpr: list[SeSymbol]) -> None:
        super().__init__()
        self.extends = None
        self.in_bom = True
        self.on_board = True
        self.properties = {}
        self.parse_symbol(sexpr)

    def parse_symbol(self, sexpr: list[SeSymbol]):
        if car(sexpr) != SeSymbol("symbol"):
            raise ValueError("Not a 'symbol'")

        self.library_id, *tokens = cdr(sexpr)  # type: ignore

        # print('LIBRARY_ID: ' + self.library_id)

        for tok in tokens:
            if car(tok) == SeSymbol("extends"):
                self.extends = str(cdr(tok)[0])
            elif car(tok) == SeSymbol("in_bom"):
                self.in_bom = yes_no_bool(str(cdr(tok)[0]))
            elif car(tok) == SeSymbol("on_board"):
                self.on_board = yes_no_bool(str(cdr(tok)[0]))
            elif car(tok) == SeSymbol("property"):
                prop = SymbolProperty(tok)  # type: ignore
                # print('\tPROP: ' + repr(prop))
                self.properties[prop.key] = prop.value
            else:
                pass  # Ignore unknown

    @property
    def keywords(self) -> str | None:
        return self.properties.get("ki_keywords")

    @property
    def description(self) -> str | None:
        return self.properties.get("ki_description")

    @property
    def fp_filters(self) -> str | None:
        return self.properties.get("ki_fp_filters")

    @property
    def footprint(self) -> str | None:
        return self.properties.get("Footprint")


class SymbolLibrary:
    version: str
    generator: str
    _symbols: dict[str, Symbol]

    def __init__(self, sym_lib: TextIOWrapper | str) -> None:
        super().__init__()
        self._symbols = {}
        
        sexpr: list[SeSymbol] = loads(sym_lib) if isinstance(sym_lib, str) else load(sym_lib)
        self.parse_library(sexpr)
        # Some parts extends a parent part
        # Merge parent properties into the child
        self.resolve_parts()

    def parse_library(self, sexpr: list[SeSymbol]):
        if car(sexpr) != SeSymbol("kicad_symbol_lib"):
            raise ValueError("Not a 'kicad_symbol_lib' file")

        tokens: list[SeSymbol] = cdr(sexpr)  # type: ignore
        for tok in tokens:
            if car(tok) == SeSymbol("version"):
                self.version = str(cdr(tok)[0])
            elif car(tok) == SeSymbol("generator"):
                self.generator = str(cdr(tok)[0])
            elif car(tok) == SeSymbol("symbol"):
                sdef = Symbol(tok)  # type: ignore
                self._symbols[sdef.library_id] = sdef
            else:
                raise ValueError(f"Unknown symbol '{car(tok)}'")

    def resolve_parts(self):
        for sym in self._symbols.values():
            if sym.extends is not None:
                parent_sym = self._symbols.get(sym.extends)

                if parent_sym is not None:
                    # Add properties not defined in child symbol
                    for k, v in parent_sym.properties.items():
                        if sym.properties.get(k) is None:
                            sym.properties[k] = v
                else:
                    # Delete parts with invalid parent
                    del self._symbols[sym.library_id]
                

    @property
    def symbols(self) -> list[Symbol]:
        return list(self._symbols.values())


class SymbolSchema(Schema):
    library_id = fields.Str()
    extends = fields.Str()
    in_bom = fields.Bool()
    on_board = fields.Bool()
    description = fields.Str(required=False)
    keywords = fields.Str(required=False)
    fp_filters = fields.Str(required=False)
    footprint = fields.Str()


class SymbolLibrarySchema(Schema):
    symbols = fields.Nested(SymbolSchema, many=True)


def load_library(l: LibraryTableItem) -> tuple[LibraryTableItem, SymbolLibrary]:
    uri = l.get_uri()
    # print(f"{l.name}: {uri}")
    with open(uri) as symbol_lib_file:
        symbol_lib = SymbolLibrary(symbol_lib_file)
        return (l, symbol_lib)


def load_symbol_libraries(
    sym_lib_table_path: str,
) -> tuple[LibraryTable, list[tuple[LibraryTableItem, SymbolLibrary]]] | None:
    pool = Pool()

    try:
        sym_lib_table_f = open(sym_lib_table_path)
        sym_lib_table = LibraryTable(sym_lib_table_f, "sym_lib_table")
        # pprint(sym_lib_table)

        sym_libs = pool.map(load_library, sym_lib_table.libraries)

        return sym_lib_table, sym_libs
    except IOError as ioerr:
        print("Failed to open sym-lib-table", ioerr)
        return None


if __name__ == "__main__":
    load_dotenv()
    pool = Pool()

    symbols_dir = os.environ["KICAD7_CONFIG_DIR"]
    with open(os.path.join(symbols_dir, "sym-lib-table")) as sym_lib_table_f:
        sym_lib_table = LibraryTable(sym_lib_table_f, "sym_lib_table")

        libraries = pool.map(load_library, sym_lib_table.libraries)

        # pprint(SymbolTableSchema(many=True).dump(sym_lib_table.libraries))

        for nick, lib in libraries:
            pprint(
                {
                    "nick": SymbolLibrarySchema().dump(
                        dict(**nick.__dict__, slug=slugify.slugify(nick.name))
                    ),
                    "symbols": SymbolSchema(many=True).dump(lib.symbols),
                }
            )
