from io import TextIOWrapper
from multiprocessing import Pool
import os
from pprint import pprint
from typing import ItemsView
from dotenv import load_dotenv
from marshmallow import Schema, fields
from sexpdata import load, loads, Symbol as SeSymbol, car, cdr  # type: ignore
import slugify

from ki365_common.integrations.kicad.library_table import LibraryTable, LibraryTableItem


class FootprintProperty:
    key: str
    value: str

    def __init__(self, sexpr: list[SeSymbol]) -> None:
        super().__init__()
        self.parse_symbol_property(sexpr)

    def parse_symbol_property(self, sexpr: list[SeSymbol]):
        if car(sexpr) != SeSymbol("property"):
            raise ValueError("Not a 'property'")

        self.key, self.value, *_tokens = cdr(sexpr)

    def __repr__(self) -> str:
        return f"Property({self.key!r}, {self.value!r})"


class Footprint:
    version: str
    generator: str
    library_id: str
    description: str
    keywords: str
    properties: list[FootprintProperty]

    def __init__(self, module: TextIOWrapper | str) -> None:
        super().__init__()
        self.in_bom = True
        self.on_board = True
        self.properties = []
        self.description = ""
        self.keywords = ""
        sexpr: list[SeSymbol] = (
            loads(module) if isinstance(module, str) else load(module)
        )
        self.parse_module(sexpr)

    def parse_module(self, sexpr: list[SeSymbol]):
        if car(sexpr) != SeSymbol("footprint") and car(sexpr) != SeSymbol("module"):
            raise ValueError("Not a 'footprint'")

        self.library_id, *tokens = cdr(sexpr)  # type: ignore
        for tok in tokens:
            # print(repr(tok))
            if car(tok) == SeSymbol("version"):
                self.version = str(cdr(tok)[0])
            elif car(tok) == SeSymbol("generator"):
                self.generator = str(cdr(tok)[0])
            elif car(tok) == SeSymbol("descr"):
                self.description = str(cdr(tok)[0])
            elif car(tok) == SeSymbol("tags"):
                self.keywords = str(cdr(tok)[0])
            elif car(tok) == SeSymbol("property"):
                prop = FootprintProperty(tok)  # type: ignore
                # print('\tPROP: ' + repr(prop))
                self.properties.append(prop)
            else:
                pass


class FootprintLibrary:
    footprints: list[Footprint]

    def __init__(self, path: str) -> None:
        super().__init__()
        self.footprints = []
        self.parse_library(path)

    def parse_library(self, path: str):
        for fp in os.listdir(path):
            if not fp.endswith(".kicad_mod"):
                continue

            fp_path = os.path.join(path, fp)

            with open(fp_path) as fp_f:
                footprint = Footprint(fp_f)
                self.footprints.append(footprint)


class FootprintSchema(Schema):
    library_id = fields.Str()
    description = fields.Str(required=False)
    keywords = fields.Str(required=False)


class FootprintLibrarySchema(Schema):
    footprints = fields.Nested(FootprintSchema, many=True)


def load_library(l: LibraryTableItem) -> tuple[LibraryTableItem, FootprintLibrary]:
    u = l.get_uri()
    fp_lib = FootprintLibrary(u)
    return (l, fp_lib)


def load_footprint_libraries(
    fp_lib_table_path: str,
) -> tuple[LibraryTable, list[tuple[LibraryTableItem, FootprintLibrary]]] | None:
    pool = Pool()

    try:
        fp_lib_table_f = open(fp_lib_table_path)
        fp_lib_table = LibraryTable(fp_lib_table_f, "fp_lib_table")
        # pprint(sym_lib_table)

        fp_libs = pool.map(load_library, fp_lib_table.libraries)

        return fp_lib_table, fp_libs
    except IOError as ioerr:
        print("Failed to open fp-lib-table", ioerr)
        return None
