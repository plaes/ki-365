from io import TextIOWrapper
from marshmallow import Schema, fields
from sexpdata import Symbol as Sym, car, cdr, load, loads  # type: ignore


import os
from typing import ItemsView, List

from slugify import slugify


class LibraryTableItem:
    name: str
    slug: str
    uri: str
    options: str | None
    description: str | None

    def __init__(self, sexpr: list[Sym]) -> None:
        super().__init__()
        self.parse_lib(sexpr)
        if self.name is None or self.uri is None:
            raise ValueError("Missing required symbols")

    def parse_lib(self, sexpr: list[Sym]):
        if car(sexpr) != Sym("lib"):
            raise ValueError("Not a symbol library")

        for lib in cdr(sexpr):
            if car(lib) == Sym("name"):
                self.name = str(cdr(lib)[0])
                self.slug = slugify(self.name)
            elif car(lib) == Sym("type"):
                type = str(cdr(lib)[0])
                assert type.lower() == "kicad", f"Unknown library type '{type}'"
            elif car(lib) == Sym("uri"):
                self.uri = str(cdr(lib)[0])
            elif car(lib) == Sym("options"):
                self.options = str(cdr(lib)[0])
            elif car(lib) == Sym("descr"):
                self.description = str(cdr(lib)[0])
            else:
                raise ValueError(f"Unknown symbol '{car(lib)}'")

    def get_uri(self, environs: ItemsView[str, str] | None = None) -> str:
        if environs is None:
            environs = os.environ.items()
        res = self.uri
        for name, value in environs:
            res = res.replace(f"${{{name}}}", value)
        return res

    def __repr__(self) -> str:
        return f"Lib({self.name!r}, uri={self.uri!r}, options={self.options!r}, descr={self.description!r})"


class LibraryTable:
    version: str | None
    libraries: List[LibraryTableItem]

    def __init__(self, lib_table: TextIOWrapper | str, table_type: str) -> None:
        super().__init__()
        self.version = None
        self.libraries = []
        self.table_type = table_type
        sexpr: list[Sym] = (
            loads(lib_table) if isinstance(lib_table, str) else load(lib_table)
        )
        self.parse_lib_table(sexpr, table_type)

    def parse_lib_table(self, sexpr: list[Sym], table_type: str):
        if car(sexpr) != Sym(table_type):
            raise ValueError(f"Not a '{table_type}' file")

        for lib in cdr(sexpr):
            if car(lib) == Sym("version"):
                # Documentation implies that this should be a string in YYMMDD fromat
                # but I've only seen a number...
                self.version = str(cdr(lib)[0])
                assert (
                    self.version == "7"
                ), f"Unknown xxx-lib-table version '{self.version}'"
            elif car(lib) == Sym("lib"):
                slib = LibraryTableItem(lib)  # type: ignore
                self.libraries.append(slib)
            else:
                raise ValueError(f"Unknown symbol '{car(lib)}'")

    def __repr__(self) -> str:
        libraries = ",\n".join([f"\t{l!r}" for l in self.libraries])
        return f"""LibTable(version={self.version!r}, libraries=[{
            libraries
        }\n])"""


class LibraryTableItemSchema(Schema):
    name = fields.Str()
    slug = fields.Str()
    description = fields.Str()


class LibraryTableSchema(Schema):
    libraries = fields.Nested(LibraryTableItemSchema, many=True)
