from dataclasses import dataclass, field
import json
from pprint import pprint
from typing import List, Optional
from marshmallow import EXCLUDE, INCLUDE

import marshmallow_dataclass


@dataclass
class KicadDblMeta:
    version: int
    filename: str

    # Ignore unknown fields
    class Meta:
        unknown = EXCLUDE


@dataclass
class KicadDblSource:
    type: str
    # dsn: Optional[str]
    # username: Optional[str]
    # password: Optional[str]
    # timeout_seconds: Optional[int]
    connection_string: Optional[str]

    # Ignore unknown fields
    class Meta:
        unknown = EXCLUDE


@dataclass
class KicadDblField:
    column: str = field(metadata=dict(load_only=True))
    name: str
    key: Optional[str]

    # Ignore unknown fields
    class Meta:
        unknown = EXCLUDE


@dataclass
class KicadDblProperties:
    description: Optional[str]
    keywords: Optional[str]
    exclude_from_bom: Optional[str]
    exclude_from_board: Optional[str]

    # Ignore unknown fields
    class Meta:
        unknown = EXCLUDE


@dataclass
class KicadDblLibrary:
    name: str
    table: str = field(metadata=dict(load_only=True))
    slug: Optional[str]
    key: str = field(metadata=dict(load_only=True))
    symbols: str = field(metadata=dict(load_only=True))
    footprints: str = field(metadata=dict(load_only=True))
    fields: List[KicadDblField]
    properties: Optional[KicadDblProperties] = field(metadata=dict(load_only=True))

    # Ignore unknown fields
    class Meta:
        unknown = EXCLUDE


@dataclass
class KicadDbl:
    meta: KicadDblMeta = field(metadata=dict(load_only=True))
    name: str
    description: str
    slug: Optional[str]
    # Database source
    source: KicadDblSource = field(metadata=dict(load_only=True))
    # List of libraries
    libraries: List[KicadDblLibrary]

    # Ignore unknown fields
    class Meta:
        unknown = EXCLUDE


KicadDblSchema = marshmallow_dataclass.class_schema(KicadDbl)

if __name__ == "__main__":
    with open("examples/example.kicad_dbl") as dbl:
        data = json.load(dbl)
        kdbl: KicadDbl = KicadDblSchema().load(data) # type: ignore
        print(kdbl)
