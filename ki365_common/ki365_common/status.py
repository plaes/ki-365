from enum import Enum
from typing_extensions import override


class Status:
    def is_warning(self) -> bool:
        raise NotImplementedError

    def is_error(self) -> bool:
        raise NotImplementedError
    
    def is_preferred(self) -> bool:
        return False
    
    @property
    def score(self) -> int:
        if self.is_error():
            return -1
        if self.is_preferred():
            return 1
        return 0
    
    def __lt__(self, other):
        return self.score < other.score

class PartStatus(Status, Enum):

    PRELIMINARY = "preliminary"
    ACTIVE = "active"
    NOT_RECOMMENDED = "not-recommended"
    OBSOLETE = "obsolete"

    @override
    def is_warning(self) -> bool:
        return self.value == self.PRELIMINARY or self.value == self.NOT_RECOMMENDED

    @override
    def is_error(self) -> bool:
        return self.value == self.OBSOLETE


class ManufacturerStatus(Status, Enum):

    PRELIMINARY = "preliminary"
    ACTIVE = "active"
    PREFERRED = "preferred"
    NOT_RECOMMENDED = "not-recommended"
    DO_NOT_USE = "do-not-use"
    DEFUNCT = "defunct"

    @override
    def is_warning(self) -> bool:
        return self.value == self.PRELIMINARY or self.value == self.NOT_RECOMMENDED

    @override
    def is_error(self) -> bool:
        return self.value == self.DEFUNCT or self.value == self.DO_NOT_USE
    
    @override
    def is_preferred(self) -> bool:
        return self.value == self.PREFERRED


# Uses the same status codes as Part
class ManufacturerPartStatus(Status, Enum):
    PRELIMINARY = "preliminary"
    ACTIVE = "active"
    PREFERRED = "preferred"
    NOT_RECOMMENDED = "not-recommended"
    OBSOLETE = "obsolete"

    @override
    def is_warning(self) -> bool:
        return self.value == self.PRELIMINARY or self.value == self.NOT_RECOMMENDED

    @override
    def is_error(self) -> bool:
        return self.value == self.OBSOLETE
    
    @override
    def is_preferred(self) -> bool:
        return self.value == self.PREFERRED

# USes the same status codes as manufacturers (for now)
class SupplierStatus(Status, Enum):
    PRELIMINARY = "preliminary"
    ACTIVE = "active"
    PREFERRED = "preferred"
    NOT_RECOMMENDED = "not-recommended"
    DO_NOT_USE = "do-not-use"
    DEFUNCT = "defunct"

    @override
    def is_warning(self) -> bool:
        return self.value == self.PRELIMINARY or self.value == self.NOT_RECOMMENDED

    @override
    def is_error(self) -> bool:
        return self.value == self.DEFUNCT or self.value == self.DO_NOT_USE
    
    @override
    def is_preferred(self) -> bool:
        return self.value == self.PREFERRED

# Uses the same status codes as Part
class SupplierPartStatus(Status, Enum):
    PRELIMINARY = "preliminary"
    ACTIVE = "active"
    PREFERRED = "preferred"
    NOT_RECOMMENDED = "not-recommended"
    OBSOLETE = "obsolete"

    @override
    def is_warning(self) -> bool:
        return self.value == self.PRELIMINARY or self.value == self.NOT_RECOMMENDED

    @override
    def is_error(self) -> bool:
        return self.value == self.OBSOLETE

    @override
    def is_preferred(self) -> bool:
        return self.value == self.PREFERRED

# Uses the same status codes as Part
class AssemblyRevisionStatus(Status, Enum):
    PRELIMINARY = "preliminary"
    ACTIVE = "active"
    OBSOLETE = "obsolete"

    @override
    def is_warning(self) -> bool:
        return self.value == self.PRELIMINARY

    @override
    def is_error(self) -> bool:
        return self.value == self.OBSOLETE

