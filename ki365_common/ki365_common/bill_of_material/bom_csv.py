from csv import DictReader, DictWriter
import os
from pprint import pprint
import re

from .bom import Bom, BomGrouped, BomPart, BomPartGroup, BomProject

CSV_REFERENCES = re.compile(r"([a-z]+)([0-9]+)-([a-z]*)([0-9]+)", re.I)
CSV_REFERENCE = re.compile(r"([a-z]+)([0-9]+)", re.I)


def sanitize_refs(references_str: str) -> list[str]:
    result = []

    references: list[str] = [ref.strip() for ref in references_str.split(",")]

    for ref in references:
        match = CSV_REFERENCES.match(ref)
        if match:
            designator1, start, _designator2, stop = match.groups()
            result.extend([designator1 + str(x) for x in range(int(start), int(stop))])
        else:
            result.append(ref)

    return result


def unsanitize_refs(references: list[str]) -> str:
    result = []
    seq_designator = None
    start_num = 0
    stop_num = 0

    def ref_key(ref: str) -> tuple[str, int]:
        match = CSV_REFERENCE.match(ref)
        if match:
            designator, num_str = match.groups()
            return designator, int(num_str)
        else:
            return ref, 0

    refs = [ref_key(ref) for ref in references]

    for designator, num in sorted(refs, key=lambda k: k[1] or k[0]):
        # print((designator, num))
        if num:
            if seq_designator == designator and num == stop_num + 1:
                stop_num += 1
            else:
                # emit
                if seq_designator is not None:
                    if start_num != stop_num:
                        result.append(
                            f"{seq_designator}{start_num}-{seq_designator}{stop_num}"
                        )
                    else:
                        result.append(f"{seq_designator}{start_num}")

                # restart
                seq_designator = designator
                start_num = num
                stop_num = num

        else:
            result.append(f"{designator}{num}")

    if seq_designator is not None:
        if start_num != stop_num:
            result.append(f"{seq_designator}{start_num}-{seq_designator}{stop_num}")
        else:
            result.append(f"{seq_designator}{start_num}")

    return ", ".join(result)


def bom_from_csv(
    csv_file: str,
    encoding: str | None = None,
    part_id_field: str = "Part#",
    reference_field: str = "Reference",
    value_field: str = "Value",
    datasheet_field: str = "Datasheet",
    **kwargs,
) -> Bom:
    filename, _file_extension = os.path.splitext(csv_file)
    title = os.path.basename(filename).replace("_", " ").replace("-", " ").title()
    parts: list[BomPart] = []

    with open(csv_file, newline="", encoding=encoding) as csvfile:
        reader = DictReader(csvfile, **kwargs)

        for sort, row in enumerate(reader):
            part_id_str = row[part_id_field]
            try:
                part_id = int(part_id_str)
            except ValueError:
                part_id = None
                part_id_str = row[reference_field]

            references: list[str] = sanitize_refs(row[reference_field])
            for ref in references:
                datasheet = row.get(datasheet_field, "")
                title = row[value_field]
                bom_part = BomPart(sort, ref, part_id, datasheet, title=title)
                parts.append(bom_part)

    proj = BomProject(filename, title, revision=None, properties={})

    return Bom(proj, parts)


def bom_to_csv(
    bom: Bom | BomGrouped,
    csv_file: str,
    encoding: str | None = None,
    part_id_field: str = "Part#",
    reference_field: str = "Reference",
    quantity_field: str = "Qty",
    title_field: str = "Value",
    datasheet_field: str = "Datasheet",
    manufacturer_field: str = "Manufacturer",
    supplier_field: str = "Supplier",
    partno_field: str = "Part",
    cost_field: str = "Cost",
    currency_field: str = "Currency",
    stock_field: str = "Stock",
):
    field_names = dict.fromkeys(
        [
            part_id_field,
            reference_field,
            quantity_field,
            title_field,
            datasheet_field,
        ]
    )
    rows = []
    if isinstance(bom, BomGrouped):
        for group in bom.groups:
            row = {
                part_id_field: str(group.part_id) if group.part_id is not None else "",
                reference_field: unsanitize_refs(group.references),
                quantity_field: str(group.quantity),
                title_field: group.title,
                datasheet_field: group.datasheet,
            }
            for i, m in enumerate(group.manufacturers):
                row = {
                    **row,
                    **{
                        f"{manufacturer_field}{i+1}": m.manufacturer.name,
                        f"{manufacturer_field}{i+1} {partno_field}": m.partno,
                    },
                }

                for j, s in enumerate(m.suppliers):
                    row = {
                        **row,
                        **{
                            f"{manufacturer_field}{i+1} {supplier_field}{j+1}": s.supplier.name,
                            f"{manufacturer_field}{i+1} {supplier_field}{j+1} {partno_field}": s.partno,
                        },
                    }
                    if s.stock:
                        row = {
                            **row,
                            f"{manufacturer_field}{i+1} {supplier_field}{j+1} {cost_field}": ",".join(
                                f"{p.cost};{p.quantity}" for p in s.stock.pricing
                            ),
                            f"{manufacturer_field}{i+1} {supplier_field}{j+1} {currency_field}": s.stock.currency,
                            f"{manufacturer_field}{i+1} {supplier_field}{j+1} {stock_field}": str(s.stock.in_stock),
                        }

            rows.append(row)

            # Add any unknown keys
            field_names = {**field_names, **row}
    else:
        for part in bom.parts:
            row = {
                part_id_field: str(part.part_id) if part.part_id is not None else "",
                reference_field: part.reference,
                quantity_field: '1',
                title_field: part.title or '',
                datasheet_field: part.datasheet,
            }
            rows.append(row)
            

    with open(csv_file, "w", encoding=encoding) as csvfile:
        writer = DictWriter(csvfile, list(field_names))
        writer.writeheader()
        writer.writerows(rows)

        if bom.project.total_costs:
            writer.writer.writerow([])
            writer.writer.writerow(
                [quantity_field, f"Total [{bom.project.total_costs.currency}]"]
            )
            for p in bom.project.total_costs.pricing:
                writer.writer.writerow([p.quantity, p.cost])
