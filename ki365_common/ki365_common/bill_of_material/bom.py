from dataclasses import dataclass, field, fields
from enum import Enum
import logging
from typing import List, Optional
from marshmallow import post_dump
import marshmallow_dataclass


from ki365_common.status import (
    ManufacturerPartStatus,
    ManufacturerStatus,
    SupplierPartStatus,
    SupplierStatus,
)

logger = logging.getLogger("bom")

MISSING_PART_ID = -1


class Severity(Enum):
    OK = 10  # Nothing to note / debug information
    INFO = 20  # Informative
    WARNING = 30  # Warning (might not be alright?)
    ERROR = 40  # Error (can continue)
    CRITICAL = 50  # Cannot continue


class BomCommonSchema:
    @post_dump(pass_original=True, pass_many=True)
    def remove_skip_values(self, data, original=None, **kwargs):
        if original:
            for f in fields(original):
                field_name = f.metadata.get("data_key", f.name)
                if field_name in data and f.metadata.get("remove"):
                    # Remove data if it's falsish
                    if not data[field_name]:
                        del data[field_name]
        return data

    class Meta:
        ordered = True


@dataclass
class BomLogEvent(BomCommonSchema):
    """Different price breaks"""

    severity: Severity
    message: str
    source: str | None = None

    def __str__(self) -> str:
        msg = self.message
        if self.source:
            msg = f"{self.source}: {self.message}"

        return f"{self.severity.name}: {msg}"

    def log(self, logger: logging.Logger | str | None = None):
        if not isinstance(logger, logging.Logger):
            logger = logging.getLogger(logger)

        logger.log(self.severity.value, f"{self.source}: {self.message}")


@dataclass
class BomPriceBreak:
    """Different price breaks"""

    quantity: int
    cost: float


@dataclass
class BomSupplierStock(BomCommonSchema):
    """Stock information fetched from supplier"""

    status: str
    in_stock: int
    currency: str
    pricing: List[BomPriceBreak] = field(metadata=dict(remove=True))


BomSupplierStockSchema = marshmallow_dataclass.class_schema(BomSupplierStock)


@dataclass
class BomSupplier(BomCommonSchema):
    id: int
    name: str
    link: str
    status: SupplierStatus = field(metadata=dict(by_value=True))


@dataclass
class BomSupplierPart(BomCommonSchema):
    id: int
    partno: str
    status: SupplierPartStatus = field(metadata=dict(by_value=True))
    supplier: BomSupplier
    stock: BomSupplierStock | None = field(metadata=dict(remove=True))


@dataclass
class BomManufacturer(BomCommonSchema):
    id: int
    name: str
    link: str
    status: ManufacturerStatus = field(metadata=dict(by_value=True))


@dataclass
class BomManufacturerPart(BomCommonSchema):
    """A manuacturer part to be exported"""

    id: int
    partno: str
    status: ManufacturerPartStatus = field(metadata=dict(by_value=True))
    manufacturer: BomManufacturer
    suppliers: List[BomSupplierPart]


@dataclass
class BomPart(BomCommonSchema):
    sort: int
    reference: str
    part_id: Optional[int]
    datasheet: str
    fields: dict[str, str] | None = field(default=None, metadata=dict(remove=True))
    title: Optional[str] = field(default=None, metadata=dict(remove=True))


@dataclass
class BomPartGroup(BomCommonSchema):
    sort: int
    references: List[str]
    part_id: Optional[int]
    quantity: int
    datasheet: str

    # List of manufacturer parts
    manufacturers: List[BomManufacturerPart]

    title: Optional[str] = field(default=None, metadata=dict(remove=True))

    # Log events related to this group
    log_events: List[BomLogEvent] | None = field(
        default=None, metadata=dict(remove=True)
    )

    def status(self) -> Severity:
        sev = Severity.OK
        if self.log_events is not None:
            for evt in self.log_events:
                if evt.severity.value > sev.value:
                    sev = evt.severity
        return sev

    # TODO: Check preferred instead of first
    def calculate_cost(self, project_qty: int) -> tuple[float | None, str]:
        cost = None
        moq = 1  # TODO
        total_qty = max(project_qty * self.quantity, moq)

        # Find first manufacturer with a supplier who has stock
        manufacturer: BomManufacturerPart | None = next(
            (
                manufacturer
                for manufacturer in self.manufacturers
                if any(
                    [
                        s.stock is not None and s.stock.in_stock >= total_qty
                        for s in manufacturer.suppliers
                    ]
                )
            ),
            None,
        )

        if manufacturer is None:
            return None, ""

        # Find first supplier with sufficient stock
        # TODO: rating
        supplier = next(
            supplier
            for supplier in manufacturer.suppliers
            if supplier.stock is not None and supplier.stock.in_stock >= total_qty
        )
        if supplier.stock is None:
            return None, ""

        prices = sorted(supplier.stock.pricing, key=lambda price: price.quantity)
        for price in prices:
            # Price break is above the total quantity
            if price.quantity > total_qty:
                return cost, supplier.stock.currency

            # Calculate cost with this price break
            cost = float(total_qty) * price.cost

        return cost, supplier.stock.currency


@dataclass
class BomProjectPrice(BomCommonSchema):
    """Different price breaks"""

    pricing: List[BomPriceBreak]
    currency: str


@dataclass
class BomProject(BomCommonSchema):
    source: str
    title: str
    revision: str | None = field(metadata=dict(remove=True))
    properties: dict[str, str] = field(metadata=dict(remove=True))
    total_costs: BomProjectPrice | None = field(
        default=None, metadata=dict(remove=True)
    )
    part_id: int | None = field(default=None, metadata=dict(remove=True))


@dataclass
class BomGrouped(BomCommonSchema):
    project: BomProject
    groups: list[BomPartGroup]


BomGroupedSchema = marshmallow_dataclass.class_schema(BomGrouped)


@dataclass
class Bom(BomCommonSchema):
    project: BomProject
    parts: list[BomPart]

    def group_by_part_id(self) -> BomGrouped:
        groups: dict[int, BomPartGroup] = {}
        ungrouped: list[BomPartGroup] = []
        sort = 1

        for part in self.parts:
            group = groups.get(part.part_id) if part.part_id is not None else None
            if group:
                group.references.append(part.reference)
                group.quantity += 1
            else:
                new_group = BomPartGroup(
                    sort,
                    [part.reference],
                    part.part_id,
                    1,
                    part.datasheet,
                    [],
                    title=part.title,
                )
                if (
                    new_group.part_id is not None
                    and new_group.part_id != MISSING_PART_ID
                ):
                    groups[new_group.part_id] = new_group
                else:
                    ungrouped.append(new_group)

                sort += 1

        return BomGrouped(
            project=self.project, groups=[*list(groups.values()), *ungrouped]
        )


BomSchema = marshmallow_dataclass.class_schema(Bom)
