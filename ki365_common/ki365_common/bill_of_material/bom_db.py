from copy import copy
from pprint import pprint
from typing import Any
from slugify import slugify

from sqlalchemy import and_
from ki365_common.bill_of_material.bom import (
    MISSING_PART_ID,
    Bom,
    BomLogEvent,
    BomManufacturer,
    BomManufacturerPart,
    BomPart,
    BomPartGroup,
    BomProject,
    BomSupplier,
    BomSupplierPart,
    Severity,
    logger,
)
from ki365_common.models.assemblies import (
    Assembly,
    AssemblyRevision,
    AssemblyRevisionField,
    AssemblyRevisionPart,
    AssemblyRevisionPartField,
)


from sqlalchemy.orm import Session
from ki365_common.models.manufacturers import ManufacturerPart

from ki365_common.models.part import Part
from ki365_common.status import AssemblyRevisionStatus


def bom_from_assembly_revision(revision: AssemblyRevision, source: str = "db") -> Bom:
    project = BomProject(
        source=source,
        title=revision.assembly.part.title,
        revision=revision.revision,
        properties={field.name: field.value for field in revision._fields},
    )
    # pprint(project)

    bom_parts = []

    for part in revision.parts:
        # TODO: get from database instead
        part_datasheet = ""
        fields = (
            {f.name: f.value for f in part._fields} if len(part._fields) > 0 else None
        )
        title = part.part.title if part.part is not None else None
        bom_parts.append(
            BomPart(
                part.id,
                part.reference,
                part.part_id,
                part_datasheet,
                fields=fields,
                title=title,
            )
        )

    bom = Bom(project, parts=bom_parts)
    return bom


def assembly_revision_from_bom(
    revision: AssemblyRevision, bom: Bom, append: bool = False
) -> list[Any]:
    to_commit: list[Any] = []
    revision._fields = [
        AssemblyRevisionField(name=k, value=v)
        for k, v in bom.project.properties.items()
    ]

    parts = revision.parts if append else []
    for p in bom.parts:
        fields = (
            [AssemblyRevisionPartField(name=k, value=v) for k, v in p.fields.items()]
            if p.fields is not None
            else []
        )
        part = AssemblyRevisionPart(
            reference=p.reference, part_id=p.part_id, _fields=fields
        )
        parts.append(part)
        to_commit.extend([part, *fields])

    # Connect to revision
    revision.parts = parts

    return to_commit


def bom_from_db(session: Session, revision_id: int, source: str = "db") -> Bom:
    assembly_revision = (
        session.query(AssemblyRevision)
        .where(AssemblyRevision.id == revision_id)
        .one_or_none()
    )
    # pprint(assembly_revision)
    if assembly_revision is None:
        raise ValueError("Invalid revision id")

    return bom_from_assembly_revision(assembly_revision, source=source)


def create_revision_from_bom(
    session: Session,
    bom: Bom,
    assembly_id: int,
    revision: str,
) -> AssemblyRevision:
    assembly: Assembly = session.query(Assembly).where(Assembly.id == assembly_id).one()

    assembly_revision = AssemblyRevision(
        revision=revision,
        revision_slug=slugify(revision),
        status=AssemblyRevisionStatus.PRELIMINARY,
    )

    assembly_revision_from_bom(assembly_revision, bom, append=False)

    assembly.revisions.append(assembly_revision)

    return assembly_revision


def bom_explode_subassemblies(
    session: Session,
    bom: Bom,
    explode_assemblies: bool | list[int] = True,
    indent: int = 1,
):
    part_cache: dict[int, Part] = {}

    new_parts = []

    if bom.parts is None:
        return

    for bom_part in bom.parts:
        # Check that part contains a part_id
        if bom_part.part_id is None or bom_part.part_id == MISSING_PART_ID:
            continue

        # Cache parts to avoid unnecessary db hits
        part = part_cache.get(bom_part.part_id)
        if part is None:
            part = session.query(Part).where(Part.id == bom_part.part_id).one()
            part_cache[bom_part.part_id] = part

        def should_explode_assembly(
            explode_assemblies: bool | list[int], assembly_id: int
        ):
            if isinstance(explode_assemblies, bool):
                return explode_assemblies

            return assembly_id in explode_assemblies

        if part.assembly is not None:
            print(f"Assembly detected: {bom_part.reference}: {part!r}")
            assembly = part.assembly
            ordered_revisions = sorted(
                assembly.revisions, key=lambda r: (r.status, -r.id)
            )
            selected_revision = next((r for r in ordered_revisions), None)
            if selected_revision is None:
                # TODO: Handle no revisions
                continue

            print("  " * indent + f"Selected revision: {selected_revision.revision}")

            always_explode = selected_revision.get_field("KI365_EXPLODE") == "True" or (
                bom_part.fields is not None
                and bom_part.fields.get("KI365_EXPLODE") == "True"
            )

            if (
                should_explode_assembly(explode_assemblies, assembly.id)
                or always_explode
            ):
                print("  " * indent + "Exploding...")

                assembly_bom = bom_from_assembly_revision(selected_revision)
                bom_explode_subassemblies(
                    session,
                    assembly_bom,
                    explode_assemblies=explode_assemblies,
                    indent=indent + 1,
                )

                if assembly_bom.parts is None:
                    continue

                for assembly_part in assembly_bom.parts:
                    new_part = copy(assembly_part)
                    new_part.reference = (
                        bom_part.reference + "." + assembly_part.reference
                    )
                    new_parts.append(new_part)
            else:
                new_parts.append(bom_part)

        else:
            new_parts.append(bom_part)

            # bom.parts.remove(bom_part)

    bom.parts = new_parts


def bom_populate_groups_from_db(
    session: Session,
    part_groups: list[BomPartGroup],
):
    """Populate BOM with manufacturer and supplier links from the database"""
    for part_group in part_groups:
        # Check that part contains a part_id
        if part_group.part_id is None or part_group.part_id == MISSING_PART_ID:
            refs = ",".join(part_group.references)
            print(f"Missing part-id for {refs}")
            continue

        part = session.query(Part).where(Part.id == part_group.part_id).one()

        for mfr_part in part.manufacturer_parts:
            bom_supplier_parts = []
            events = []

            active_supplier_parts = [
                supplier_part for supplier_part in mfr_part.supplier_parts
            ]

            for supplier_part in active_supplier_parts:
                if supplier_part.status.is_error() or supplier_part.status.is_warning():
                    event = BomLogEvent(
                        Severity.ERROR
                        if supplier_part.status.is_error()
                        else Severity.WARNING,
                        supplier_part.status.value.capitalize(),
                        f"'{supplier_part.supplier.name}/{supplier_part.partno}'",
                    )
                    event.log(logger)
                    events.append(event)

                #
                bom_supplier = BomSupplier(
                    supplier_part.supplier.id,
                    supplier_part.supplier.name,
                    supplier_part.supplier.link,
                    supplier_part.supplier.status,
                )
                bom_supplier_parts.append(
                    BomSupplierPart(
                        supplier_part.id,
                        supplier_part.partno,
                        supplier_part.status,
                        bom_supplier,
                        stock=None,
                    )
                )

            #
            manufacturer = BomManufacturer(
                mfr_part.manufacturer.id,
                mfr_part.manufacturer.name,
                mfr_part.manufacturer.link,
                mfr_part.manufacturer.status,
            )
            if part_group.manufacturers is None:
                part_group.manufacturers = []
            part_group.manufacturers.append(
                BomManufacturerPart(
                    mfr_part.id,
                    mfr_part.partno,
                    mfr_part.status,
                    manufacturer,
                    bom_supplier_parts,
                )
            )
