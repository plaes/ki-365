import logging
import os
import subprocess
from tempfile import TemporaryDirectory
from typing import List, Literal, Optional, overload
from dataclasses import dataclass, field
from pprint import pprint
from marshmallow import EXCLUDE

import marshmallow_dataclass
from xmltodict import parse as parse_xml  # type: ignore

from ki365_common.bill_of_material.bom import (
    Bom,
    BomLogEvent,
    BomPart,
    BomPartGroup,
    BomProject,
    Severity,
)

logger = logging.getLogger("bom.kicad")

KICAD_CLI_EXECUTABLE = os.environ.get("KICAD_CLI_EXECUTABLE", "kicad-cli")

PART_ID_FIELD_NAMES = ["Part#", "PartID", "Part-ID" "Partno"]


####################### <design> #######################
@dataclass
class KicadBomTextVar:
    name: str = field(metadata=dict(data_key="@name"))
    value: str = field(metadata=dict(data_key="#text"))

    class Meta:
        unknown = EXCLUDE


@dataclass
class KicadBomTitleblockComment:
    number: int = field(metadata=dict(data_key="@number"))
    value: str = field(metadata=dict(data_key="@value"))

    class Meta:
        unknown = EXCLUDE


@dataclass
class KicadBomSheetTitleblock:
    title: str | None
    company: str | None
    rev: str | None
    date: str | None
    source: str
    comments: List[KicadBomTitleblockComment] = field(metadata=dict(data_key="comment"))

    class Meta:
        unknown = EXCLUDE


@dataclass
class KicadBomSheet:
    name: str = field(metadata=dict(data_key="@name"))
    number: int = field(metadata=dict(data_key="@number"))
    title_block: KicadBomSheetTitleblock

    class Meta:
        unknown = EXCLUDE


@dataclass
class KicadBomDesign:
    source: str
    date: str
    _textvars: List[KicadBomTextVar] = field(metadata=dict(data_key="textvar"))
    sheets: List[KicadBomSheet] = field(metadata=dict(data_key="sheet"))

    class Meta:
        unknown = EXCLUDE

    @property
    def textvars(self) -> dict[str, str]:
        return {p.name: p.value for p in self._textvars}

    def get_primary_sheet(self, primary_sheet: str | None = None) -> KicadBomSheet:
        # Find and return the primary sheet
        if primary_sheet is not None:
            for s in self.sheets:
                if s.name == primary_sheet:
                    return s

        # Return first sheet
        return next(s for s in self.sheets)

    @overload
    def get_text_var(self, name: str, default: str) -> str:
        ...

    @overload
    def get_text_var(self, name: str, default: Literal[None] = None) -> str | None:
        ...

    def get_text_var(self, name: str, default: str | None = None) -> str | None:
        value = default

        for var in self._textvars:
            if var.name == name:
                value = var.value
                break

        return value


####################### </design> #######################


####################### <component> #######################
@dataclass
class KicadBomComponentField:
    name: str = field(metadata=dict(data_key="@name"))
    value: str | None = field(metadata=dict(data_key="#text"))

    class Meta:
        unknown = EXCLUDE


@dataclass
class KicadBomComponentFields:
    fields: List[KicadBomComponentField] = field(metadata=dict(data_key="field"))

    class Meta:
        unknown = EXCLUDE


@dataclass
class KicadBomComponentProperty:
    name: str = field(metadata=dict(data_key="@name"))
    value: str | None = field(metadata=dict(data_key="@value"))

    class Meta:
        unknown = EXCLUDE


@dataclass
class KicadBomComponentLibsource:
    lib: str = field(metadata=dict(data_key="@lib"))
    part: str = field(metadata=dict(data_key="@part"))
    description: str = field(metadata=dict(data_key="@description"))

    class Meta:
        unknown = EXCLUDE

    def __repr__(self) -> str:
        return f"{self.lib}:{self.part}"


@dataclass
class KicadBomComponentSheetpath:
    names: str = field(metadata=dict(data_key="@names"))

    class Meta:
        unknown = EXCLUDE

    def __repr__(self) -> str:
        return repr(self.names)


@dataclass
class KicadBomComponent:
    ref: str = field(metadata=dict(data_key="@ref"))
    value: str
    footprint: Optional[str]
    datasheet: Optional[str]
    sheet_path: KicadBomComponentSheetpath = field(metadata=dict(data_key="sheetpath"))
    lib_source: KicadBomComponentLibsource = field(metadata=dict(data_key="libsource"))
    properties: List[KicadBomComponentProperty] = field(
        metadata=dict(data_key="property")
    )
    fields: KicadBomComponentFields | None

    class Meta:
        unknown = EXCLUDE

    def get_property(self, fname: str, case_sensitive: bool = True, default: str | None = None) -> str | None:
        if self.fields is None:
            return default

        # Then search in fields
        for f in self.fields.fields:
            if f.name == fname:
                return f.value
            elif not case_sensitive and f.name.lower() == fname.lower():
                return f.value

        # Search in properties first
        for p in self.properties:
            if p.name == fname:
                return p.value
            elif not case_sensitive and p.name.lower() == fname.lower():
                return p.value

        # No field found
        return default

    def exclude(
        self,
        dnp: bool = True,
        exclude_from_bom: bool = True,
        exclude_from_board: bool = True,
    ):
        for f in self.properties:
            if f.name.lower() == "dnp" and dnp:
                return True
            if f.name.lower() == "exclude_from_bom" and exclude_from_bom:
                return True
            if f.name.lower() == "exclude_from_board" and exclude_from_board:
                return True

        return False


@dataclass
class KicadBomComponents:
    comp: List[KicadBomComponent]

    class Meta:
        unknown = EXCLUDE


####################### </components> #######################

####################### <libparts> #######################


@dataclass
class KicadBomLibpart:
    lib: str = field(metadata=dict(data_key="@lib"))
    part: str = field(metadata=dict(data_key="@part"))
    description: str
    fields: KicadBomComponentFields | None

    class Meta:
        unknown = EXCLUDE

    @overload
    def get_field(
        self,
        fname: str,
        default: str,
        case_sensitive: bool = True,
        ignore_tilde: bool = True,
    ) -> str:
        ...

    @overload
    def get_field(
        self,
        fname: str,
        default: Literal[None] = None,
        case_sensitive: bool = True,
        ignore_tilde: bool = True,
    ) -> str | None:
        ...

    def get_field(
        self,
        fname: str,
        default: str | None = None,
        case_sensitive: bool = True,
        ignore_tilde: bool = True,
    ) -> str | None:
        """_summary_

        Args:
            fname (str): Name of field
            case_sensitive (bool, optional): Field name is case-sensitive. Defaults to True.
            default (str | None, optional): Default value if the field is not found. Defaults to None.
            ignore_tilde (bool, optional): Ignore field values equal to '~' (used by kicad as placeholder for no value). Defaults to True.

        Returns:
            str | None: Value of the field, or none
        """

        if self.fields is None:
            return None

        # Then search in fields
        value = None
        for f in self.fields.fields:
            if f.name == fname:
                value = f.value
                break
            elif not case_sensitive and f.name.lower() == fname.lower():
                value = f.value
                break

        # No value found, return default
        if value is None or (value == "~" and ignore_tilde):
            return default

        # Return value
        return value

    @property
    def part_id(self) -> int | None:
        for name in PART_ID_FIELD_NAMES:
            val = self.get_field(name, case_sensitive=False)
            # Return first non-null or not empty value
            if val is not None and val != "":
                value = int(val)
                # A part-id of "" or -1 is treated as non-existant
                if value == -1:
                    continue
                return value

        # No part-id found
        return None


@dataclass
class KicadBomLibparts:
    libpart: List[KicadBomLibpart]

    class Meta:
        unknown = EXCLUDE


####################### </libparts> #######################


@dataclass
class KicadBomExport:
    version: str = field(metadata=dict(data_key="@version"))
    design: KicadBomDesign
    components: KicadBomComponents
    libparts: KicadBomLibparts

    class Meta:
        unknown = EXCLUDE


@dataclass
class KicadBom:
    export: KicadBomExport

    class Meta:
        unknown = EXCLUDE

    def project(self) -> tuple[BomProject, List[BomLogEvent]]:
        events = []

        source = self.export.design.source
        properties = self.export.design.textvars
        primary_sheet = self.export.design.get_primary_sheet()

        # Get revision
        revision = self.export.design.get_text_var("KI365_PROJECT_REVISION")
        if revision is None:
            revision = self.export.design.get_text_var("PROJECT_REVISION")
        if revision is None:
            revision = primary_sheet.title_block.rev

        title = self.export.design.get_text_var("KI365_PROJECT_TITLE")
        if title is None:
            title = self.export.design.get_text_var("PROJECT_TITLE")
        if title is None:
            title = primary_sheet.title_block.title
        if title is None:
            title = os.path.basename(source)

        # Set project part_id if it exists in properties
        part_id = None
        if "Ki365_PART_ID" in properties:
            val = properties["Ki365_PART_ID"]
            try:
                part_id = int(val)
                del properties["Ki365_PART_ID"]
            except ValueError:
                event = BomLogEvent(
                    Severity.WARNING,
                    f'Expected integer, found "{val}"',
                    f"'Ki365_PART_ID'",
                )
                event.log(logger)
                events.append(event)

        return (
            BomProject(source, title, revision, properties, part_id=part_id),
            events,
        )


KicadBomSchema = marshmallow_dataclass.class_schema(KicadBom)


def bom_from_kicad(xml, encoding: str | None = None, group: bool = True) -> Bom:
    xml_dict = parse_xml(xml, encoding=encoding, xml_attribs=True)

    kicad_bom: KicadBom = KicadBomSchema().load(xml_dict) # type: ignore
    parts = []
    sort = 1
    for kicad_part in kicad_bom.export.components.comp:
        part_id = next(
            (
                libpart.part_id
                for libpart in kicad_bom.export.libparts.libpart
                if libpart.lib == kicad_part.lib_source.lib
                and libpart.part == kicad_part.lib_source.part
            ),
            None,
        )

        fields = None
        if kicad_part.fields:
            fields = {f.name : (f.value or '') for f in kicad_part.fields.fields}

        part = BomPart(
            sort,
            kicad_part.ref,
            part_id,
            kicad_part.get_property("Datasheet") or '',
            fields=fields,
            title=kicad_part.value,
        )
        #pprint(part)
        parts.append(part)

        sort += 1

    proj, _events = kicad_bom.project()

    return Bom(proj, parts=parts)


def bom_from_kicad_sch(kicad_sch_path: str) -> Bom:
    with TemporaryDirectory(
        prefix="ki365-",
    ) as tmp:
        filename, _extension = os.path.splitext(kicad_sch_path)
        xml_bom_path = os.path.join(tmp, os.path.basename(filename) + ".xml")

        logging.info(f"Exporting BOM to {xml_bom_path}...")
        command = [
            KICAD_CLI_EXECUTABLE,
            "sch",
            "export",
            "python-bom",
            kicad_sch_path,
            "--output",
            xml_bom_path,
        ]
        logging.debug('Executing "' + " ".join(command) + '"')
        p = subprocess.run(
            command,
            # capture_output=True,
        )
        if p.returncode != 0:
            raise ValueError()

        # Run BOM script
        with open(xml_bom_path, "rb") as xml:
            bom = bom_from_kicad(xml, encoding="utf8")
            return bom


if __name__ == "__main__":
    import sys

    logger.setLevel(logging.DEBUG)

    with open(sys.argv[1], "rb") as xml:
        bom = bom_from_kicad(xml)
        pprint(bom)
