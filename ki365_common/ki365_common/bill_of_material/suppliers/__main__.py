from argparse import ArgumentParser
import logging
from pprint import pprint

from dotenv import load_dotenv

from .supplier_api import SupplierApi

load_dotenv()

parser = ArgumentParser()
parser.add_argument(
    "supplier",
    choices=[s.name()[0] for s in SupplierApi.get_supplier_apis() if s.is_enabled()],
)
parser.add_argument("partno", nargs="+")
parser.add_argument("--refresh", action='store_true')
args = parser.parse_args()

logging.basicConfig(level=logging.DEBUG)

api = SupplierApi.find_supplier(args.supplier)
if api is not None:
    info = api.get_batch_pricing(args.partno, refresh=args.refresh)
    pprint(info)
else:
    print("Invalid API")
