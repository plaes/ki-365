import os
from typing import Sequence
from typing_extensions import override
from ki365_common.bill_of_material.bom import BomSupplierStock, BomPriceBreak
from .supplier_api import SupplierApi
from digikey.v3.batchproductdetails import ProductDetails as BatchProductDetails  # type: ignore
from digikey.v3.productinformation import ProductDetails  # type: ignore
from digikey import product_details  # type: ignore


class DigikeyApi(SupplierApi):
    @override
    @staticmethod
    def name() -> list[str]:
        return ["digikey", "digi-key"]

    @override
    @staticmethod
    def is_enabled():
        return (
            "DIGIKEY_CLIENT_ID" in os.environ
            and "DIGIKEY_CLIENT_SECRET" in os.environ
            and "DIGIKEY_STORAGE_PATH" in os.environ
        )

    @staticmethod
    def _digikey_to_bom(pd: ProductDetails | BatchProductDetails) -> BomSupplierStock:
        if pd.standard_pricing is not None:
            pricing = [
                BomPriceBreak(quantity=pb.break_quantity, cost=pb.unit_price)
                for pb in pd.standard_pricing
            ]
        else:
            pricing = []

        currency = (
            pd.search_locale_used.currency
            if pd.search_locale_used is not None
            else "USD"
        )
        return BomSupplierStock(
            status=pd.product_status or "Unknown",
            in_stock=pd.quantity_available or 0,
            currency=currency,
            pricing=pricing,
        )

    @override
    def get_pricing(self, supplier_partno: str) -> BomSupplierStock:
        pd = product_details(supplier_partno)
        return DigikeyApi._digikey_to_bom(pd)

    # Doesn't work for some reason (returns unauthorized even with ProductInformation permission key)
    # @override
    # def get_batch_pricing(
    #     self, supplier_partno: Sequence[str]
    # ) -> Sequence[BomSupplierStock | None]:
    #     res = []
    #     batch_request = BatchProductDetailsRequest(products=supplier_partno)
    #     part_results = digikey.batch_product_details(body=batch_request)
    #     if part_results.product_details is not None:
    #         product_details: list[BatchProductDetails] = part_results.product_details
    #         for pd in product_details:
    #             stock = DigikeyApi._digikey_to_bom(pd)
    #             res.append(stock)

    #     return res


if __name__ == "__main__":
    pass
