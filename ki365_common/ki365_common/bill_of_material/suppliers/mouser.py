from dataclasses import dataclass
import os
from pprint import pprint
from typing import List, Literal, Optional
from typing_extensions import override
from ki365_common.bill_of_material.bom import BomPriceBreak, BomSupplierStock
from .supplier_api import NoSuchPart, SupplierApi

from marshmallow import EXCLUDE
from marshmallow_dataclass import class_schema

from ratelimit import limits, sleep_and_retry  # type: ignore

import requests

MOUSER_API_URL = "https://api.mouser.com/api/v2/search/"

MOUSER_API_LIMIT_CALLS = int(os.environ.get("MOUSER_API_LIMIT_CALLS", 30))
MOUSER_API_LIMIT_PERIOD = int(os.environ.get("MOUSER_API_LIMIT_PERIOD", 60))


@dataclass
class ErrorEntity:
    Id: int
    Code: str
    Message: str
    ResourceKey: str
    ResourceFormatString: str
    ResourceFormatString2: str
    PropertyName: str

    def __str__(self) -> str:
        return self.Message


@dataclass
class PriceBreak:
    Quantity: int
    Price: str
    Currency: str


@dataclass
class MouserPart:
    ProductDetailUrl: str
    AvailabilityInStock: str  # API docs says integer but it returns a string...
    PriceBreaks: List[PriceBreak]
    LifecycleStatus: Optional[str]

    class Meta:
        unknown = EXCLUDE


@dataclass
class SearchResponse:
    NumberOfResult: int
    Parts: List[MouserPart]


@dataclass
class SearchResponseRoot:
    Errors: List[ErrorEntity]
    SearchResults: SearchResponse


SearchResponseRootSchema = class_schema(SearchResponseRoot)


@dataclass
class SearchByPartRequest:
    mouserPartNumber: str
    partSearchOptions: Literal["None", "Exact", 1, 2] = "Exact"


@dataclass
class SearchByPartRequestRoot:
    SearchByPartRequest: SearchByPartRequest


class MouserApi(SupplierApi):
    @override
    @staticmethod
    def name() -> list[str]:
        return ["mouser"]

    @override
    @staticmethod
    def is_enabled():
        return "MOUSER_API_KEY" in os.environ

    @staticmethod
    def _mouser_to_bom(pd: MouserPart) -> BomSupplierStock:
        def price_to_float(price: str):
            price, *_currency = price.split(" ")
            price = price.replace(",", ".")
            return float(price)

        pricing = [
            BomPriceBreak(quantity=pb.Quantity, cost=price_to_float(pb.Price))
            for pb in pd.PriceBreaks
        ]

        currency = next((pb.Currency for pb in pd.PriceBreaks), "")

        return BomSupplierStock(
            status=pd.LifecycleStatus or 'Unknown',
            in_stock=int(pd.AvailabilityInStock),
            currency=currency,
            pricing=pricing,
        )

    @override
    @sleep_and_retry
    @limits(calls=MOUSER_API_LIMIT_CALLS, period=MOUSER_API_LIMIT_PERIOD)
    def get_pricing(self, supplier_partno: str) -> BomSupplierStock:
        params = dict(apiKey=os.environ.get("MOUSER_API_KEY"))

        resp = requests.post(
            url=MOUSER_API_URL + "partnumber",
            params=params,
            json={
                "SearchByPartRequest": {
                    "mouserPartNumber": supplier_partno,
                    "partSearchOptions": "Exact",
                }
            },
        )
        data = resp.json()
        data_cls: SearchResponseRoot = SearchResponseRootSchema().load(data)  # type: ignore
        if len(data_cls.SearchResults.Parts) >= 1:
            return self._mouser_to_bom(data_cls.SearchResults.Parts[0])
        elif len(data_cls.Errors) > 0:
            msg = ", ".join([str(err) for err in data_cls.Errors])
            print(msg)
            raise NoSuchPart(msg=msg)
        else:
            print("No results")
            raise NoSuchPart(msg="No results")
