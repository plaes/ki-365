from __future__ import annotations
import logging

import os
from pprint import pprint
from typing import List, Sequence
from ki365_common.bill_of_material.bom import Bom, BomGrouped, BomSupplierPart
import concurrent.futures

from ki365_common.util import to_bool

from .supplier_api import SupplierApi

logger = logging.getLogger("ki365.suppliers")


def get_supplier_part_pricing(sp: BomGrouped, multithread: bool = True):
    parts_by_supplier: dict[str, list[BomSupplierPart]] = {}

    # Get all supplier parts
    supplier_parts: list[BomSupplierPart] = []
    for group in sp.groups:
        for mfr in group.manufacturers:
            supplier_parts.extend(mfr.suppliers)

    # Split into differenet suppliers
    for supplier_part in supplier_parts:
        supplier_list = parts_by_supplier.get(supplier_part.supplier.name)
        if supplier_list is None:
            supplier_list = []
            parts_by_supplier[supplier_part.supplier.name] = supplier_list
        supplier_list.append(supplier_part)

    def common(supplier_api: SupplierApi, parts: list[BomSupplierPart]):
        unique = {s.partno: s for s in parts}
        pricings = supplier_api.get_batch_pricing(list(unique.keys()))
        print(f"  Fetched {len([p for p in pricings if p is not None])} pricings from {supplier_api.name()[0]}...")
        #pprint(pricings)
        for partno, pricing in pricings.items():
            unique[partno].stock = pricing

    if multithread and to_bool(
        os.environ.get("KI365_MULTITHREAD_SUPPLIER_FETCH", True)
    ):
        workers = os.environ.get("KI365_MULTITHREAD_SUPPLIER_WORKERS")
        if workers is not None:
            num_workers = int(workers)
        else:
            num_workers = None
        
        with concurrent.futures.ThreadPoolExecutor(max_workers=num_workers) as executor:
            futures: list[concurrent.futures.Future] = []
            for supplier_name, parts in parts_by_supplier.items():
                supplier_api = SupplierApi.find_supplier(supplier_name)
                if supplier_api is not None:
                    print(f"Fetching parts from {supplier_name}")
                    fut = executor.submit(common, supplier_api, parts)
                    futures.append(fut)
                else: 
                    print(f"Supplier not available: '{supplier_name}'")
            
            for fut in futures:
                try:
                    _result = fut.result()
                    print('OK', fut)
                except Exception as err:
                    logger.exception("Error")
    else:
        for supplier_name, parts in parts_by_supplier.items():
            supplier_api = SupplierApi.find_supplier(supplier_name)
            if supplier_api is not None:
                print(f"Fetching parts from {supplier_name}")
                common(supplier_api, parts)
            else: 
                print(f"Supplier not available: '{supplier_name}'")


