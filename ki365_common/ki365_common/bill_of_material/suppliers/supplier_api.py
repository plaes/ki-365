from __future__ import annotations
import hashlib
import json
import logging
import os
from time import time
from typing_extensions import override

from ki365_common.bill_of_material.bom import BomSupplierStock, BomSupplierStockSchema

from typing import Sequence

from marshmallow import ValidationError

KI365_SUPPLIER_CACHE_TTL = int(os.environ.get("KI365_SUPPLIER_CACHE_TTL", 3600 * 24))


class NoSuchPart(Exception):
    msg: str | None

    def __init__(self, msg: str | None) -> None:
        super().__init__(msg)
        self.msg = msg


class SupplierCache(object):
    _subclasses: list[type[SupplierCache]] = []

    def __init__(self, supplier_name: str, **kwargs) -> None:
        self.logger = logging.getLogger(f"ki365.suppliers.cache.{supplier_name}")

    @classmethod
    def get_supplier_caches(cls) -> list[type[SupplierCache]]:
        return cls._subclasses

    @classmethod
    def create_cache(cls, supplier_name: str) -> SupplierCache:
        typ = os.environ.get("KI365_SUPPLIER_CACHE", "MEMORY")
        for c in cls.get_supplier_caches():
            if c.name().lower() == typ.lower():
                return c(supplier_name=supplier_name)

        raise ValueError("Invalid cache")

    def __init_subclass__(cls):
        SupplierCache._subclasses.append(cls)  # type: ignore

    def get_cached_pricing(
        self, supplier_partno: str, ttl: int = -1
    ) -> BomSupplierStock | None:
        if ttl == -1:
            ttl = KI365_SUPPLIER_CACHE_TTL
        r = self._get_cached_pricing(supplier_partno, ttl)
        self.logger.debug(f"GET {supplier_partno} = {r}")
        return r

    def set_cached_pricing(self, supplier_partno: str, pricing: BomSupplierStock):
        self.logger.debug(f"SET {supplier_partno} = {pricing}")
        self._set_cached_pricing(supplier_partno, pricing)

    @staticmethod
    def name() -> str:
        raise NotImplementedError

    def _get_cached_pricing(
        self, supplier_partno: str, ttl: int
    ) -> BomSupplierStock | None:
        raise NotImplementedError

    def _set_cached_pricing(self, supplier_partno: str, pricing: BomSupplierStock):
        raise NotImplementedError


class DummySupplierCache(SupplierCache):
    cache: dict[str, tuple[BomSupplierStock, int]]

    def __init__(self, **kwargs) -> None:
        super().__init__(**kwargs)

    @override
    @staticmethod
    def name() -> str:
        return "dummy"

    @override
    def _get_cached_pricing(
        self, supplier_partno: str, ttl: int
    ) -> BomSupplierStock | None:
        return None

    @override
    def _set_cached_pricing(self, supplier_partno: str, pricing: BomSupplierStock):
        pass


class MemorySupplierCache(SupplierCache):
    cache: dict[str, tuple[BomSupplierStock, int]]

    def __init__(self, **kwargs) -> None:
        super().__init__(**kwargs)
        self.cache = {}

    @override
    @staticmethod
    def name() -> str:
        return "memory"

    @override
    def _get_cached_pricing(
        self, supplier_partno: str, ttl: int
    ) -> BomSupplierStock | None:
        part, part_ttl = self.cache.get(supplier_partno, (None, 0))
        if part is not None and (part_ttl + ttl > int(time())):
            return part
        else:
            # self.cache[supplier_partno] = None
            return None

    @override
    def _set_cached_pricing(self, supplier_partno: str, pricing: BomSupplierStock):
        self.cache[supplier_partno] = (pricing, int(time()))


class FileSupplierCache(SupplierCache):
    def __init__(self, supplier_name: str, **kwargs) -> None:
        super().__init__(supplier_name=supplier_name, **kwargs)
        self.supplier_name = supplier_name
        c_dir = os.environ.get("KI365_SUPPLIER_CACHE_DIR", "/tmp/ki365/suppliers/cache")
        self.cache_dir = os.path.join(c_dir, self.supplier_name)
        os.makedirs(self.cache_dir, exist_ok=True)

    @override
    @staticmethod
    def name() -> str:
        return "file"

    @override
    def _get_cached_pricing(
        self, supplier_partno: str, ttl: int
    ) -> BomSupplierStock | None:
        sha = hashlib.sha1()
        sha.update(supplier_partno.encode())
        file = os.path.join(self.cache_dir, sha.hexdigest())
        if os.path.exists(file) and int(os.path.getmtime(file)) + ttl > int(time()):
            with open(file) as partno_cache:
                data_json = json.load(partno_cache)

            try:
                part = BomSupplierStockSchema().load(data_json)
                return part  # type: ignore
            except ValidationError:
                os.remove(file)
                return None
        else:
            return None

    @override
    def _set_cached_pricing(self, supplier_partno: str, pricing: BomSupplierStock):
        sha = hashlib.sha1()
        sha.update(supplier_partno.encode())
        file = os.path.join(self.cache_dir, sha.hexdigest())
        with open(file, "x") as partno_cache:
            data = BomSupplierStockSchema().dump(pricing)
            json.dump(data, partno_cache)


try:
    import redis

    KI365_SUPPLIER_CACHE_REDIS_HOST = os.environ.get(
        "KI365_SUPPLIER_CACHE_REDIS_HOST", "localhost"
    )
    KI365_SUPPLIER_CACHE_REDIS_PORT = int(
        os.environ.get("KI365_SUPPLIER_CACHE_REDIS_PORT", 6379)
    )
    conn = redis.Redis(
        host=KI365_SUPPLIER_CACHE_REDIS_HOST, port=KI365_SUPPLIER_CACHE_REDIS_PORT
    )

    class RedisSupplierCache(SupplierCache):
        supplier_name: str

        def __init__(self, supplier_name: str, **kwargs) -> None:
            super().__init__(supplier_name=supplier_name, **kwargs)
            self.supplier_name = supplier_name

        @override
        @staticmethod
        def name() -> str:
            return "redis"

        @override
        def _get_cached_pricing(
            self, supplier_partno: str, ttl: int
        ) -> BomSupplierStock | None:
            key = f"ki365:suppliers:{self.supplier_name}:cache:{supplier_partno}"
            data = conn.get(key)
            if data is None:
                return None

            data_json = json.loads(data)
            try:
                part = BomSupplierStockSchema().load(data_json)
                return part  # type: ignore
            except ValidationError:
                conn.delete(key)
                return None

        @override
        def _set_cached_pricing(self, supplier_partno: str, pricing: BomSupplierStock):
            key = f"ki365:suppliers:{self.supplier_name}:cache:{supplier_partno}"
            ttl = int(KI365_SUPPLIER_CACHE_TTL)
            data = BomSupplierStockSchema().dump(pricing)
            conn.setex(key, ttl, json.dumps(data))

except ImportError:
    pass


class SupplierApi(object):
    _subclasses: list[SupplierApi] = []
    _caches: dict[str, SupplierCache] = {}
    logger: logging.Logger

    def __init__(self) -> None:
        self.logger = logging.getLogger(f"ki365.suppliers.{self.name()[0]}")

    @classmethod
    def get_supplier_apis(cls) -> list[SupplierApi]:
        return cls._subclasses

    @classmethod
    def find_supplier(cls, name: str):
        return next(
            (s for s in cls._subclasses if name.lower() in s.name() and s.is_enabled()),
            None,
        )

    def __init_subclass__(cls):
        SupplierApi._subclasses.append(cls())  # type: ignore

    @staticmethod
    def name() -> list[str]:
        raise NotImplementedError

    @staticmethod
    def is_enabled():
        """Return true if api is enabled and have all required parameters"""
        return True

    def get_cached_pricing(
        self, supplier_partno: str, refresh: bool = False
    ) -> BomSupplierStock:
        """Get pricing for a single part"""
        name = self.name()[0]

        # Get cache or create a new cache if one does not exist
        cache = SupplierApi._caches.get(name)
        if cache is None:
            cache = SupplierCache.create_cache(name)
            SupplierApi._caches[name] = cache

        # Check if part exists in cache
        pricing = cache.get_cached_pricing(supplier_partno)
        if pricing is None or refresh:
            print(f"Fetching {supplier_partno} using '{name}' API")
            pricing = self.get_pricing(supplier_partno)
            cache.set_cached_pricing(supplier_partno, pricing)

        return pricing

    def get_pricing(self, supplier_partno: str) -> BomSupplierStock:
        """Get pricing for a single part"""
        raise NotImplementedError

    def get_batch_pricing(
        self, supplier_partno: Sequence[str], refresh: bool = False
    ) -> dict[str, BomSupplierStock | None]:
        """Get pricing for multiple parts"""
        res = {}
        for partno in supplier_partno:
            try:
                pricing = self.get_cached_pricing(partno, refresh=refresh)
            except NoSuchPart as err:
                pricing = None
            res[partno] = pricing
        return res


# Import to register
from .digikey import DigikeyApi
from .mouser import MouserApi
