# kicad-365

Main web application

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

## Deployment

Below is an example deployment using docker and docker compose and our [example library](https://gitlab.com/ki365/library).

### Prerequisites
* [docker & docker-compose](https://docs.docker.com/compose/install/)

### Prepare kicad and database libraries
```bash
mkdir kicad
cd kicad
# Clone standard kicad libraries
git clone https://gitlab.com/kicad/libraries/kicad-symbols.git
git clone https://gitlab.com/kicad/libraries/kicad-footprints.git
# Clone our example database library
# If you already .kicad_dbl symlink it as library/ki365.kicad_dbl
git clone https://gitlab.com/ki365/library.git
```

### Download docker-compose file
```bash
mkdir ki365
cd ki365

# Download and edit example docker-compose file
wget "https://gitlab.com/ki365/ki-365/-/raw/main/docker-compose.yml"

# Edit the bottom "/path/to/kicad" to the kicad directory you created previously
nano docker-compose.yml

# Run the server
FLASK_SECRET="<some secret>" JWT_SECRET_KEY="<some secret>" POSTGRES_PASSWORD="<password>" POSTGRES_USER=ki365_admin docker compose up
```

### Initialize the database
```bash
# Enter the server
docker compose exec ki365 sh

# Initialize the ki365 database
cd ki365_server/
alembic upgrade head

# Initialize example kicad library database
# Or use your own tools to create the database
cd /kicad/library
alembic upgrade head
```

### Check if it works
Ki365 should be served on http://localhost:8000

Default user is "Admin", password is created upon first login.

For a internet facing server I suggest using a reverse proxy such as Nginx, Caddy, or a VPN for security.

## Resources

Add links to external resources for this project, such as CI server, bug tracker, etc.

## License
This library is licensed under GPLv3.

See [LICENSE](./LICENSE)